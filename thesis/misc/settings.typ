#import "@preview/lemmify:0.1.5": *

#let page-is-empty(loc) = {
  let page = loc.page()
  let pairs = state("chapter-markers").at(loc)
  if pairs == none { return false }
  return pairs.any(((end-page, start-page)) => {page > end-page and page < start-page})
}

#let page-is-first(loc) = {
  let page = loc.page()
  let pairs = state("chapter-markers").at(loc)
  if pairs == none { return false }
  return pairs.any(((end-page, start-page)) => {page == start-page})
}

#let document_settings(content) = {

	set document(
		title: "Chromatic quasisymmetric functions",
		keywords: "Master's thesis in mathematics",
		author: "Eduardo Venturini",
	)

	set page(
		paper: "a4",
		margin: (y: 3cm),
		header: locate(loc => {
			if not page-is-empty(loc) and page.numbering != none {
				let prev = query(heading.where(level: 1).before(loc))
				if prev != () and not page-is-first(loc) {
					let chapter-name = smallcaps(prev.last().body)
					if calc.even(counter(page).at(loc).first()) {
						[#counter(page).display(page.numbering) #h(1fr) #chapter-name]
					} else {
						[#chapter-name #h(1fr) #counter(page).display(page.numbering)]
					}
				}
			}
		}),
		footer: locate(loc =>	{
			if not page-is-empty(loc) and page.numbering != none {
				let prev = query(heading.where(level: 1).before(loc))
				if prev != () and page-is-first(loc) {
					align(center)[#counter(page).display(page.numbering)]
				}
			}
		}),
	)

	set heading(
		numbering: "1.",
	)

	set text(
		lang: "en",
		size: 11pt,
	)

	set par(
		justify: true,
		first-line-indent: 1.5em,
	)

	show heading: it => {
		if it.level == 1 [
			#[]<chapter-end-marker>
			#pagebreak(to: "odd", weak: true)
			#[]<chapter-start-marker>
			#v(3cm)
			#set par(justify: false)
			#set align(center)
			#set text(size: 24pt, hyphenate: false)
			#if it.supplement == [chapter] [
				#smallcaps("Chapter " + counter(heading).display("1")) \
			] else if it.supplement == [appendix] [
				#smallcaps("Appendix") \
			]
			#smallcaps(it.body)
			#v(3cm)
		] else {
			if it.level == 2 {v(3cm)}
			if it.numbering != none [
				#block([#counter(heading).display() #it.body])
			] else [
				#block(it.body)
			]
		}
	}

	locate(loc => {
		let chapter-end-markers = query(<chapter-end-marker>, loc)
		let chapter-start-markers = query(<chapter-start-marker>, loc)
		let pairs = chapter-end-markers.enumerate().map(((index, chapter-end-marker)) => {
			let chapter-start-marker = chapter-start-markers.at(index)
			let end-page = chapter-end-marker.location().page()
			let start-page = chapter-start-marker.location().page()
			(end-page, start-page)
		})
		state("chapter-markers").update(pairs)
	})

	set outline(
		indent: auto,
	)

	show outline: it => {
		set par(first-line-indent: 0pt)
		it
	}

	show outline.entry: it => {
		let it-loc = if it.level == 1 {
			query(selector(<chapter-start-marker>).after(it.element.location())).first().location()
		} else {
			it.element.location()
		}
		let it-page = numbering(it-loc.page-numbering(), counter(page).at(it-loc).first())
		let it-body = if it.element.supplement == [appendix] and it.level == 1 {
			[#numbering(it.element.numbering, counter(heading).at(it-loc).first()) Appendix. #it.element.body]
		}	else {
			it.body
		}
		let entry = link(it-loc)[#it-body #box(width: 1fr)[#it.fill] #it-page]
		if it.level == 1 {
			v(1em, weak: true)
			strong(entry)
		} else {
			entry
		}
	}

	content

}


#let thm-style-simple-bar(thm-type, name, number, body) = block(width: 100%, breakable: true)[
	#grid(
	  columns: (0fr, 1fr),
		row-gutter: 1em,
	column-gutter: 0.5em,
		grid.cell(colspan: 2)[#{
			strong(thm-type) + " "
			if number != none {strong(number) + " "}
			if name != none {emph[(#name)] + " "}
		}],
		grid.cell()[], body,
	)
]

#let (
	theorem, lemma, corollary, remark, proposition, example, proof, rules: default-theorems-rules
	) = default-theorems(
		"thm-group", lang: "en",
		thm-numbering: thm-numbering-heading,
		thm-styling: thm-style-simple-bar,
		)
#let (definition, rules: definition-rules) = new-theorems(
	"thm-group",
	("definition": text[Definition]),
	thm-numbering: thm-numbering-heading,
	thm-styling: thm-style-simple-bar,
	)
#let (conjecture, rules: conjecture-rules) = new-theorems(
	"thm-group",
	("conjecture": text[Conjecture]),
	thm-numbering: thm-numbering-heading,
	thm-styling: thm-style-simple-bar,
	)
#let (counterexample, rules: counterexample-rules) = new-theorems(
	"thm-group",
	("counterexample": text[Counterexample]),
	thm-numbering: thm-numbering-heading,
	thm-styling: thm-style-simple-bar,
	)
#let (problem, rules: problem-rules) = new-theorems(
	"thm-group",
	("problem": text[Problem]),
	thm-numbering: thm-numbering-heading,
	thm-styling: thm-style-simple-bar,
	)

#let default_settings(content) = {

	show: default-theorems-rules
	show: definition-rules
	show: conjecture-rules
	show: counterexample-rules
	show: problem-rules

	show math.prec: math.scripts(math.prec)
	show math.prec.eq: math.scripts(math.prec.eq)
	show math.succ: math.scripts(math.succ)
	show math.succ.eq: math.scripts(math.succ.eq)

	content

}
