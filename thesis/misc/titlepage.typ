#set align(center)
#set text(size: 20pt)

#image("../resources/marchio_unipi_pant541.svg", width: 75%)
#v(12mm)
#smallcaps[Department of Mathematics]
#v(12mm)
#text(size: 26pt, weight: "semibold")[Chromatic quasisymmetric functions]
#v(12mm)
#smallcaps[Master's Thesis in Mathematics]
#v(12mm)
#grid(columns: (1fr, 1fr),
  [#align(left)[
    #smallcaps[Candidate] \
    #text(weight: "semibold")[Eduardo Venturini] \
  ]],
  [#align(right)[
    #smallcaps[Supervisor] \
    Prof. #text(weight: "semibold")[Michele D'Adderio] \
    #text(style: "italic")[University of Pisa] \
  ]]
)
#v(12mm)
#line(length: 100%)
#v(5mm)
#text(size: 20pt)[Academic year 2023-2024]
