#import "@preview/fletcher:0.5.0" as fletcher: diagram, node, edge
#import "@preview/cetz:0.2.2"


#let labelled-circle(label) = {(node, extrude) => {
    cetz.draw.circle(
      (0,0),
      name: "labelled-circle",
      radius: node.radius + extrude)
    cetz.draw.content(
      "labelled-circle.north-east",
      box(fill: white, inset: 2pt)[#text(size: 10pt)[#label]])
}}

#let node-pos(x,y,n,i,r) = {
  if n == 1 {
    (x,y)
  } else {
    (x+r*calc.sin(i*360deg/n), y+r*calc.cos(i*360deg/n))
  }
}

#let my-graph(x, y, ls, es, cs: none, group: none, group-label: [], rad: 1, s: 10mm) = {
  x = x*s
  y = y*s
  rad = rad*s
  let name-prefix = if group == none {""} else {group}
  let to-label(i) = label(name-prefix + "-" + str(i))
  let n = ls.len()
  for i in range(n) {
    if cs == none {
      node(
        node-pos(x,y,n,i,rad),
        [#ls.at(i)],
        name: to-label(i)
      )
    } else {
      node(
        stroke: .1em,
        node-pos(x,y,n,i,rad),
        box(width: .3em, height: .3em)[#cs.at(i)],
        shape: labelled-circle(ls.at(i)),
        name: to-label(i)
      )
    }
  }
  for e in es {
  edge(to-label(e.at(0)), to-label(e.at(1)))
  }
  if group != none {
    node(
      (x,y),
      stroke: (paint: gray, dash: "dashed"),
      outset: .2em,
      enclose: range(n).map(to-label),
      name: label(name-prefix + "-group"),
    )
    node(
      (x,y - (if n==1 {1} else {2})*s),
      group-label,
    )
  }
}

#let graph-0-col = {
  box()[
    #diagram(
      node-stroke: .1em,
      spacing: 1em,
    {
      my-graph(0, 0, ([1],[2],[3],[4]), ((0,1), (0,2), (0,3), (2,3)), cs: ([1],[2],[2],[3]))
    }
    )
  ]
}

#let del-con-reduction = {
  box()[
    #diagram(
      spacing: 1em,
      {
        let to-label(s) = label(s + "-group")
        let s = 8mm
        let dh = 5
        let dv1 = 4
        let rad = 0.75
        my-graph(0*dh, 0, ([1],[2],[3],), ((0,1), (0,2), (1,2),), group: "g", group-label: [$x^3 - 3x^2 + 2x$], rad: rad, s: s)
          my-graph(1*dh, + dv1, ([1],[2],[3],), ((0,1), (0,2),), group: "gd", group-label: [$x^3 - 2x^2 + x$], rad: rad, s: s)
          edge(to-label("g"), to-label("gd"), "=>")
            my-graph(2*dh, + dv1 + 3, ([1],[2],[3],), ((0,1),), group: "gdd", group-label: [$x^3 - x^2$], rad: rad, s: s)
            edge(to-label("gd"), to-label("gdd"), "=>")
              my-graph(3*dh, + dv1 + 3 + 4, ([1],[2],[3],), (), group: "gddd", group-label: [$x^3$], rad: rad, s: s)
              edge(to-label("gdd"), to-label("gddd"), "=>")
              my-graph(3*dh, + dv1 + 3 - 1, ([12],[3],), (), group: "gddc", group-label: [$x^2$], rad: rad, s: s)
              edge(to-label("gdd"), to-label("gddc"), "=>")
            my-graph(2*dh, + dv1 - 3, ([13],[2],), ((0,1),), group: "gdc", group-label: [$x^2 - x$], rad: rad, s: s)
            edge(to-label("gd"), to-label("gdc"), "=>")
              my-graph(3*dh, + dv1 - 3 + 0, ([13],[2],), (), group: "gdcd", group-label: [$x^2$], rad: rad, s: s)
              edge(to-label("gdc"), to-label("gdcd"), "=>")
              my-graph(3*dh, + dv1 - 3 - 4, ([123],), (), group: "gdcc", group-label: [$x$], rad: rad, s: s)
              edge(to-label("gdc"), to-label("gdcc"), "=>")
          my-graph(1*dh, - dv1, ([1],[23],), ((0,1),), group: "gc", group-label: [$x^2 - x$], rad: rad, s: s)
          edge(to-label("g"), to-label("gc"), "=>")
            my-graph(2*dh, - dv1 + 0, ([1],[23],), (), group: "gcd", group-label: [$x^2$], rad: rad, s: s)
            edge(to-label("gc"), to-label("gcd"), "=>")
            my-graph(2*dh, - dv1 - 4, ([123],), (), group: "gcc", group-label: [$x$], rad: rad, s: s)
            edge(to-label("gc"), to-label("gcc"), "=>")
      }
    )
  ]
}

#let graph-0 = {
  box()[
    #diagram(
      spacing: 1em,
      {
      my-graph(0, 0, ([1],[2],[3],[4]), ((0,1), (0,2), (0,3), (2,3)))
      }
    )
  ]
}

#let poset-0-hasse = {
  box()[
    #diagram(
      spacing: 1em,
      {
      node((0,0), [$3$], name: label("3"))
      node((0,1), [$2$], name: label("2"))
      node((0,2), [$1$], name: label("1"))
      node((0,3), [$0$], name: label("0"))
      edge(label("3"), label("2"))
      edge(label("2"), label("1"))
      edge(label("1"), label("0"))
    })
  ]
}

#let poset-1-hasse = {
  box()[
    #diagram(
      spacing: 1em,
      {
      node((0,0), [${x,y,z}$], name: label("xyz"))
      node((-1,1), [${y,z}$], name: label("yz"))
      node((0,1), [${x,z}$], name: label("xz"))
      node((1,1), [${x,y}$], name: label("xy"))
      node((-1,3), [${x}$], name: label("x"))
      node((0,3), [${y}$], name: label("y"))
      node((1,3), [${z}$], name: label("z"))
      node((0,4), [${}$], name: label("e"))
      edge(label("xyz"), label("yz"))
      edge(label("xyz"), label("xz"))
      edge(label("xyz"), label("xy"))
      edge(label("yz"), label("y"))
      edge(label("yz"), label("z"))
      edge(label("xz"), label("x"))
      edge(label("xz"), label("z"))
      edge(label("xy"), label("x"))
      edge(label("xy"), label("y"))
      edge(label("x"), label("e"))
      edge(label("y"), label("e"))
      edge(label("z"), label("e"))
    })
  ]
}

#let poset-1-inc = {
  box()[
    #diagram(
      spacing: 1em,
      {
      node((0,0), [${x,y,z}$], name: label("xyz"))
      node((-1,1), [${y,z}$], name: label("yz"))
      node((0,1), [${x,z}$], name: label("xz"))
      node((1,1), [${x,y}$], name: label("xy"))
      node((-1,3), [${x}$], name: label("x"))
      node((0,3), [${y}$], name: label("y"))
      node((1,3), [${z}$], name: label("z"))
      node((0,4), [${}$], name: label("e"))
      edge(label("yz"), label("xz"))
      edge(label("xz"), label("xy"))
      edge(label("yz"), label("xy"), bend: +20deg)
      edge(label("yz"), label("x"))
      edge(label("xz"), label("y"))
      edge(label("xy"), label("z"))
      edge(label("x"), label("y"))
      edge(label("y"), label("z"))
      edge(label("x"), label("z"), bend: -20deg)
    })
  ]
}

#let poset-claw = {
  grid(columns: 3, align: horizon,
    [
      #diagram(
        spacing: 1em,
        {
        node((0,0), [$x_3$], name: label("x3"))
        node((0,1), [$x_2$], name: label("x2"))
        node((0,2), [$x_1$], name: label("x1"))
        node((1,1), [$y$], name: label("y"))
        edge(label("x3"), label("x2"))
        edge(label("x2"), label("x1"))
      })
    ], [
    #h(3em)
    $==>$
    #h(3em)
    ], [
      #diagram(
        spacing: 1em,
        {
        node((0,0), [$x_3$], name: label("x3"))
        node((0,1), [$x_2$], name: label("x2"))
        node((0,2), [$x_1$], name: label("x1"))
        node((1,1), [$y$], name: label("y"))
        edge(label("x3"), label("y"))
        edge(label("x2"), label("y"))
        edge(label("x1"), label("y"))
      })
    ]
  )
}

#let interval-graph(h) = {

  let n = h.len()
  let s = 12pt
  h.insert(0,0)

  let grid = {
    for i in range(1, n){
      cetz.draw.line((0*s, i*s), (n*s, i*s), stroke: stroke(paint: gray))
      cetz.draw.line((i*s, 0*s), (i*s, n*s), stroke: stroke(paint: gray))
    }
  }
  let diag = {
    cetz.draw.line((0*s, 0*s), (n*s, n*s), stroke: stroke(dash: "dashed"))
  }
  let frame = {
    cetz.draw.rect((0,0), (n*s, n*s))
  }
  let path = {
    for i in range(n){
      cetz.draw.line((i*s, h.at(i)*s), (i*s,h.at(i+1)*s), stroke: stroke(paint: blue, thickness: 2pt))
      cetz.draw.line((i*s, h.at(i+1)*s), ((i+1)*s,h.at(i+1)*s), stroke: stroke(paint: blue, thickness: 2pt))
      cetz.draw.content((-.5*s, (i+1)*s), [#(i+1)])
      cetz.draw.content(((i+1)*s, -.5*s), [#(i+1)])
    }
  }

  box([#cetz.canvas({
    grid
    diag
    frame
    path
  })])

}

#let graph-1 = {
  box()[
    #diagram(
      spacing: 1.5em,
      {
        node((0,0), [1], name: label("1"))
        node((1,0), [2], name: label("2"))
        node((2,0), [3], name: label("3"))
        node((3,0), [4], name: label("4"))
        node((4,0), [5], name: label("5"))
        edge(label("1"), label("2"))
        edge(label("1"), label("3"), bend: +45deg)
        edge(label("1"), label("4"), bend: +45deg)
        edge(label("2"), label("3"))
        edge(label("3"), label("4"))
        edge(label("3"), label("5"), bend: +45deg)
      }
    )
  ]
}

#let graph-2 = {
  box()[
    #diagram(
      spacing: 1.5em,
      {
        node((0,0), [1], name: label("1"))
        node((1,0), [2], name: label("2"))
        node((2,0), [3], name: label("3"))
        node((3,0), [4], name: label("4"))
        node((4,0), [5], name: label("5"))
        edge(label("1"), label("2"))
        edge(label("1"), label("3"), bend: +45deg)
        edge(label("2"), label("3"))
        edge(label("3"), label("4"))
        edge(label("3"), label("5"), bend: +45deg)
        edge(label("4"), label("5"))
      }
    )
  ]
}

#let graph-3 = {
  box()[
    #diagram(
      spacing: 1em,
      {
        node((1*calc.sin(180deg+0*120deg),1*calc.cos(180deg+0*120deg)), [1], name: label("1"))
        node((1*calc.sin(180deg+1*120deg),1*calc.cos(180deg+1*120deg)), [2], name: label("2"))
        node((1*calc.sin(180deg+2*120deg),1*calc.cos(180deg+2*120deg)), [3], name: label("3"))
        node(((1+calc.sqrt(3))*calc.sin(180deg+0*120deg),(1+calc.sqrt(3))*calc.cos(180deg+0*120deg)), [4], name: label("4"))
        node(((1+calc.sqrt(3))*calc.sin(180deg+1*120deg),(1+calc.sqrt(3))*calc.cos(180deg+1*120deg)), [5], name: label("5"))
        node(((1+calc.sqrt(3))*calc.sin(180deg+2*120deg),(1+calc.sqrt(3))*calc.cos(180deg+2*120deg)), [6], name: label("6"))
        edge(label("1"), label("2"))
        edge(label("2"), label("3"))
        edge(label("3"), label("1"))
        edge(label("1"), label("4"))
        edge(label("2"), label("5"))
        edge(label("3"), label("6"))
      }
    )
  ]
}

#let graph-melting-lollipop(nt, nc, nm) = {
  box()[
    #diagram(
      spacing: 1em,
      {
        for i in range(nt){
          node((-nt -1 + i,0), [#(i+1)], name:label(str(i)))
        }
        for i in range(nc){
          node((-calc.sin(90deg+i*360deg/nc), -calc.cos(90deg+i*360deg/nc)), [#(i+1+nt)], name: label(str(i+nt)))
        }
        for i in range(nt+nc){
          for j in range(i){
            if j > nt or i == j+1 or (j == nt and i < nt + nc - nm) {
              edge(label(str(i)), label(str(j)))
            }
          }
        }
      }
    )
  ]
}

#let ssyt-content = {

  let s = 12pt
  let nr = 8
  let nc = 9

  let a-grid = stroke(paint: gray)
  let a-diag = stroke(paint: gray, dash: "dashed")

  let read-ord = (
    ((0,0), none),
    ((1,0), 6),
    ((2,0), 3),
    ((0,1), 11),
    ((1,1), 9),
    ((3,3), 8),
    ((4,3), 5),
    ((5,3), 2),
    ((3,4), 10),
    ((6,6), none),
    ((7,6), none),
    ((8,6), 1),
    ((6,7), none),
    ((7,7), 7),
    ((8,7), 4),
    )

  box()[#cetz.canvas({
    for i in range(nr+1) {
      cetz.draw.line((0*s, i*s), (nc*s, i*s), stroke: a-grid)
    }
    for i in range(nc+1) {
      cetz.draw.line((i*s, 0*s), (i*s, nr*s), stroke: a-grid)
    }
    for i in range(-2,2) {
      cetz.draw.line((calc.max(0,-i)*s, calc.max(i,0)*s), (calc.min(nc, nr - i)*s, calc.min(nc+i, nr)*s), stroke: a-diag)
    }
    for ((x,y), v) in read-ord {
      cetz.draw.content(((x+0.5)*s,(y+0.5)*s), {if v==none [$times$] else [#v]})
    }
  })]

}

#let ao-1(ord) = {
  box()[
    #diagram(
      spacing: 1.5em,
      {
        node((0,0), [1], name: label("1"))
        node((1,0), [2], name: label("2"))
        node((2,0), [3], name: label("3"))
        node((3,0), [4], name: label("4"))
        edge(label("1"), label("2"), if ord.at(0) < ord.at(1) {"->-"} else {"-<-"})
        edge(label("1"), label("3"), if ord.at(0) < ord.at(2) {"->-"} else {"-<-"}, bend: +45deg)
        edge(label("1"), label("4"), if ord.at(0) < ord.at(3) {"->-"} else {"-<-"}, bend: +45deg)
        edge(label("3"), label("4"), if ord.at(2) < ord.at(3) {"->-"} else {"-<-"})
      }
    )
  ]
}

#let msf-1 = {
   box()[
    #diagram(
      spacing: 1em,
      {
        node((0,0), [4], name: label("3"))
        node((-0.5,1), [1], name: label("0"))
        node((0.5,1), [7], name: label("6"))
        node((-0.5,2), [2], name: label("1"))
        node((2,0), [5], name: label("4"))
        node((2,1), [6], name: label("5"))
        node((3,0), [3], name: label("2"))
        edge(label("3"), label("0"))
        edge(label("0"), label("1"))
        edge(label("3"), label("6"))
        edge(label("4"), label("5"))
      }
    )
  ]
}

#let msf-2(par) = {
  let stack = array(())
  stack.push((par.position(j => j==-1), (0,0)))
  box()[
    #diagram(
      spacing: 1em,
      {
        while stack.len() > 0 {
          let (i, (x,y)) = stack.pop()
          node((x,y), [#(i + 1)], name: label(str(i)))
          let chl = range(par.len()).filter(j => par.at(j)==i)
          for j in range(chl.len()) {
            edge(label(str(i)), label(str(chl.at(j))))
            stack.push((chl.at(j), (x - (chl.len()-1)/2 + j, y + 1)))
          }
        }
      }
    )
  ]
}

#let ig-mod-law-1 = {

  let h = (3,5,5,5,5)
  let n = h.len()
  let s = 12pt
  h.insert(0,0)

  let col-boxes = {
    let col-box(pos, color) = {
      cetz.draw.rect(((pos.at(0)-1)*s, (pos.at(1)-1)*s), (pos.at(0)*s, pos.at(1)*s), fill: color, stroke: none)
    }
    col-box((1,3), yellow)
    col-box((1,4), yellow)
    col-box((2,3), yellow)
    col-box((2,4), yellow)
    col-box((3,5), yellow)
    col-box((4,5), yellow)
    col-box((3,4), lime)
  }
  let grid = {
    for i in range(1, n){
      cetz.draw.line((0*s, i*s), (n*s, i*s), stroke: stroke(paint: gray))
      cetz.draw.line((i*s, 0*s), (i*s, n*s), stroke: stroke(paint: gray))
    }
  }
  let diag = {
    cetz.draw.line((0*s, 0*s), (n*s, n*s), stroke: stroke(dash: "dashed"))
  }
  let bounce-path = {
    cetz.draw.line((1*s, 3*s), (3*s, 3*s), stroke: stroke(paint: red))
    cetz.draw.line((3*s, 3*s), (3*s, 5*s), stroke: stroke(paint: red))
  }
  let frame = {
    cetz.draw.rect((0,0), (n*s, n*s))
  }
  let path = {
    for i in range(n){
      cetz.draw.line((i*s, h.at(i)*s), (i*s,h.at(i+1)*s), stroke: stroke(paint: blue, thickness: 2pt))
      cetz.draw.line((i*s, h.at(i+1)*s), ((i+1)*s,h.at(i+1)*s), stroke: stroke(paint: blue, thickness: 2pt))
      cetz.draw.content((-.5*s, (i+1)*s), [#(i+1)])
      cetz.draw.content(((i+1)*s, -.5*s), [#(i+1)])
    }
  }
  let mod-law-path = {
    cetz.draw.line((0*s, 2*s), (1*s, 2*s), stroke: stroke(paint: teal, thickness: 2pt))
    cetz.draw.line((1*s, 2*s), (1*s, 3*s), stroke: stroke(paint: teal, thickness: 2pt))
    cetz.draw.line((0*s, 3*s), (0*s, 4*s), stroke: stroke(paint: teal, thickness: 2pt))
    cetz.draw.line((0*s, 4*s), (1*s, 4*s), stroke: stroke(paint: teal, thickness: 2pt))
    cetz.draw.line((0*s, 2*s), (0*s, 3*s), stroke: stroke(paint: olive, thickness: 2pt))
    cetz.draw.line((0*s, 3*s), (1*s, 3*s), stroke: stroke(paint: olive, thickness: 2pt))
    cetz.draw.line((1*s, 3*s), (1*s, 4*s), stroke: stroke(paint: olive, thickness: 2pt))
  }

  box([#cetz.canvas({
    col-boxes
    grid
    diag
    bounce-path
    frame
    path
    mod-law-path
  })])
}

#let ig-mod-law-2 = {

  let h = (4,4,5,5,5)
  let n = h.len()
  let s = 12pt
  h.insert(0,0)

  let col-boxes = {
    let col-box(pos, color) = {
      cetz.draw.rect(((pos.at(0)-1)*s, (pos.at(1)-1)*s), (pos.at(0)*s, pos.at(1)*s), fill: color, stroke: none)
    }
    col-box((1,2), yellow)
    col-box((1,3), yellow)
    col-box((2,4), yellow)
    col-box((3,4), yellow)
    col-box((2,5), yellow)
    col-box((3,5), yellow)
    col-box((2,3), lime)
  }
  let grid = {
    for i in range(1, n){
      cetz.draw.line((0*s, i*s), (n*s, i*s), stroke: stroke(paint: gray))
      cetz.draw.line((i*s, 0*s), (i*s, n*s), stroke: stroke(paint: gray))
    }
  }
  let diag = {
    cetz.draw.line((0*s, 0*s), (n*s, n*s), stroke: stroke(dash: "dashed"))
  }
  let bounce-path = {
    cetz.draw.line((0*s, 2*s), (2*s, 2*s), stroke: stroke(paint: red))
    cetz.draw.line((2*s, 2*s), (2*s, 4*s), stroke: stroke(paint: red))
  }
  let frame = {
    cetz.draw.rect((0,0), (n*s, n*s))
  }
  let path = {
    for i in range(n){
      cetz.draw.line((i*s, h.at(i)*s), (i*s,h.at(i+1)*s), stroke: stroke(paint: blue, thickness: 2pt))
      cetz.draw.line((i*s, h.at(i+1)*s), ((i+1)*s,h.at(i+1)*s), stroke: stroke(paint: blue, thickness: 2pt))
      cetz.draw.content((-.5*s, (i+1)*s), [#(i+1)])
      cetz.draw.content(((i+1)*s, -.5*s), [#(i+1)])
    }
  }
  let mod-law-path = {
    cetz.draw.line((1*s, 4*s), (1*s, 5*s), stroke: stroke(paint: teal, thickness: 2pt))
    cetz.draw.line((1*s, 5*s), (2*s, 5*s), stroke: stroke(paint: teal, thickness: 2pt))
    cetz.draw.line((2*s, 4*s), (3*s, 4*s), stroke: stroke(paint: teal, thickness: 2pt))
    cetz.draw.line((3*s, 4*s), (3*s, 5*s), stroke: stroke(paint: teal, thickness: 2pt))
    cetz.draw.line((1*s, 4*s), (2*s, 4*s), stroke: stroke(paint: olive, thickness: 2pt))
    cetz.draw.line((2*s, 4*s), (2*s, 5*s), stroke: stroke(paint: olive, thickness: 2pt))
    cetz.draw.line((2*s, 5*s), (3*s, 5*s), stroke: stroke(paint: olive, thickness: 2pt))
  }

  box([#cetz.canvas({
    col-boxes
    grid
    diag
    bounce-path
    frame
    path
    mod-law-path
  })])
}

#let isf-1 = {
  box()[
    #diagram(
      spacing: 1.5em,
      {
        node((0,0), [1], name: label("1"))
        node((1,0), [2], name: label("2"))
        node((2,0), [3], name: label("3"))
        node((3,0), [4], name: label("4"))
        node((4,0), [5], name: label("5"))
        node((5,0), [6], name: label("6"))
        node((6,0), [7], name: label("7"))
        edge(label("1"), label("4"), bend: +45deg)
        edge(label("1"), label("5"), bend: +45deg)
        edge(label("1"), label("6"), bend: +45deg)
        edge(label("2"), label("3"))
        edge(label("4"), label("7"), bend: +45deg)
      }
    )
  ]
}

#let part-lis-0 = {

  let parts = (
    ("v", 1, [1]),
    ("v", 0, [2]),
    ("v", 2, [3]),
    ("v", 2, [4]),
    ("v", 0, [5]),
    ("m", 0, (0,1,0,1), ((0,1), (1,2), (2,3)), ([6], [7], [8], [9])))

  let n = 3
  let s = 24pt
  let box-size = .5
  let circle-rad = .1
  let text-size = .3
  let d1 = .75
  let d2 = .5

  let x = .5 + d1

  let base(x) = {
    for i in range(n){
      cetz.draw.content((0*s, i*s), [#(i+1)])
      cetz.draw.line((.5*s,i*s), (x*s, i*s), stroke: .5pt)
    }
  }

  let vertex(i, l, x) = {
    ( {
        cetz.draw.rect(((x - box-size/2)*s, (i - box-size/2)*s), ((x + box-size/2)*s, (i + box-size/2)*s))
        cetz.draw.circle((x*s, i*s), radius: circle-rad*s, fill: black)
        cetz.draw.content((x*s, (i + (if i > 0 {1} else {-1})*(box-size/2 + text-size))*s), l)
      }
    , x + d1
    )
  }

  let bicol(i, vs, es, ls, x) = {
    let m = vs.len()
    ( {
        cetz.draw.rect(((x - box-size/2)*s, (i - box-size/2)*s), ((x + (m - 1)*d2 + box-size/2)*s, (i + 1 + box-size/2)*s))
        for j in range(m) {
          cetz.draw.circle(((x+j*d2)*s, (i+vs.at(j))*s), radius: circle-rad*s, fill: black)
          cetz.draw.content(((x+j*d2)*s, (i+vs.at(j)+(vs.at(j)*2-1)*(box-size/2 + text-size))*s), ls.at(j))
        }
        for (j,l) in es {
          cetz.draw.line(((x+j*d2)*s, (i+vs.at(j))*s), ((x+l*d2)*s,(i+vs.at(l))*s))
        }
      }
    , x + d1 + (m - 1)*d2
    )
  }

  let render-parts(parts, x) = {
    let rs = ()
    for p in parts {
      let (r, xt) = {
        if p.at(0) == "v" {
          vertex(p.at(1), p.at(2), x)
        } else {
          bicol(p.at(1), p.at(2), p.at(3), p.at(4), x)
        }
      }
      x = xt
      rs.push(r)
    }
    (rs, x)
  }

  box()[#cetz.canvas({
    let (rs, x) = render-parts(parts, x)
    for r in rs {r}
    base(x)
  })]

}

#let part-lis-poset-0 = {

  let parts = (
    ("v", 1, [1]),
    ("v", 0, [2]),
    ("v", 2, [3]),
    ("v", 2, [4]),
    ("v", 0, [5]),
    ("m", 0, (0,1,0,1), ((0,1), (1,2), (2,3)), ([6], [7], [8], [9])))

  let n = 3
  let s = 24pt
  let box-size = .5
  let circle-rad = .1
  let text-size = .3
  let d1 = .75
  let d2 = .5

  let x = .5 + d1

  let vertex(i, l, x) = {
    let pos = ()
    let cnt = {
      cetz.draw.circle((x*s, i*s), radius: circle-rad*s, fill: black)
      pos.push((x, i))
      cetz.draw.content((x*s, (i + (if i > 0 {1} else {-1})*(box-size/2 + text-size))*s), l)
    }
    (cnt, pos, x + d1)
  }

  let bicol(i, vs, es, ls, x) = {
    let m = vs.len()
    let pos = ()
    let cnt = {
      for j in range(m) {
        cetz.draw.circle(((x+j*d2)*s, (i+vs.at(j))*s), radius: circle-rad*s, fill: black)
        pos.push((x+j*d2, i+vs.at(j)))
        cetz.draw.content(((x+j*d2)*s, (i+vs.at(j)+(vs.at(j)*2-1)*(box-size/2 + text-size))*s), ls.at(j))
      }
      for (j,l) in es {
        cetz.draw.line(((x+j*d2)*s, (i+vs.at(j))*s), ((x+l*d2)*s,(i+vs.at(l))*s))
      }
    }
    (cnt, pos, x + d1 + (m - 1)*d2)
  }

  let render-parts(parts, x) = {
    let cnt = ()
    let pos = ()
    for p in parts {
      let (_cnt, _pos, xt) = {
        if p.at(0) == "v" {
          vertex(p.at(1), p.at(2), x)
        } else {
          bicol(p.at(1), p.at(2), p.at(3), p.at(4), x)
        }
      }
      x = xt
      cnt.push(_cnt)
      pos.push(_pos)
    }
    (cnt, pos, x)
  }

  let add-edges(pos) = {
    for i in range(pos.len()) {
      for j in range(i+1, pos.len()) {
        for pi in pos.at(i) {
          for pj in pos.at(j) {
            if calc.abs(pi.at(1) - pj.at(1)) > 1 or pi.at(1) + 1 == pj.at(1) {
              cetz.draw.line((pi.at(0)*s, pi.at(1)*s), (pj.at(0)*s, pj.at(1)*s))
            }
          }
        }
      }
    }
  }

  box()[#cetz.canvas({
    let (cnt, pos, x) = render-parts(parts, x)
    for _cnt in cnt {_cnt}
    add-edges(pos)
  })]

}

#let sym-fun-pos-exp = {
  box()[
    #diagram(
      spacing: 1em,
      node((0,0), [$h_lambda$], name: label("h")),
      node((-1,0), [$e_lambda$], name: label("e")),
      node((-1,2), [$s_lambda$], name: label("s")),
      node((0,2), [$p_lambda$], name: label("p")),
      node((0,4), [$m_lambda$], name: label("m")),
      edge(label("h"), label("s")),
      edge(label("e"), label("s")),
      edge(label("h"), label("p"), "--"),
      edge(label("s"), label("m")),
      edge(label("p"), label("m")),
    )
  ]
}

#let qsym-fun-pos-exp = {
  box()[
    #diagram(
      spacing: 1em,
      node((0,0), [$h_lambda$], name: label("h")),
      node((-1,0), [$e_lambda$], name: label("e")),
      node((-1,2), [$s_lambda$], name: label("s")),
      node((0,2), [$p_lambda$], name: label("p")),
      node((0,4), [$m_lambda$], name: label("m")),
      node((-3,4), [$F_alpha$], name: label("F")),
      node((-1,4), [$Psi_alpha$], name: label("P")),
      node((-1,6), [$M_alpha$], name: label("M")),
      edge(label("h"), label("s")),
      edge(label("e"), label("s")),
      edge(label("h"), label("p"), "--"),
      edge(label("s"), label("m")),
      edge(label("p"), label("m")),
      edge(label("s"), label("F"), ".."),
      edge(label("p"), label("P"), ".."),
      edge(label("m"), label("M"), ".."),
      edge(label("F"), label("M")),
      edge(label("P"), label("M"), "--"),
    )
  ]
}
