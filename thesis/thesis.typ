#import "misc/settings.typ": *
#import "misc/drawings.typ"

#show: document_settings
#show: default_settings

#set page(numbering: none)
#include "misc/titlepage.typ"

#set page(numbering: "i")
#counter(page).update(1)
#outline()
#include "chapters/0.typ"

#set page(numbering: "1")
#counter(page).update(1)
#include "chapters/1.typ"
#include "chapters/2.typ"
#include "chapters/3.typ"
#include "chapters/4.typ"
#include "chapters/5.typ"
#include "chapters/6.typ"

#bibliography("misc/bibliography.bib", style: "misc/ieee-sorted-by-author.csl")