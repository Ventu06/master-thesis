#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

#set heading(supplement: [chapter])
= From chromatic polynomials to the Stanley-Stembridge conjecture

This chapter is meant to be almost a historical introduction to the problem we want to address in the following chapters.
We introduce the objects involved, showing various examples and properties that, although often not necessary to continue with the discussion, motivate subsequent choices and show the evolution of a theory that slowly becomes more and more complex.

We begin by discussing the chromatic polynomial, the deletion-contraction relation and some formulas for expressing it.
In the second section, these are generalized to chromatic symmetric function, modular law (which, however, we state explicitly only in the second chapter) and formulas that include bases of symmetric functions.
Right here we introduce the concept of positivity of a symmetric function, one of the two ingredients for formulating the Stanley-Stembridge conjecture.
The other ingredient, addressed in the third section, concerns particular categories of posets and graphs.
Finally, in the last section, we state this conjecture and see the motivations that make it interesting and some results concerning it and related problems.

As in the transition from the chromatic polynomial to the chromatic symmetric function, in the third chapter, we make a further generalization to the chromatic quasisymmetric function.
For this reason, some results and identities in this chapter take on greater significance later.

Finally, some basic knowledge of symmetric functions and, optionally, also of their links to representation theory is necessary to tackle this chapter.
For this reason, we recommend that the reader unfamiliar with these topics read the first two sections of the appendix.

#include "1-1.typ"
#include "1-2.typ"
#include "1-3.typ"
#include "1-4.typ"
