#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

#set heading(supplement: [chapter])
= The Guay-Paquet proof

As anticipated in the previous chapter, the most important result obtained regarding the Stanley-Stembridge conjecture lies in the reduction of the class of posets to be checked, from $(3+1)$-free posets to both $(3+1)$-free and $(2+2)$-free posets.
This result was obtained by Guay-Paquet in @guay-paquet_modular_2013.
The purpose of this chapter is precisely to go over this proof, but at the same time to isolate some ideas and theories that we will meet again later.

The first of the three sections is devoted to part listings, a representation of $(3+1)$-free posets also introduced by Guay-Paquet.
This section extends the one on posets from the previous chapter while remaining more specific, substantial, and less useful in the future to be incorporated into the previous one.

The second concerns the modular law, a local linear relation between chromatic symmetric functions of graphs that generalizes the deletion-contraction relation.
This is introduced in the general case and then specialized in the case of part listings.
It is later taken up in the fifth chapter, where we try to generalize it to some special cases to obtain additional results.

Finally, the third section contains the actual proof, which makes use of more technical and specific tools in order to obtain the theorem we are so interested in.

#include "2-1.typ"
#include "2-2.typ"
#include "2-3.typ"
