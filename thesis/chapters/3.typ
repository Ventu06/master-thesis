#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

#set heading(supplement: [chapter])
= Chromatic quasisymmetric functions

The topic of this chapter is the so-called chromatic quasisymmetric functions, that is, a generalization of the chromatic symmetric functions seen in the first chapter.
We study such functions mainly on unit interval graphs, which we recall to be the incomparability graphs of both $(3+1)$-free and $(2+2)$-free posets.

These new functions are a $q$-analogue of the previous ones, that is, a variant in which we have introduced a new variable $q$ that takes into account an additional statistic.
In the first section, we look in more detail at the definition, some examples and formulas that generalize those stated for the chromatic symmetric functions.

The second section concerns the Shareshian-Wachs conjecture, an analog of the Stanley-Stembridge conjecture in the quasisymmetric case, which, thanks to the Guay-Paquet result seen in the previous chapter, implies the latter.
Similar to what was done for the Stanley-Stembridge conjecture, we enunciate results on similar problems, results on particular cases and state some general ideas on how this problem can be approached.

In the last two sections, we investigate the links between chromatic quasisymmetric functions and other fields of mathematics.
In the third one, we deal with Hessenberg varieties and the relation between these functions and a particular action of the symmetric group on the cohomology groups of these varieties.
In the fourth one, we discuss LLT polynomials, how they can be related to colorings of graphs in the unicellular case, and a curious duality between these polynomials and chromatic quasisymmetric functions, given by the Carlsson-Mellit identity.

Basic knowledge of quasisymmetric functions is necessary to deal with this chapter and subsequent chapters, so we recommend reading the third section of the appendix for an introduction to these topics if necessary.

#include "3-1.typ"
#include "3-2.typ"
#include "3-3.typ"
#include "3-4.typ"
