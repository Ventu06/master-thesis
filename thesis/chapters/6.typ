#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

#set heading(supplement: [appendix])
= Symmetric and quasisymmetric functions

The purpose of this appendix is to give the mathematical tools necessary to deal with the rest of the thesis and to fix some standard notations.
The theory we show mainly concerns symmetric functions, the connection between them and the theory of symmetric group representations, and finally quasisymmetric functions.
These topics are not too extensive or complex, nor do they require any special prerequisites, so we believe that they can be tackled even by a reader not experienced in the field.
Given the purpose of this chapter, we have decided to state the main facts omitting their proofs, which can easily be found in the books and articles we are going to point out.
Instead, we have decided to show some constructions explicitly, both as a matter of completeness and to show ideas that aid intuition and lend themselves to being reused.

#include "6-1.typ"
#include "6-2.typ"
#include "6-3.typ"
