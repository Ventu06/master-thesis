#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Modular law revisited

A few chapters ago, during the exposition of Guay-Paquet's proof, we introduced a linear relation on chromatic symmetric functions, the modular law (@lmm-csf-mod-law).
It seems natural to ask whether this identity extends to chromatic quasisymmetric functions.
In general, the answer is negative, however, we see some special cases in which a generalization of this identity applies.
This result allows us to obtain relatively deep formulas for expressing the chromatic quasisymmetric function.

We begin by recalling the modular law.
Let $G$ be a graph and let $v$, $w_1$ and $w_2$ be vertices of $G$, all connected to each other.
Let $e_1$ and $e_2$ be the edges connecting $v$ to $w_1$ and $w_2$ respectively.
By setting $G_1 = G backslash e_1$, $G_2 = G backslash e_2$ and $G_(12) = G backslash e_1 backslash e_2$, we have
$
  chi_G + chi_(G_(12)) = chi_(G_1) + chi_(G_2) med.
$

In the proof of the modular law we checked, for each coloring, the contribution of that to both members of the identity.
Let us try to reuse this idea, this time counting the various coinversions.

We require that the vertices of $G$ be labeled with the numbers $1$ to $n$.
Without loss of generality, suppose $w_1 < w_2$.
First, we distinguish three cases: $v < w_1 < w_2$, $w_1 < v < w_2$ and $w_1 < w_2 < v$.

Actually, it is sufficient to study the first two, as we can trace the third back to the first.
In fact, we denote by $G^"rev"$ the graph $G$ in which at the node with label $i$ we have substituted the label $n+1-i$.
Then $chi_(G^"rev")^q (X;q) = q^(\#lr(|E|)) chi_G^q (X; q^(-1))$, so we can find a correspondence between the linear relations between $chi_G^q$, $chi_(G_1)^q$, $chi_(G_2)^q$ and $chi_(G_(12))^q$, and those between $chi_(G^"rev")^q$, $chi_((G_1)^"rev")^q$, $chi_((G_2)^"rev")^q$ and $chi_((G_(12))^"rev")^q$.
It follows that, unless we reverse the order of the labels and exchange $w_1$ for $w_2$, we can assume that $v$ is not the largest of the three vertices chosen.

Suppose now that $v < w_1 < w_2$ and let us fix a coloring $kappa$.
We can assume that $kappa$ is a proper coloring of $G_(12)$, otherwise its contribution is zero in all terms.
Let us fix the following notations:
$ kappa_v = kappa(v) med, quad kappa_1 = kappa(w_1) med, quad kappa_2 = kappa(w_2) med, \
  kappa_"min" = min(kappa(w_1), kappa(w_2)) med, quad kappa_"max" = max(kappa(w_1), kappa(w_2)) med.
$
We now go on to study the contribution made by any edges between vertices $v$, $w_1$ and $w_2$, distinguishing several cases.
#figure()[#table(
  columns: 5,
  stroke: none,
  align: horizon+center,
  table.hline(),
  table.header([$v < w_1 < w_2$], table.vline(stroke: .5pt), [$G$], [$G_1$], [$G_2$], [$G_(12)$]),
  table.hline(),
  [$kappa_v < kappa_1 < kappa_2$], [$q^3$], [$q^2$], [$q^2$], [$q^1$],
  [$kappa_v < kappa_2 < kappa_1$], [$q^2$], [$q$], [$q$], [$1$],
  table.hline(stroke: .5pt),
  [$kappa_1 < kappa_v < kappa_2$], [$q^2$], [$q^2$], [$q$], [$q$],
  [$kappa_2 < kappa_v< kappa_1$], [$q$], [$1$], [$q$], [$1$],
  table.hline(stroke: .5pt),
  [$kappa_1 < kappa_2 < kappa_v$], [$q$], [$q$], [$q$], [$q$],
  [$kappa_2 < kappa_1 < kappa_v$], [$1$], [$1$], [$1$], [$1$],
  table.hline(stroke: .5pt),
  [$kappa_v = kappa_1 < kappa_2$], [$0$], [$q^2$], [$0$], [$q$],
  [$kappa_v = kappa_2 < kappa_1$], [$0$], [$0$], [$q$], [$1$],
  table.hline(stroke: .5pt),
  [$kappa_1 < kappa_2 = kappa_v$], [$0$], [$0$], [$q$], [$q$],
  [$kappa_2 < kappa_1 = kappa_v$], [$0$], [$1$], [$0$], [$1$],
  table.hline(),
)]

We can repeat the procedure in the case $w_1 < v < w_2$, obtaining the following contributions.
#figure()[#table(
  columns: 5,
  stroke: none,
  align: horizon+center,
  table.hline(),
  table.header([$w_1 < v < w_2$], table.vline(stroke: .5pt), [$G$], [$G_1$], [$G_2$], [$G_(12)$]),
  table.hline(),
  [$kappa_v < kappa_1 < kappa_2$], [$q^2$], [$q^2$], [$q$], [$q$],
  [$kappa_v < kappa_2 < kappa_1$], [$q$], [$q$], [$1$], [$1$],
  table.hline(stroke: .5pt),
  [$kappa_1 < kappa_v < kappa_2$], [$q^3$], [$q^2$], [$q^2$], [$q$],
  [$kappa_2 < kappa_v < kappa_1$], [$1$], [$1$], [$1$], [$1$],
  table.hline(stroke: .5pt),
  [$kappa_1 < kappa_2 < kappa_v$], [$q^2$], [$q$], [$q^2$], [$q$],
  [$kappa_2 < kappa_1 < kappa_v$], [$q$], [$1$], [$q$], [$1$],
  table.hline(stroke: .5pt),
  [$kappa_v = kappa_1 < kappa_2$], [$0$], [$q^2$], [$0$], [$q$],
  [$kappa_v = kappa_2 < kappa_1$], [$0$], [$0$], [$1$], [$1$],
  table.hline(stroke: .5pt),
  [$kappa_1 < kappa_2 = kappa_v$], [$0$], [$0$], [$q^2$], [$q$],
  [$kappa_2 < kappa_1 = kappa_v$], [$0$], [$1$], [$0$], [$1$],
  table.hline(),
)]

Unfortunately, in neither of the previous two tables do we have a linear dependence relation (on $QQ(q)$) among the four columns, so we can say that there is no analog of the modular law in the same generality.

Let us now treat a more special case: given a graph $G$, we say that two vertices $w_1$ and $w_2$ are indistinguishable if, for every vertex $z$ other than $w_1$ and $w_2$, there is an edge between $z$ and $w_1$ if and only if there is an edge between $z$ and $w_2$.
Note that in a graph $G$ with two indistinguishable vertices $w_1$ and $w_2$, to each proper coloring $kappa$ we can associate the proper coloring $kappa'$ in which we swap the colors of $w_1$ and $w_2$.
Note that the function $kappa |-> kappa'$ is an involution and if $w_1$ and $w_2$ are connected by an edge then there are no fixed points.
Moreover, the only coinversions that are changed are those between vertices whose label lies weakly between $w_1$ and $w_2$.

Let us now return to the first case, where $v$, $w_1$ and $w_2$ are all connected to each other and $v < w_1 < w_2$.
Suppose that $w_1$ and $w_2$ are indistinguishable and consecutive, i.e. $w_1 = w_2 - 1$.
By the previous reasoning, we can pair the colorings and the only differences in the contribution to the chromatic quasisymmetric function are due to the edge between $w_1$ and $w_2$, which we already took into account in the first table.
Adding up the contributions of the paired colorings we obtain the following table.
#figure()[#table(
  columns: 5,
  stroke: none,
  align: horizon+center,
  table.hline(),
  table.header([$v < w_1 = w_2 - 1$], table.vline(stroke: .5pt), [$G$], [$G_1$], [$G_2$], [$G_(12)$]),
  table.hline(stroke: .5pt),
  [$kappa_v < kappa_"min"$], [$q^2(1+q)$], [$q(1+q)$], [$q(1+q)$], [$1+q$],
  [$kappa_"min" < kappa_v < kappa_"max"$], [$q(1+q)$], [$1+q^2$], [$2q$], [$1+q$],
  [$kappa_"max" < kappa_v$], [$1+q$], [$1+q$], [$1+q$], [$1+q$],
  [$kappa_v = kappa_"min"$], [$0$], [$q^2$], [$q$], [$1+q$],
  [$kappa_"max" = kappa_v$], [$0$], [$1$], [$q$], [$1+q$],
  table.hline(),
)]

Similarly, we can consider the case where $w_1 + 1 = v = w_2 - 1$.
Starting from the second table and summing over the paired colorings, we obtain the following contributions.
#figure()[#table(
  columns: 5,
  stroke: none,
  align: horizon+center,
  table.hline(),
  table.header([$w_1 + 1 = v = w_2 - 1$], table.vline(stroke: .5pt), [$G$], [$G_1$], [$G_2$], [$G_(12)$]),
  table.hline(stroke: .5pt),
  [$kappa_v < kappa_"min"$], [$q(1+q)$], [$q(1+q)$], [$1+q$], [$1+q$],
  [$kappa_"min" < kappa_v < kappa_"max"$], [$q^3+1$], [$1+q^2$], [$1+q^2$], [$1+q$],
  [$kappa_"max" < kappa_v$], [$q(1+q)$], [$1+q$], [$q(1+q)$], [$1+q$],
  [$kappa_v = kappa_"min"$], [$0$], [$q^2$], [$1$], [$1+q$],
  [$kappa_"max" = kappa_v$], [$0$], [$1$], [$q^2$], [$1+q$],
  table.hline(),
)]

Unfortunately, even in the last table, the columns are linearly independent.
Instead, in the third table there is a linear relation between the columns, that is, $C_G + q C_(G_(12)) = (1+q) C_(G_2)$.
Summing over all pairs of colorings we get the following.

#lemma(name: [Modular law for chromatic quasisymmetric functions, case $v < w_1, w_2$])[
  Let $G$ be a graph and let $v$, $w_1$ and $w_2$ be vertices of $G$, all connected to each other and such that $v < w_1 = w_ 2 - 1$.
  We further assume that $w_1$ and $w_2$ are indistinguishable.
  Let $e_1$ and $e_2$ be the edges connecting $v$ to $w_1$ and $w_2$ respectively.
  Then the following relation holds:
  $
    chi_G^q + q chi_(G_(12))^q = (1+q) chi_(G_2)^q med.
  $
]<lmm-csfq-mod-law-lt>

Using the duality $chi_(G^"rev")^q (X;q) = q^(\#lr(|E|)) chi_G^q (X; q^(-1))$ introduced earlier, we obtain the following symmetric result.
#lemma(name: [Modular law for chromatic quasisymmetric functions, case $v > w_1, w_2$])[
  Let $G$ be a graph and let $v$, $w_1$ and $w_2$ be vertices of $G$, all connected to each other and such that $w_1 + 1 = w_ 2 < v$.
  We further assume that $w_1$ and $w_2$ are indistinguishable.
  Let $e_1$ and $e_2$ be the edges connecting $v$ to $w_1$ and $w_2$ respectively.
  Then the following relation holds:
  $
    chi_G^q + q chi_(G_(12))^q = (1+q) chi_(G_1)^q med.
  $
]<lmm-csfq-mod-law-gt>

Note that, in both situations, by specializing $q |-> 1$ and observing that $chi_(G_1) = chi_(G_2)$, we get back the modular law $chi_G + chi_(G_(12)) = chi_(G_1) + chi_(G_2)$.

Using the Carlsson-Mellit identity (@thm-cqsf-rel-llt), we obtain the following linear relations between LLT polynomials in the two cases, respectively:
$
    "LLT"_G + q "LLT"_(G_(12)) = (1+q) "LLT"_(G_2)^q med; \
    "LLT"_G + q "LLT"_(G_(12)) = (1+q) "LLT"_(G_1)^q med.
$
Clearly, such relations can be shown even without the use of the Carlsson-Mellit identity and were already present in articles such as @lee_linear_2021[Theorem 3.5], @huh_melting_2020[Theorem 3.1] and @alexandersson_llt_2021[Corollary 20, Proposition 23].

Let us now give a universal definition for the modular law.
#definition(name: [Modular law])[
  Given a certain subset of the graphs on $n$ vertices $cal(G)$ and a $QQ(q)$-algebra $cal(A)$, we say that a certain function $f : cal(G) -> cal(A)$ satisfies the modular law if
  $
    f(G) + q f(G backslash e backslash overline(e)) = (1+q) f(G backslash e)
  $
  holds in the following cases.
  - In the graph $G$ there are three vertices $v$, $w_1$ and $w_2$, all connected to each other and such that $v < w_1 = w_ 2 - 1$ and that $w_1$ and $w_2$ are indistinguishable.
    In this case we set $e = {v, w_2}$ and $overline(e) = {v, w_1}$.
  - In the graph $G$ there are three vertices $v$, $w_1$ and $w_2$, all connected to each other and such that $w_1 + 1 = w_ 2 < v$ and that $w_1$ and $w_2$ are indistinguishable.
    In this case we set $e = {v, w_1}$ and $overline(e) = {v, w_2}$.
]<def-mod-law>

Thus we can summarize the previous discussion by saying that the chromatic quasisymmetric function and LLT polynomials satisfy the modular law.

In the rest of this section, we want to study the modular law and its consequences in the case where $cal(G)$ is the set of unit interval graphs on $n$ vertices.
Let us begin by reformulating the definition of modular law in this specific case.
Recall that unit interval graphs are indexed by the Dyck paths, that is, by the weakly increasing functions $h : [n] -> [n]$ that are always above the diagonal.
Having called $cal(D)$ the set of these functions, we directly consider the functions from $cal(D)$ in a $QQ(q)$ -algebra $cal(A)$.
#definition(name: [Modular law for unit interval graphs])[
  A certain function $f : cal(D) -> cal(A)$ satisfies the modular law if
  $
    f(h_2) + q f(h_0) = (1+q) f(h_1)
  $
  holds in the following cases.
  - There exists $i in [n-1]$ such that $h_1(i-1) < h_1(i) < h_1(i+1)$ (where $h(0)=0$) and $h_1(h_1(i)) = h_1(h_1(i)+1)$.
    Moreover $h_0(j) = h_1(j) = h_2(j)$ for every $j eq.not i$, while $h_0(i) = h_1(i)-1$ and $h_2(i) = h_1(i)+1$.
  - There exists $i in [n-1]$ such that $h_1(i+1) = h_1(i)+1$ and $h_1^(-1)(i) = emptyset$.
    Moreover $h_0(j) = h_1(j) = h_2(j)$ for every $j eq.not i, i+1$, while $h_0(i) = h_0(i+1) = h_1(i)$ and $h_2(i) = h_2(i+1) = h_1(i+1)$.
]<def-mod-law-uig>

In the former case the roles of $v$, $w_1$ and $w_2$ are filled by $i$, $h(i)$ and $h(i+1)$, respectively, while in the latter case by $h(i)$, $i$ and $i+1$.

To get a more direct intuition, we suggest looking at @fig-ig-mod-law.
Note how $w_1$ and $w_2$ are connected (lime-colored cells) and indistinguishable (yellow-colored cells).
The three paths $h_0$, $h_1$ and $h_2$ differ in olive green and light blue colored steps.
#figure(caption: [Modular law for unit interval graphs.])[
  #grid(
    columns: 2,
    column-gutter: 6em,
    [#drawings.ig-mod-law-1],
    [#drawings.ig-mod-law-2],
  )
]<fig-ig-mod-law>

For a function $f$ that satisfies the modular law, one might wonder over which unit interval graphs it is necessary to define $f$ so that its value can be determined over all unit interval graphs by repeatedly applying the modular law.
This question was answered by Abreu and Nigro in the following way.
#theorem(name: [@abreu_chromatic_2021[Theorem 1.2]])[
  Let $f$ be a function from $cal(D)$ in a $QQ(q)$ algebra $cal(A)$ that satisfies the modular law, then $f$ is determined by the values it takes in the disjoint union of complete graphs $K_lambda = K_(lambda_1) union.sq dots union.sq K_(lambda_(l(lambda)))$ as $lambda tack.r n$, or in other words, in
  $h = \( underbrace(lambda_1\, dots\, lambda_1, lambda_1), underbrace(lambda_1+lambda_2\, dots\, lambda_1+lambda_2, lambda_2), dots, underbrace(n\, dots\, n, lambda_(l(lambda))) \)$.
]<thm-mod-law-det-cmp>
To prove this theorem, they define an algorithm that repeatedly applies the modular law until they reduce to base cases, where the graph is a disjoint union of complete graphs, this time unordered by size (and thus indexed by compositions of $n$).
At this point, they conclude by showing that $f$ does not depend on the order in which these graphs are considered.

Let us take a look at some consequences of this result.

A first idea is to study the coefficients by which $f(G)$, for a generic unit interval graph $G$, is expressed as a function of $f(K_lambda)$ for $lambda tack.r n$.
In special cases, such as when the complementary graph $G$ is bipartite, it can be verified that these coefficients are positive.
Taking $f(G) = chi_G^q (X;q)$ and noting that $chi_(K_lambda)^q (X;q) = lambda_q ! dot e_lambda = product_(i=1)^(l(lambda)) [lambda_i]_q ! dot e_lambda$ we can conclude that the Shareshian-Wachs conjecture is verified for this particular class of graphs.

We note how $chi_(K_lambda)^q (X;q)$, as $lambda tack.r n$ varies, is a rescaling of the elementary basis, but this does not require that the coefficients appearing in the expression of $chi_G^q (X;q)$ as a function of $chi_(K_lambda)^q (X;q)$ be positive for the Shareshian-Wachs conjecture to be valid; indeed, in general they are not even polynomials but only rational functions.

Before moving on to the next section, let us see one last result.
Let $V_n$ be the space of functions satisfying the modular law on the unit interval graphs of $n$ vertices.
By the result seen just now, all these functions are linearly determined in the same way by the values they take on the $K_lambda$ for $lambda tack.r n$, so $V_n$, as a vector space, has dimension at most $\#lr(|{lambda tack.r n}|)$.
On the other hand, if for a unit interval graph $G$, we write $chi_G^q (X;q) = sum_(lambda tack.r n) chi_lambda^q (G) e_lambda$, we can observe that the various $chi_lambda^q$ satisfy the modular law.
Moreover $f_lambda (K_mu) = lambda_q ! delta_(lambda, mu)$, consequently the various $f_lambda$ are linearly independent and thus generate $V_n$, which has as its dimension just the number of partitions of $n$.
We conclude that, for a function $f$ that satisfies the modular law, we can write
$
  f(G) = sum_(lambda tack.r n) (chi_lambda^q (G)) / (lambda_q !) f(K_lambda) med.
$
The latter identity provides us with further motivation for the study of chromatic quasisymmetric functions.
