#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Hessenberg varieties

Algebraic combinatorics is a branch in which connections with other branches, mainly algebra and geometry, are relatively often found.
Sometimes, the description of algebraic or geometric objects can be traced back to the study of a finite structure that governs the behavior of the entire object.
It then becomes natural to approach the study of this structure combinatorially.
Examples of these phenomena can easily be found in representation theory, for example, the study of representations of the symmetric group or the general linear group can be traced back, in a sense, to the study of Young's Tableaux.
A more complex example is root systems, which are fundamental in the theory of Lie algebras and groups, algebraic groups and other similar objects.
Sometimes a different phenomenon also occurs, in which one finds that combinatorial structures of one's own interest have relations with other mathematical objects, or even such objects are constructed in such a way as to have an additional algebraic or geometric structure on which to make considerations.
In the latter case, we often speak of #emph[categorification], and it is essentially the reverse process of the one just described above.

In our case, the chromatic quasisymmetric functions of unit interval graphs have an interesting relation with the cohomology of Hessenberg varieties.
This relation was first conjectured by Shareshian and Wachs in @shareshian_chromatic_2012 and in @shareshian_chromatic_2016, and was known for a while as #emph[Shareshian-Wachs conjecture], before being proved independently and by different methods by Brosnan and Chow in @brosnan_unit_2018 and by Guay-Paquet in @guay-paquet_second_2016.
This relation has proved useful in the study of (our) Shareshian-Wachs conjecture, e.g., from it immediately descends the Schur positivity of the chromatic quasisymmetric function of unit interval graphs and, as mentioned in the previous section, the positivity of the coefficients of elementary basis elements associated with partitions of length at most two (see @abreu_chromatic_2021).

Finally, such a relation, together with the interest in $e$-positivity and in the Stanley-Stembridge conjecture, is one of the main reasons motivating the study of chromatic quasisymmetric functions.
Although we did not focus too much on this fact in the pre-thesis work, we still think it is important to introduce and discuss it briefly.

We begin, therefore, by introducing the Hessenberg varieties.

Given $n in NN$, the #emph[flag variety] of rank $n$ on $CC$ (briefly, $cal(F)(CC^n)$) is the set of complete flags of vector spaces
$
  cal(F)(CC^n) = { {0} = F_0 subset F_1 subset dots subset F_n = CC^n mid(|) op("dim")(F_i) = i "for" 0 <= i <= n }
$
on which a projective variety structure is defined (e.g., as a Grassmannian product subspace).
Given $G$ the general linear group $op("GL")(n, CC)$ and $B$ its Borel subgroup of upper triangular matrices, we have an isomorphism of varieties $cal(F)(CC^n) tilde.equiv G slash B$.

Let us now consider a Hessenberg function, i.e., a weakly increasing $h : [n] -> [n]$ function such that $h(i) >= i$ for $1 <= i <= n$, which we recall indexes unit interval graphs and is in bijection with Dyck paths.
We also consider a linear function $M : CC^n -> CC^n$, semisimple (i.e., diagonalizable) and regular (i.e., with distinct eigenvalues).
#definition(name: [Hessenberg variety])[
  The #emph[Hessenberg variety] associated with $(h, M)$ is the closed subvariety of $cal(F)(CC^n)$ given by
  $
    cal(H)(h) = cal(H)(h,M) = {F in cal(F)(CC^n) mid(|) M(F_i) subset F_(h(i)) "for" 1 <= i <= n} med.
  $
]
The choice of $M$ is not important, particularly as $M$ varies, the varieties turn out to be isomorphic, so we omit the dependence.

Let us look at a couple of examples:
- if $h = (n, n, dots, n)$, the conditions $M(F_i) subset F_(h(i)) = CC^n$ are trivial, so we have $cal(H)(h) = cal(F)(CC^n)$;
- if $h = (1, 2, dots, n)$, given $v_1, v_2, dots, v_n$ the eigenvectors of $M$, we have that every $F in cal(H)(h)$ must be of the type $({0} subset angle.l v_(sigma(1)) angle.r subset angle.l v_(sigma(1)), v_(sigma(2)) angle.r subset dots subset CC^n)$ for some $sigma in frak(S)_n$, so $cal(H)(h)$ is disjoint union of $n!$ points, which we identify with $frak(S)_n$.

Fixed a Hessenberg variety given by $(h,M)$, we consider the centralizer of $M$ in $G$, called $T$, which is easily seen to be a maximal torus.
$T$ acts on $cal(H)(h)$ by left translation, so let us consider the $0$-dimensional and $1$-dimensional orbits of this action.
The former corresponds to the points fixed by the action of $T$ and thus also by the action of $M$, i.e., they are a subset of $cal(H)((1,2, dots, n)) = frak(S)_n$ as seen in the previous example.
It is immediately seen that the dots in this set are fixed by $T$, so we identify the $0$-orbits with $frak(S)_n$.
Then it can be shown that there are exactly two fixed points in the closure of the $1$-orbits.

At this point, we can construct the #emph[moment graph], i.e., a graph whose vertices are the $0$-orbits ($V = frak(S)_n$) and whose edges have the two fixed points in the closure of the $1$-orbits as their extremes.
From this graph, we can construct a particular ring of polynomials, and the latter is sufficient to determine the cohomology groups of $cal(H)(h)$.
Again through the graph of moments, we define an action of $frak(S)_n$ on such a ring and consequently obtain a representation of $frak(S)_n$ on the cohomology groups of $cal(H)(h)$, called #emph[Tymoczko's representation].
For the details of this last part, we refer to @tymoczko_permutation_2007 and @tymoczko_permutation_2008.

We can now state the relation between chromatic quasisymmetric function and Hessenberg varieties.
#theorem(name: [(Old) Shareshian-Wachs conjecture (proved in @brosnan_unit_2018 and in @guay-paquet_second_2016)])[
  Let $h$ be a Hessenberg function and let $G$ be the associated unit interval graph.
  Then
  $
    omega(chi_G^q (X;q)) = sum_(i=0)^(\#lr(|E|)) q^i op("Frob") H^(2i)(cal(H)(h))
  $
  where $op("Frob") H^(2i)(cal(H)(h))$ is the image through Frobenius isomorphism of the character associated with Tymoczko's representation.
]<thm-cqsf-ch-hess>

As anticipated, this problem has been addressed in different ways by Brosnan and Chow, and by Guay-Paquet.

The first two proved a relation between the character of Tymoczko's representation and the Betti numbers of Hessenberg varieties, this time, however, defined with the transformation $M$ regular but no longer necessarily semisimple (see @brosnan_unit_2018[Theorem 4]).
At this point they were able to link a combinatorial description of the Betti numbers already established by Tymoczko in @tymoczko_linear_2006 with the combinatorics of chromatic quasisymmetric functions, finally arriving at the identity sought.
The proof in question makes extensive use of advanced geometric techniques, drawn, for example, from the theory of local systems and perverse sheaves, and is not easily accessible with only combinatorial knowledge.

The second proof, instead of treating Hessenberg varieties as a single object, attempts to deconstruct them recursively.
To accomplish this task, a new Hopf algebra is introduced, which serves as a backbone to perform the recursion.
In this way, part of the complexity of the problem has been shifted towards the study of this algebra.
But this turns out to have strong links with Dyck paths, thus providing the sought-after link between Hessenberg varieties and chromatic quasisymmetric functions.
As one can imagine from the description, this proof is more easily approached without advanced geometric tools, and it is in a sense complementary to Brosnan and Chow's, highlighting different aspects of Hessenberg varieties.

Although this conjecture has been closed, Hessenberg varieties have not yet been fully studied, and it is possible to expect that in the future new results, possibly also obtained through geometric means, may be transferred to chromatic quasisymmetric functions.
In particular, we can formulate a conjecture equivalent to the (new) Shareshian-Wachs conjecture concerning only these varieties (see also @prop-frob-id-h).
