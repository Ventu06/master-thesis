#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Chromatic polynomials

Graphs are one of the simplest and most interesting structures in mathematics.
We can represent a (finite) graph $G$ as a pair $(V,E)$, where $V$ is a finite set of elements called #emph[vertices] and $E$ is a set of unordered pairs of vertices, called #emph[edges].
Throughout the thesis, our graphs are simple, that is, there are no multiple edges or edges connecting a vertex to itself (loops).
Moreover, edges are not oriented, unless stated otherwise.

A #emph[coloring] $kappa$ of a graph $G$ is a function $kappa : V -> NN^+$, that is, a choice of a positive natural number, which in this case represents the color, for each vertex.
A coloring $kappa$ is said to be #emph[proper] if vertices connected by an edge have different colors, that is, if $kappa(v) eq.not kappa(w)$ for every edge ${v,w}$.
A coloring $kappa$ is said to be an $r$-coloring or a coloring with $r$ colors, if it uses at most the first $r$ colors, that is, $1 <= kappa(v) <= r$ per every vertex $v$.
For a certain graph $G$, let $op("PC")(G)$ the set containing all proper colorings of $G$, and let $op("PC")(G,r)$ the subset of $op("PC")(G)$ containing only the colorings with $r$ colors.

Look at @fig-graph-0-col for an example of a proper $3$-coloring of the graph $G = ({1,2,3,4}, {{1,2},{1,3},{1,4},{3,4}})$: the colors are placed inside the vertices, while the labels of the vertices are placed on their border.

#figure(
  drawings.graph-0-col,
  caption: [A proper $3$-coloring of a graph.]
)<fig-graph-0-col>

Graph colorings give rise to several interesting problems in combinatorics, including the well-known four-color theorem.

In this context, it is natural to introduce a statistic that counts the number of colorings with $r$ colors.
This function is called chromatic polynomial and was first considered by Birkhoof in @birkhoff_determinant_1912 in an attempt to prove precisely the four-color theorem.

#definition(name: [Chromatic polynomial])[
  The #emph[chromatic polynomial] $p_G (r)$ of a graph $G$ is the function counting proper colorings with $r$ colors, that is
  $
    p_G (r) = \#lr(|op("PC")(G, r)|) med.
  $
]<def-cp>

As guessable from the name, this function is indeed a polynomial in the number of colors.

In the case $G$ is the graph $E_n$ with $n$ vertices and no edges, we can notice that
$
  p_(E_n) (r) = r^n
$
which is indeed a polynomial.

To show that this last fact holds in the general case, we need the following identity.
#lemma(name: [Deletion-contraction relation])[
  Let $G = (V,E)$ a graph and let $e in E$, then
  $
    p_G (r) = p_(G backslash e) (r) - p_(G slash e) (r)
  $
  where $G backslash e$ denotes the graph $G$ with the edge $e$ removed, while $G slash e$ denotes the graph $G$ with the edge $e$ contracted, that is, in which the vertices of $e$ are joined into a single vertex and the multiple edges and loops are removed.
]<lmm-del-con-rel>

This lemma can be restated as $p_G (r) = p_(G union.sq e) (r) + p_(G slash e) (r)$ when $e in.not E$, and now we can easily prove it.
Indeed, let $e = {v,w}$ and notice that proper colorings $kappa$ of $G union.sq e$ are in bijection with proper colorings of $G$ with $kappa(v) eq.not kappa(w)$.
Notice also that proper colorings of $G slash e$ are in bijection with proper colorings of $G$ with $kappa(v) = kappa(w)$.
Using these two facts we can conclude the proof.

Using this lemma on a graph $G$, we can reduce the problem of calculating its chromatic polynomial to calculating the polynomial of the graph with one less edge or one less vertex (and some fewer edges).
Iterating this substitution we end up with a linear combination of chromatic polynomials of empty graphs, which we already checked to be polynomials, so we can conclude that also $p_G (r)$ is a polynomial.

As it can be seen from this proof, this recursive algorithm can be used to effectively calculate the chromatic polynomial.
See for example @fig-del-con-reduction.
In this case, since we are not dealing with colorings, we represent nodes only through their label.
In general, we adopt the representation as in @fig-graph-0-col when we are dealing with colorings, and as the one in @fig-del-con-reduction otherwise.

#figure(
  drawings.del-con-reduction,
  caption: [Deletion-contraction recursion]
)<fig-del-con-reduction>

As we have been able to see, the deletion-contraction relation is a linear and local relation in graphs, in the sense that it involves somewhat close graphs (in this case the difference is an edge or a vertex).
However, after verifying that the chromatic polynomial satisfies this identity, we were able to obtain a global result in the graphs, namely, that the chromatic polynomial is indeed a polynomial.
We will see that such an idea comes up again in the next chapters in the form of a #emph[modular law], that is, a form adapted to a generalization of the chromatic polynomial, i.e., chromatic symmetric functions, and it will be an indispensable tool for obtaining important results.

Let us now show some examples.
#example(name: [Chromatic polynomial of some graphs])[
  - As we have already seen, for the empty graph $E_n$ we have
    $
      p_(E_n) (r) = r^n med.
    $
  - For the complete graph $K_n$ with $n$ vertices, we have
    $
      p_(K_n) (r) = [r]_n = r (r-1) (r-2) dots (r-n+1) med.
    $
    To show this, start coloring the vertices and notice that for each vertex one has to choose one color among the possible $r$ colors, different from all the previous ones.
  - For a tree $T$ with $n$ vertices, we have
    $
      p_T (r) = r (r-1)^(n-1) med.
    $
    Again, start with a vertex and proceed by coloring the neighbors of an already colored vertex.
    Since a tree is connected and has no cycles, each vertex except the first has only one colored neighbor and thus we can choose its color in $r-1$ ways.
]<exm-cp>

Let us now see some properties regarding the chromatic polynomial.

#proposition()[
  If $G_1$, $G_2$ are graphs and $G = G_1 union.sq G_2$ is their disjoint union, then
  $
    p_G (r) = p_(G_1) (r) p_(G_2) (r) med.
  $
]<prop-cp-union>
#proof[
  This follows immediately from the fact that the colorings of $G$ are the couples made by a coloring of $G_1$ and a coloring of $G_2$.
]

#proposition()[
  Let $G$ be a graph with $n$ vertices, $n_e$ edges and $n_c$ connected components, then the following statements are true:
    + $p_G (r)$ is a monic polynomial of degree $n$ with integers coefficients.
    + The coefficient of $r^(n-1)$ is $- n_e$.
    + The signs of the coefficients alternate.
    + The maximal power of $r$ dividing $p_G (r)$ is $r^(n_c)$, that is, $nu_r (p_G) = n_c$ where $nu_r$ is the valuation over the element $r$.
]<prop-cp-props>
#proof[
  We proceed by strong induction on the number of vertices and the number of edges.
  For the empty graph $E_n$, it is verified that the thesis holds.
  Now consider a generic graph $G$ and by using the deletion-contraction relation we obtain
  $
    p_G (r)
      &= p_(G backslash e) (r) - p_(G slash e) (r) \
      &= (r^n - (n_e - 1) r^(n-1) + c_(n-2) r^(n-2) - c_(n-3) r^(n-3) + dots) - (r^(n-1) - d_(n-2) r^(n-2) + d_(n-3) r^(n-3) - dots) \
      &= r^n - n_e r^(n-1) + (c_(n-2) + d_(n-2)) r^(n-2) - (c_(n-3) + d_(n-3)) r^(n-3) + (dots)
  $
  where in the second equality we used the inductive hypothesis and so $c_j, d_j in NN$.
  This proves the first three points.

  For the fourth point, notice that $G backslash e$ has at least as many connected components as $G$, while $G slash e$ has the same number of connected components as $G$, so $nu_r (p_(G backslash E)) >= n_c$, while $nu_r (p_(G slash E)) = n_c$.
  Hence we have
  $
    p_G (r)
      &= p_(G backslash e) (r) - p_(G slash e) (r) \
      &= r^(n_c) (r^(n-n_c) - dots + (-1)^(n-n_c) c_(n_c)) - r^(n_c)(r^(n-1-n_c) - dots + (-1)^(n-1-n_c) d_(n_c)) \
      &= r^(n_c) (r (dots) + (-1)^(n-n_c)(c_(n_c) + d_(n_c)))
  $
  for $c_(n_c) in NN$ and $d_(n_c) in NN^+$ by inductive hypothesis.
  This shows that the coefficient of $r^(n_c)$ in $p_G (r)$ is non zero, so we conclude $nu_r (p_G) = n_c$.
]

Using this last theorem, we can prove a converse of the statement regarding trees in @exm-cp:
if $p_G (r) = r (r-1)^(n-1)$, then $G$ is a tree with $n$ vertices.
To prove this fact, it's enough to show that $G$ has $n-1$ edges and is connected.
But this follows from @prop-cp-props, the fact that $nu_r (p_G) = 1$ and that the coefficient of $r^(n-1)$ is $- (n-1)$.

We conclude this section by showing a couple of formulas for computing the chromatic polynomial, which can also be found in the article @whitney_logical_1932 by Whitney.
These formulas are generalized in the next chapters to the case of chromatic symmetric functions.

The first one follows from an application of the inclusion-exclusion principle and it is as follows.
#theorem()[
  For a graph $G$ we have
  $
    p_G (r) = sum_(S subset.eq E) (-1)^(\#lr(|S|)) r^(n_c(G_S))
  $
  where $n_c (G_S)$ is the number of connected components of the subgraph $G_S = (V,S)$ of $G$.
]<thm-cp-exp1>

For the second one, we need to introduce some notation.
If $pi = {B_1, dots, B_k}$ is a partition of $V$, then we say that $pi$ is connected if the restriction of $G$ to each block $B_i$ of $pi$ is connected.
Then we define $L_G$ as the set of all connected partitions of $G$, partially ordered by refinement.
Let $hat(0)$ be the minimal element of $L_G$, that is, the partition of $V$ into $n$ one-element blocks.
We can now state the following using the Möbius function of $L_G$.
#theorem()[
  For a graph $G$ we have
  $
    p_G (r) = sum_(pi in L_G) mu(hat(0), pi) n^(\#lr(|pi|)) med.
  $
]<thm-cp-exp2>
