#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Part listings and $(3+1)$-free posets

In this section, we introduce part listings, that is, an easily manipulated $(3+1)$-free representation of posets.
We then go on to see some relations between part listings that, if satisfied, guarantee that they induce the same poset.
The main reference for this section is the @guay-paquet_modular_2013 article by Guay-Paquet, from which we draw the main definitions and some theorems.

We begin by enunciating what a part listing is.
#definition(name: [Part listing])[
  A #emph[part listing] is an ordered list of #emph[parts], which are arranged on positive integer #emph[levels].
  Each part is either a single vertex at a given level or a bicolored graph where the color classes correspond to two adjacent levels and all edges join vertices on distinct levels.
]<def-part-lis>

A part listing can be given as a word over the alphabet
$
  Sigma = { a_i mid(|) i in NN^+ } union { b_(i,i+1) (G) mid(|) i in NN^+, G "is a bicolored graph" }
$
where the symbol $a_i$ corresponds to a vertex on level $i$, and the symbol $b_(i,i+1) (G)$ corresponds to a copy of the bicolored graph $G$ on levels $i$ and $i+1$.

Given a vertex $v$ of a part listing, let us denote by $p(v)$ the index of the part to which it belongs.

Given a part listing $L$, we can define an associated poset on its vertex set as follows.
If $v$ and $w$ are vertices in $L$, then let $v prec w$ if one of the following holds.
+ $v$ is at least two levels below $w$.
+ $v$ is exactly one level below $w$ and $p(v) < p(w)$, that is, the part containing $v$ appears strictly before the part cointaining $w$ in $L$.
+ $v$ is exactly one level below $w$ and they are joined by a bicolored graph edge.
Note that this construction does yield a poset; indeed, $v prec w$ implies that $v$ is on a level strictly below $w$, which guarantees antisymmetry, while the first condition guarantees transitivity.

Look at @fig-part-lis for an example.
#figure(caption: [left, part listing $L = a_2 a_1 a_3 a_3 a_1 b_(1,2) (G)$; right, it's associated poset])[
  #grid(columns: (1fr, 1fr), align: horizon+center,
    [#drawings.part-lis-0],
    [#drawings.part-lis-poset-0],
  )
]<fig-part-lis>

Let us see a first proposition that justifies the introduction of part listings and its proof.
#proposition()[
  Given a part listing $L$, the associated poset is $3+1$-free.
]<prop-part-lis-31-free>
#proof[
  Let $v_1 prec v_2 prec v_3$ be a chain of vertices in $P$, and suppose there exists a vertex $w$ incomparable with all three.
  Since vertices can be incomparable only if they lie on the same or adjacent levels, necessarily $v_2$ and $w$ lie on the same level, while $v_1$ lies one level below and $v_3$ one level above.
  Since $v_1$ and $v_3$ lie two levels apart, we must have $p(v_1) eq.not p(v_3)$.
  If $p(v_1) < p(v_3)$, then $p(v_1) < p(w)$ or $p(w) < p(v_3)$.
  Both cases lead to an absurd since in the former one would have $v_1 prec w$, while in the latter $w prec v_3$.
  Similarly, if $p(v_1) > p(v_3)$, then $p(v_1) > p(v_2)$ or $p(v_2) > p(v_3)$.
  Here again, we come to an absurdity, since in the first case $v_1$ and $v_2$ would be incomparable, while in the second case $v_2$ and $v_3$ would be.
]

As anticipated in the introduction, a converse of the previous proposition also applies.
#proposition()[
  Given a $(3+1)$-free poset $P$, there exists a part listing whose associated poset is $P$.
]<prop-31-free-part-lis>
#proof[
  We do not address the proof of this result in detail, but we refer to @guay-paquet_structure_2014[Theorem 2.38], also due to Guay-Paquet.
  This result shows how to decompose a $(3+1)$-free poset into #emph[clone sets] and #emph[tangles].
  In our case, a clone set with $k$ vertices at level $i$ corresponds to $k$ consecutive parts $a_i$ in the part listing, while a tangle $t_(i,i+1) (G)$ corresponds to a bicolored graph part $b_(i,i+1) (G)$.
]

The previous two propositions state that part listings succeed in characterizing $(3+1)$-free posets.
However, this representation is not unique; in fact, two distinct part listings $L$ and $L'$ can give rise to the same poset.
In such a case, we say that they are equivalent and write $L tilde_P L'$.
Let us now look at some conditions that guarantee us the equivalence of two part listings.
- Commutation relations.\
  If two consecutive parts of a part listing $L$ are at least two levels apart, then they can be swapped leaving the associated poset unchanged.
  In other words, if $A$, $B$ are words over the alphabet $Sigma$ and $i$, $j$ are levels with $j - i >= 2$, then we have the following relations:
  $
    A a_i a_j B &tilde_P A a_j a_i B med, &#h(4em) A a_i b_(j,j+1) B &tilde_P A b_(j,j+1) a_i B med, \
    A b_(i,i+1) b_(j+1,j+2) B &tilde_P A b_(j+1,j+2) b_(i,i+1) B med, & A b_(i,i+1) a_(j+1) B &tilde_P A a_(j+1) b_(i,i+1) B med. \
  $
  The verification of these relations is immediate.

  For example, in the part listing in @fig-part-lis, we can swap vertices $4$ and $5$, then vertices $3$ and $5$, obtaining the relation $a_2 a_1 a_3 a_3 a_1 b_(1,2) (G) tilde_P a_2 a_1 a_1 a_3 a_3 b_(1,2) (G)$.

- Circulation relations.\
  Given a word $A$ over the alphabet $Sigma$, let $A^+$ be the word obtained by raising each symbol by one level, that is, replacing each $a_i$ by $a_(i+1)$ and each $b_(i,i+1) (G)$ by $b_(i+1,i+2) (G)$.
  Then for any two words $A$, $B$ over $Sigma$ we have the relation
  $
    A^+ B tilde_P B A med.
  $
  It is sufficient to verify this relation when $A$ consists of only one part.
  Moreover, the order relations internal to $A$ are preserved, so it suffices to see that those between vertices of $A$ and vertices of $B$ are preserved.
  Let $v$ be a vertex in $A$ on level $i$ and $w$ a vertex of $B$ on level $j$, and denote by $v^+$ the vertex of $A^+$ corresponding to $v$, which lies on level $i+1$.
  Since $v$ is in the last part, we have $v prec w$ if and only if $j - i >= 2$ and $v succ w$ if and only if $j - i <= -1$.
  Similarly, since $v^+$ is in the first part, we have $v^+ prec w$ if and only if $j - (i+1) >= 1$ and $v^+ succ w$ if and only if $j - (i+1) <= -2$.
  From the equality of conditions, we conclude that $v prec w$ or $v succ w$ if and only if $v^+ prec w$ or $v^+ succ w$ respectively.

  For example, in the part listing in @fig-part-lis, the vertex $1$ is on level $2$, so it can be lowered to level $1$ and moved to the end, obtaining the relation $a_2 a_1 a_3 a_3 a_1 b_(1,2) (G) tilde_P a_1 a_3 a_3 a_1 b_(1,2) (G) a_1$.

- Combination relations.\
  Given some consecutive parts in a part listing, all on levels $i$ and $i+1$, we can replace them with a single part made from a bicolored graph whose edges are given by the order relations on the vertices given by the associated poset.
  Given a word $B_(i,i+1)$ over the alphabet
  $
    {a_i, a_(i+1)} union {b_(i,i+1) (G) mid(|) G "is a bicolored graph"}
  $
  we denote by $overline(B_(i,i+1))$ the equivalent bicolored graph part.
  Then for any two words $A$, $C$ over $Sigma$ we have the relation
  $
    A B_(i,i+1) C tilde_P A overline(B_(i,i+1)) C med.
  $

  For example, in the part listing in @fig-part-lis, we can combine the last two parts into a bicolored graph part $b_(1,2) (G')$ obtaining the relation $a_2 a_1 a_3 a_3 a_1 b_(1,2) (G) tilde_P a_2 a_1 a_3 a_3 b_(1,2) (G')$, where $G'$ is the graph obtained by adding to $G$ the vertex $5$ and the edges ${5,7}$ and ${5,9}$.

Finally, we treat the case of part listings in which there are no bicolored graph parts and thus each part consists of a single vertex.
We show that all and only posets both $(3+1)$-free and $(2+2)$-free can be represented in this way.
In particular, we associate a unit interval order with each such part listing and vice versa.

#proposition()[
  Given a part listing $L$ whose parts consist of single vertices, the associated poset is $(3+1)$-free and $(2+2)$-free.
]<prop-part-lis-vert-31-22-free>
#proof[
  We read the vertices of $L$ line by line from bottom to top, moving from left to right, obtaining a word $v_1 v_2 dots v_n$.
  We show that vertices in this order satisfy the property of unit interval orders, that is, if $i < j < l$ and $v_i prec.not v_l$ then $v_i prec.not v_j$ and $v_j prec.not v_l$.
  The hypothesis that $v_i prec.not v_l$ implies that $v_i$ and $v_l$ are on the same level or that $v_i$ is one level below $v_l$ and $p(v_i) > p(v_l)$.
  In the first case, $v_j$ is also on the same level as $v_i$ and $v_l$, so it is incomparable with both.
  In the second case $v_j$ can either be on the same level as $v_i$ and with $p(v_j) > p(v_i) > p(v_l)$, so incomparable with both, or on the same level as $v_l$ and with $p(v_j) < p(v_l) < p(v_i)$, so again incomparable with both.
]

#proposition()[
  Given a poset $P$ both $(3+1)$-free and $(2+2)$-free, there exists a part listing whose parts consist of single vertices and with $P$ as associated poset.
]<prop-31-22-free-part-lis-vert>
#proof[
  We represent $P$ as a unit interval order, whose associated Dyck path is described by a function $h$.
  At this point we iteratively construct a part listing, orderly placing vertices line by line first from bottom to top and then from left to right.
  Consider the sequence $r_l$ with $r_1 = 1$ and $r_(l+1) = h(r_l)+1$ as long as $r_l <= n$.
  In each level, we orderly place vertices $[r_l, h(r_l)]$.

  Clearly, we can fill the first level only in one way.
  Assume now that we have completed the $l$-th level and need to fill the $l+1$-th level with vertices $[r_(l+1), h(r_(l+1))]$.
  For each $i in [r_(l+1), h(r_(l+1))]$ we consider the maximum $j in [r_l, h(r_l)]$ such that $h(j) < i$, i.e., such that $j prec i$, well defined since $h(r_l) = r_(l+1) - 1 < i$.
  We then insert the vertex $i$ to the right of $j$ and, if $j < h(r_l)$, also to the left of $j+1$.
  If $i > r_(l+1)$, we also require $i$ to be to the right of $i-1$, a condition compatible with the previous ones since vertex $i-1$ has been placed in an interval equal to or preceding the one of $i$.

  To verify that the construction made has as its induced poset precisely $P$, we need only see that for every vertex $i$ and for every $j < i$ we have $h(j) < i$ if and only if $j$ lies at least two levels below $i$ or $j$ lies one level below $i$ and $p(j) < p(i)$.
  Given $l_i$ and $l_j$ the levels of $i$ and $j$, we distinguish several cases:
  - $l_i >= l_j + 2$, then $h(j) <= h(r_(l_j + 1)) < r_(l_j + 2) <= i$;
  - $l_i = l_j + 1$ and $p(j) < p(i)$, then $h(j) < i$ by definition;
  - $l_i = l_j + 1$ and $p(j) > p(i)$, then $h(j) >= i$ by definition;
  - $l_i = l_j$, then $h(j) >= h(r_(l_j)) >= i$.
  This concludes our proof.
]
