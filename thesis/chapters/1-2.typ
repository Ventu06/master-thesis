#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Chromatic symmetric functions

The chromatic polynomial merely counts the number of colorings of a graph but does not provide any information about those colorings.
It also represents an invariant that is not too sophisticated: as shown in @prop-cp-props, it gives us information about the number of vertices, edges, and connected components, but not much more detail, e.g., it does not allow us to distinguish between different trees with a fixed number of vertices as seen in @exm-cp.
To overcome these problems, Stanley in @stanley_symmetric_1995 introduced a generalization of the chromatic polynomial, the so-called chromatic symmetric function.

#definition(name: [Chromatic symmetric function])[
  The #emph[chromatic symmetric function] of a graph $G$ is
  $
    chi_G (X) = sum_(kappa in op("PC")(G)) x_kappa = sum_(kappa in op("PC")(G)) product_(v in V) x_(kappa(v))
  $
  that is the sum over all proper colorings of the monomials that count which colors were used.
]<def-csf>

First of all, notice that $chi_G (X)$ is indeed a symmetric function.
In fact, to show that it is invariant by permutation of the variables, it is sufficient to note that the set of proper coloring is invariant by permutation of the colors.

Secondly, we can observe how $chi_G (X)$ is a generalization of the chromatic polynomial $p_G (r)$.
Indeed, let us set $x_j |-> 1$ for $1 <= j <= r$ and $x_j |-> 0$ for $j > r$.
Then $x_kappa = 1$ if $kappa$ is an $r$-coloring, and $x_kappa = 0$ otherwise, so the function $chi_G$ under this specialization is counting proper $r$-colorings, that is
$
  chi_G \( underbrace(1\, 1\, dots\, 1, r), 0, 0, dots \) = p_G (r) med.
$
More formally, let us consider the morphism $psi : "Sym"[X] -> ZZ[r]$ defined on the powersum basis by
$
  psi(p_j) = underbrace(1^j + 1^j + dots + 1^j, r) + 0^j + 0^j + dots = r med.
$
Then the previous equality can be stated as $psi(chi_G (X)) = p_G (r)$.

Let us now show some examples.
#example(name: [Chromatic symmetric function of some graphs])[
  - For the empty graph $E_n$ we have
    $
      chi_(E_n) (X) = e_(1^n)
    $
    by noticing that we can choose the color of each vertex independently.
  - For the complete graph $K_n$ we have
    $
      chi_(K_n) (X) = n! dot e_n = n! dot m_(1^n) med.
    $
    In fact, we have to choose a different color for each vertex and there are $n!$ ways of assigning $n$ different colors to $n$ vertices.
  - #grid(columns: (2fr, 1fr), align: horizon,
    [
      For the graph in the picture, we have
      $
        chi_G (X) = 4! dot m_(1^4) + 4 m_(2,1^2) med.
      $
      We can choose $4$ different colors and assign them in $4!$ different ways, or we can choose $3$ different colors and use one of them twice.
      In the latter case, we assign the repeated color to vertex $2$ and to one between vertices $3$ and $4$, then we assign the other two colors in the two possible ways, for a total of $4$ ways.
    ],[
      #align(center)[#drawings.graph-0]
    ])
]<exm-csf>

Let us now proceed to show some properties and expansions of the chromatic symmetric function, similar to what was done for the chromatic polynomial.

Let us first note that, since $chi_G (X)$ is a generalization of $p_G (r)$, it determines the number of vertices, edges, and connected components of $G$ as seen in the @prop-cp-props.

We want the expansion in the monomial basis.
We say that a partition $pi = {B_1, dots, B_k}$ of $V$ is stable if each block is totally disconnected, that is, if there are no edges connecting vertices within the same block.
We can then associate with $pi$ a partition $lambda(pi)$ whose parts are given by the cardinalities of the blocks, ordered decreasingly.
By coloring each block with the same color, we can obtain the following formula.
#proposition()[
  For a graph $G$ we have
  $
    chi_G (X) = sum_(pi "stable") m_(lambda(pi)) med.
  $
]<prop-csf-exp-m-1>

As in the case of the chromatic polynomial, we have the following result.
#proposition()[
  If $G_1$, $G_2$ are graphs and $G = G_1 union.sq G_2$ is their disjoint union, then
  $
    chi_G (X) = chi_(G_1) (X) chi_(G_2) (X) med.
  $
]<prop-csf-union>
#proof[
  Notice that the colorings of $G$ are the couples made by a coloring of $G_1$ and a coloring of $G_2$.
  Since for $kappa = (kappa_1, kappa_2) in op("PC")(G)$ with $kappa_1 in op("PC")(G_1)$ and $kappa_2 in op("PC")(G_2)$ we have $x_kappa = x_(kappa_1) x_(kappa_2)$, the thesis immediately follows.
]

Unlike chromatic polynomials, the deletion-contraction relation no longer holds.
This is clear by noticing that $chi_G (X)$ and $chi_(G backslash e) (X)$ are homogeneous of degree $n$, while $chi_(G slash e) (X)$ is homogeneous of degree $n-1$, so they can't be in a linear relation.
However, as already anticipated, there is another linear relation that applies in the case of chromatic symmetric functions, the modular law.
We introduce this in the next chapter.

We now show a couple of formulas to effectively calculate the chromatic symmetric function, which refines @thm-cp-exp1 and @thm-cp-exp2 from the previous section. These can be found in the article @stanley_symmetric_1995 by Stanley.

As in the previous case, the first one can be obtained using an inclusion-exclusion principle.
#theorem()[
  For a graph $G$ we have
  $
    chi_G (X) = sum_(S subset.eq E) (-1)^(\#lr(|S|)) p_(lambda(S))
  $
  where $lambda(S)$ is the partition of $n$ whose parts are equal to the sizes of the connected components of the subgraph $G_S = (V,S)$ of $G$.
]<thm-csf-exp-p-1>

The second identity is also very similar to the one regarding chromatic polynomials, and can by obtained using a Möbius inversion.
#theorem()[
  For a graph $G$ we have
  $
    chi_G (C) = sum_(pi in L_G) mu(hat(0), pi) p_(lambda(pi)) med.
  $
]<thm-csf-exp-p-2>

Moreover, it can be proved that $(-1)^(n - \#lr(|pi|)) mu(hat(0), pi) > 0$ for every $pi in L_G$, so the following holds
$
  omega(chi_G (X))
    &= sum_(pi in L_G) mu(hat(0), pi) (-1)^(n - \#lr(|pi|)) p_(lambda(pi)) \
    &= sum_(pi in L_G) lr(|mu(hat(0), pi)|) p_(lambda(pi)) \
$
This formula tells us that $omega(chi_G (X)) in NN[{p_lambda}_(lambda tack.r n)]$, that is, the coefficients of the expansion in the powersum basis are non-negative integers.

In general, when the coefficients of the expansion of a certain function in a basis $u$ are non-negative integers, we say that that function is $u$-positive.
So the previous fact can be stated as follows.
#corollary()[
  $omega(chi_G (X))$ is $p$-positive.
]<omega-csf-p-pos>
Note that in @prop-csf-exp-m-1 we verified that $chi_G (X)$ is also $m$-positive.

During this thesis, we will be very interested in questions of positivity, mainly concerning the $s$-positivity and $e$-positivity of the chromatic symmetric functions and other related functions.
We see more details in the next sections.

Let us look at one last example showing us an application of @thm-csf-exp-p-1.
#example()[
  Let $G$ be the line graph with $n$ vertices, that is,
  $
    G = ([n], {{1,2}, {2,3}, dots, {n,n-1}}) med.
  $
  Using the notation introduced in @def-comp, we have
  $
    chi_G (X)
      &= sum_(S subset.eq E) (-1)^(\#lr(|S|)) p_(lambda(S)) \
      &= sum_(alpha tack.r.double n) (-1)^(n-l(alpha)) p_(lambda(alpha)) \
      &= sum_(lambda tack.r n) (-1)^(n-l(lambda)) \#lr(|{alpha tack.r.double n mid(|) alpha tilde lambda}|) p_lambda
  $
  where, if $lambda = (1^(d_1) 2^(d_2) dots n^(d_n))$, we have
  $
    \#lr(|{alpha tack.r.double n mid(|) alpha tilde lambda}|)
      = binom(l(lambda), d_1, d_2, dots, d_n)
      = (l(lambda)!)/(d_1! d_2! dots d_n!) med.
  $
]

The example above provides a nice formula for the chromatic symmetric function of a line graph, however we do not have another clean formula for generic trees as in the case of the chromatic polynomial.
As mentioned initially, one of Stanley's reasons for introducing this function is to distinguish trees.
In the article @stanley_symmetric_1995 he conjectured that non-isomorphic trees have distinct chromatic symmetric functions, and this conjecture has not yet been resolved.
However, it has been shown to be true for some particular classes and has been computationally verified for trees up to 23 vertices.
We refer to @martin_distinguishing_2008 for further details.

In the rest of the paper, we do not deal with trees, but we aim to study another conjecture introduced in the same article concerning the $e$-positivity of a different class of graphs.
In the next sections, we go on to introduce that class and then state the conjecture of our interest.
