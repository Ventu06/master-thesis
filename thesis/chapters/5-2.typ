#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Increasing spanning forests

In this section, we use the modular law to provide a formula for chromatic quasisymmetric functions of unit interval graphs.
Similar to the previous chapter, we look at multiple expressions of this formula through different structures on which to consider different statistics: subsets of permutations, a new type of forests and orientations of the edges.
We also enunciate a similar formula for LLT polynomials, partly already known but obtained by means that have opened new perspectives.

We begin by considering an interval graph $G$ on $n$ vertices, and let $h$ be the function associated with the relative path.

Let us now introduce the first structure of this section.
#definition(name: [$frak(S_(<=h))$])[
Given a permutation $sigma in frak(S)_n$, we say that $sigma <= h$ if $sigma(i) <= h(i)$ for every $1 <= i <= n$.
We denote by $frak(S)_(<=h)$ the set of such permutations, that is
$
  frak(S)_(<=h) = { sigma in frak(S)_n mid(|) sigma(i) <= h(i) quad forall i in [n] } med.
$
]<def-Sleh>

We now want to define a transformation $dot^c : frak(S)_n -> frak(S)_n$ that sends a permutation $sigma$ into $sigma^c$ in the following way: we write $sigma$ as a succession of cycles, in which each cycle begins with its smallest element and the cycles are ordered ascending according to the first element.
For example:
$
  sigma = 617892543 = (162)(3759)(48) |-> 162375948 = sigma^c med.
$

At this point, for a certain permutation $sigma$, we define its partition $lambda(sigma)$ as the partition given by the size of the cycles.
For example, for the permutation $sigma = 617892543$ shown above, we have $lambda(sigma) = (4,3,2)$
We also define the weight of a permutation as
$
  op("wt")_G (sigma) = tilde(op("inv"))_G (sigma^c)
  &= \#lr(|{ {sigma^c (i), sigma^c (j)} in E mid(|) i < j, sigma^c (i) > sigma^c (j) }|) \
  &= \#lr(|{ 1 <= i < j <= n mid(|) j <= h(i), (sigma^c)^(-1) (i) > (sigma^c)^(-1) (j) }|) med.
$

Let us now choose elements $y_lambda$ for $lambda tack.r n$ in a $QQ(q)$-algebra $cal(A)$.
For each unit interval graph $G$, that is, as $h$ varies in $cal(D)$, we define the following function
$
  f(h) = sum_(sigma <= h) q^(op("wt")_G (sigma)) y_(lambda(sigma)) med.
$

In @abreu_symmetric_2021[Proposition 3.4] it is shown that this function obeys the modular law, verifying equality for each term in a manner similar to what we tried to do in the previous section with the chromatic quasisymmetric function.
So, due to @thm-mod-law-det-cmp, $f$ is determined by the values it takes on $K_lambda$.

Moreover, given two unit interval graphs $G_1$ and $G_2$ (with the labels of the vertices of $G_2$ successive to those of the vertices of $G_1$) with associated functions $h_1$ and $h_2$, and two permutations of the respective vertices $sigma_1$ and $sigma_2$, it is easy to verify that
$
  op("wt")_(G_1 union.sq G_2) (sigma_1 union.sq sigma_2) = op("wt")_(G_1) (sigma_1) + op("wt")_(G_2) (sigma_2) med.
$
Choosing as $y_lambda$ a multiplicative funciton, that is, where $y_lambda = product_(i=1)^(l(lambda)) y_(lambda_i)$, it can be deduced that
$
  f(h_1 union.sq h_2) = f(h_1)f(h_2) med.
$
So $f(K_lambda) = product_(i=1)^(l(lambda)) f(K_(lambda_i))$, that is, $f$ is determined only by the value it takes on complete graphs.

At this point, provided we can properly specialize the various $y_j$ as $j$ varies in $NN$, it is possible to express a multiplicative function satisfying the modular law in the form given by $f(h)$.
For example, in order to obtain a formula for the chromatic quasisymmetric function, it is sufficient to solve $f(K_n) = chi_(K_n)^q (X;q)$ in the variables $y_j$, i.e.
$
  cases(
    [n]_q ! dot e_n = sum_(sigma in frak(S)_n) q^(op("inv")(sigma^c)) product_(i=1)^(l(lambda(sigma))) y_(lambda(sigma)_i) wide forall n in NN med.
    )
$

In @abreu_symmetric_2021, Abreu and Nigro provide a solution to the previous system by making use of a modification of the Hall-Littlewood polynomials: given $P_n (X;q)$ the Hall-Littlewood polynomial of index $n$, just put $y_n = omega(rho_n (X;q))$ where $ rho_n (X;q) = q^(n-1) P_n (X;q^(-1))$.
Note that Hall-Littlewood polynomials are not multiplicative, so
$
  rho_lambda (X;q)
    = q^(\#lr(|lambda|) - l(lambda)) product_(i=1)^(l(lambda)) P_(lambda_i) (X;q^(-1))
    eq.not q^(\#lr(|lambda|) - l(lambda)) P_lambda (X;q^(-1)) med.
$

The various $rho_n (X;q)$ can also be defined recursively by the following formula
$
  [n]_q h_n(X) = sum_(i=1)^n h_(n-i) (X) rho_i (X;q)
$
which shows that $rho_n (X;q)$ has close relations both with the powersum basis, since $rho_n (X;1) = p_n (X)$, and with the elementary basis, since $rho_n (X;0) = (-1)^(n-1) e_n$.

A final way to view Hall-Littlewood polynomials is through the following plethystic relation
$
  rho_n
    = (h_n [X (q-1)]) / (q-1)
    = (-1)^(n-1) (e_n [X (1-q)])/(1-q) \
  arrow.b.double \
  rho_lambda
    = (h_lambda [X (q-1)]) / (q-1)^(l(lambda))
    = (-1)^(n - l(lambda)) (e_lambda [X (1-q)]) / (1-q)^(l(lambda)) med.
$

Let us therefore state the main theorem of this section.
#theorem(name: [@abreu_symmetric_2021[Theorem 1.2]])[
  Let $G$ be a unit interval graph and $h$ the function defining the relative Dyck path, then
  $
    omega(chi_G^q (X;q)) = sum_(sigma <= h) q^(op("wt")_G (sigma)) rho_(lambda(sigma)) (X;q) med.
  $
]<thm-cqsf-exp-rho-slth-1>

Expanding the Hall-Littlewood polynomials by their plethystic definition, we obtain the following equivalent formula
$
  chi_G^q (X;q)
    &= sum_(sigma <= h) q^(op("wt")_G (sigma)) (-1)^(n - l(lambda(sigma))) / (1-q)^(l(lambda(sigma))) product_(i=1)^(l(lambda(sigma))) sum_(j=0)^(lambda_i) (-q)^j e_j h_(lambda_i-j) med.
$

Let us now see a new structure through which to express the formula just stated.
#definition(name: [Increasing spanning forest])[
  Let $G$ be an interval graph.
  An #emph[increasing spanning forest] $F$ is a collection of subgraphs $(T_1, dots T_k)$, with $T_h = (V_h, E_h)$, such that:
  - the vertices of $T_h$, as $h$ varies, form a partition of $V$, i.e., $V = union.big.sq_(1 <= h <= k) V_h$;
  - $T_h$ is a rooted tree for every $h$;
  - the trees $(T_h)_(1 <= h <= k)$ are sorted in ascending order by root;
  - for every $h$, crossing $T_h$ from the root to the leaves, the vertices encountered have increasing labels.
]<def-isf>

We denote by $op("ISF")(G)$ the set of increasing spanning forests of $G$.
Note that an increasing spanning forest is also a magic spanning forest since the condition of label growth going from the root to the leaves implies the magic property.
As in magic spanning forests, here we also define the type $F$ as the partition $lambda(F)$ given by the sizes of its component trees.

Observe that there is a bijection between $frak(S)_(<=h)$ and $op("ISF")(G)$.
In fact, given a permutation $sigma in frak(S)_(<=h)$, we consider its decomposition in cycles
$sigma = (sigma_1^1, dots, sigma_(i_1)^1) dots (sigma_1^k, dots, sigma_(i_k)^k)$
with $sigma_1^h < sigma_j^h$ for each $h in [k]$ and each $j in [2,i_h]$, and $sigma_1^h$ ordered ascendingly in $h$.
For each cycle $(sigma^h)$ we construct a tree $T_h$ with vertices appearing in the cycle, where $sigma_1^h$ is the root and the parent of $sigma_j^h$ is $sigma_l^h$ with $l = max { 1 <= l < j mid(|) sigma_l^h < sigma_j^h }$.
On the other hand, given a forest $F = (T_1, dots, T_k)$, we construct a permutation $sigma$ with $k$ cycles, each containing the vertices of a tree.
For each tree, we can explore it in depth by starting from the root and choosing the unexplored child vertex with the largest label each time, and then backtracking to the parent when all children have been explored.
The cycle associated with a tree is given by the order in which the vertices are visited.
It can be verified that these two maps are well-defined and that they are the inverse of each other.
Also note that the type of the structure, that is, the partition associated with the permutation or the forest, is preserved.

Let us see an example of a permutation of $frak(S)_(<=h)$ and the associated increasing spanning forest for the interval graph given by $h = (7,3,3,7,6,7,7)$.
#example()[
  #grid(columns: (2fr, 1fr), align: horizon+center, [
    $sigma = 6327451 = (16547)(23)$ \ \
    $arrow.b.double$ \ \
    #drawings.isf-1
  ],[
    #drawings.interval-graph((7,3,3,7,6,7,7))
  ])
]<exm-isf>

Given an increasing spanning forests $F = (T_1, dots, T_k)$ on an interval graph $G$, with $T_h = (V_h, E_h)$, we define its weight as
$
  op("wt")_G (F)
    &= op("inv")_G (F) + sum_(h=1)^k op("wt")_(G|_(T_h)) (T_h) \
    &= sum_(1 <= r < s <= k) \#lr(|{ (i,j) in V_r times V_s mid(|) {i,j} in E, i > j }|) \
    &quad + sum_(h=1)^k \#lr(|{ (i,j,l) in V_h^3 mid(|) i < j < l, {i,l} in E_h, {j,l} in E }|) med.
$

From what has been said so far, one would think that the bijection first defined between $frak(S)_(<=h)$ and $op("ISF")(G)$ preserves the weight $op("wt")_G$.
This is actually false, e.g. for the permutation $sigma$ in @exm-isf we have $op("wt")_G (sigma) = 3$, while for the corresponding forest $F$ we have $op("wt")_G (F) = 4$.

Actually, one can modify the previous bijection so that, in addition to preserving the type, it also preserves the weight.
The construction, described in @abreu_symmetric_2021[Proposition 2.6], is similar to the one illustrated but with some additional technical details and therefore is not reported.

Using this last bijection we can rewrite @thm-cqsf-exp-rho-slth-1 as follows.
#theorem()[
  Let $G$ be a unit interval graph and $h$ the function defining the relative Dyck path, then
  $
    omega(chi_G^q (X;q)) = sum_(F in op("ISF")(G)) q^(op("wt")_G (F)) rho_(lambda(F)) (X;q) med.
  $
]<thm-cqsf-exp-rho-isf-1>

The results we have obtained so far have an analog for LLT polynomials.
In fact, let us recall that the latter satisfies the modular law, so again it is possible to fall back to the case of complete graphs as done for the chromatic quasisymmetric function.
The resulting formulas are as follows.
#theorem(name: [@abreu_symmetric_2021[Theorem 1.3]])[
  Let $G$ be a unit interval graph and $h$ the function defining the relative Dyck path, then
  $
    "LLT"_G (X;q)
    &= sum_(sigma <= h) (q-1)^(n - l(lambda(sigma))) q^(op("wt")_G (sigma)) e_(lambda(sigma)) \
    &= sum_(F in op("ISF")(G)) (q-1)^(n - l(lambda(F))) q^(op("wt")_G (F)) e_(lambda(F)) med.
  $
]<thm-llt-exp-e-slth-isf-1>

This result can also be deduced from the analog by chromatic quasisymmetric function, and vice versa, by means of the Carlsson-Mellit identity.
We point out, however, that for neither result was it necessary to use this identity.

A direct consequence is the positivity in the elementary basis of the unicellular shifted LLT polynomials, a fact recently conjectured and also proved by other methods.
For more details, we refer to the section on LLT polynomials.

Finally, let us see a connection between the expansion of LLT polynomials by increasing spanning forests and the formula in @alexandersson_combinatorial_2022[Corollary 2.10], where orientations are used.

Let $G$ be a unit interval graph and let $cal(O)(G)$ be the set of orientations of the edges of $G$, not necessarily acyclic.
Given $theta in cal(O)(G)$, for each vertex $i$ of $G$ we consider the smallest vertex reachable by traversing the edges in the direction given by $theta$ so as to traverse a sequence of vertices with strictly decreasing labels (i.e., moving only to the "left").
We call this vertex $op("lrv")(i)$ (lowest reaching vertex from $i$).
We define the type of $theta$ as the partition $lambda(theta)$ given by $(op("lrv")^(-1) (i))_(1 <= i <= n)$.

Then we have the following identity.
#theorem(name: [@alexandersson_combinatorial_2022[Corollary 2.10]])[
  Let $G$ be a unit interval graph, then
  $
    "LLT"_G (X;q) = sum_(theta in cal(O)(G)) (q-1)^(op("inv")_G (theta)) e_(lambda(theta)) med.
  $
]<thm-llt-exp-e-o-1>

To show that the preceding is equivalent to the expression in @thm-llt-exp-e-slth-isf-1, we construct a map $Phi : cal(O)(G) -> op("ISF")(G)$ in the following way.
Given an orientation $theta$, for each vertex $i$ we choose as its parent the smallest vertex $j$ smaller than $i$ such that ${i,j} in E$, $op("lrv")(i) = op("lrv")(j)$ and the edge between $i$ and $j$ is oriented from $i$ toward $j$.
If such a vertex $j$ does not exist, then $i$ will be a root.
It is immediate to see that $Phi(theta)$ is an increasing spanning forest and that $lambda(theta) = lambda(Phi(theta))$.

At this point, it is sufficient to show that for each $F in op("ISF")(G)$ we have.
$
  sum_(theta in Phi(-1)(F)) (q-1)^(op("inv")_G (theta)) = (q-1)^(n - lambda(F)) q^(op("wt")_G (F)) med.
$
The latter identity is proved in @abreu_symmetric_2021[Proposition 4.1], concluding the proof.

Note that, by putting together the link just stated and the Carlsson-Mellit identity, it is possible to obtain a proof of @thm-cqsf-exp-rho-isf-1 only from @thm-llt-exp-e-o-1, thus not using the modular law as we have defined it.
However, a similar local relation is used in the original proof of @thm-llt-exp-e-o-1, so the proof of @thm-cqsf-exp-rho-isf-1 just given cannot be considered an alternative to one that uses the modular law.
