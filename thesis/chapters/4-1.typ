#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== The subset of permutations $cal(N)_(G, alpha)$

The structure we are going to study in this section, called $cal(N)_(G,alpha)$, is a particular set of permutations of $frak(S)_n$ and in a sense the simplest structure to represent, but certainly not the one with the most intuitive description.

This structure was introduced in @shareshian_chromatic_2012[Conjecture 4.15] and later in @shareshian_chromatic_2016[Definition 8.3] in a context with $G$ unit interval graph and $alpha$ partition.
Subsequently, in @dadderio_chromatic_2023[Conjecture 11.1], the case where $G$ is an interval graph and $alpha$ a composition was considered.
We enunciate the latter definition.
#definition(name: [$cal(N)_(G, alpha)$])[
  Let $G$ be an interval graph on $n$ vertices and let $alpha$ be a composition of $n$.
  Given a sequence of vertices $sigma = (sigma(1), dots, sigma(k))$, we say that:
  - given an index $i in [k-1]$, $sigma$ has a #emph[$G$-descent] at index $i$ if $sigma(i) > sigma(i+1)$ and ${sigma(i), sigma(i+1)} in.not E$;
  - given an index $i in [k]$, $sigma$ has a #emph[left-to-right $G$-maximum] at index $i$ if for every $j in [i-1]$ we have $sigma(j) < sigma(i)$ and ${sigma(j), sigma(i)} in.not E$.
  For example, $1$ is always a left-to-right $G$-maximum, which we call trivial.

  We now define $cal(N)_(G, alpha)$ as the subset of $frak(S)_n$ for which $sigma in cal(N)_(G, alpha)$ if and only if, by splitting sigma according to the composition $alpha$
  $
    sigma = \( underbrace(sigma_1, alpha_1) mid(|) dots mid(|) underbrace(sigma_(l(alpha)), alpha_(l(alpha))) \)
  $
  each of the various $sigma_i$ does not have $G$-descents, nor does it have non-trivial left-to-right $G$-maxima.
]<def-nga>

Given a permutation $sigma in frak(S)_n$, two indices $1 <= i < j <= n$ form a reverse $G$-inversion if $sigma(i) > sigma(j)$ and ${sigma(i),sigma(j)} in E$.
We denote by $tilde(op("inv"))_G (sigma)$ the number of reverse $G$-inversions, i.e.
$
  tilde(op("inv"))_G (sigma) = \#lr(|{ {sigma(i), sigma(j)} in E mid(|) i < j, sigma(i) > sigma(j) }|) med.
$

Here is an example of the elements of $cal(N)_(G, (4))$ and the number of their $G$-inversions for the interval graph with $h = [4,2,4,4]$.
#example(name: [$cal(N)_(G, (4))$ for $h = [4,2,4,4]$])[
  #grid(columns: (2fr, 1fr), align: horizon+center,
    [#table(
        columns: 4,
        stroke: none,
        align: center,
        column-gutter: 1em,
        table.hline(),
        table.header([$sigma$], [$tilde(op("inv"))_G (sigma)$], table.vline(stroke: .5pt), [$sigma$], [$tilde(op("inv"))_G (sigma)$]),
        table.hline(stroke: .5pt),
        [$(1, 2, 3, 4)$], [$0$],
        [$(1, 2, 4, 3)$], [$1$],
        [$(2, 1, 3, 4)$], [$1$],
        [$(2, 1, 4, 3)$], [$2$],
        [$(3, 1, 2, 4)$], [$1$],
        [$(3, 4, 1, 2)$], [$2$],
        [$(4, 1, 2, 3)$], [$2$],
        [$(4, 3, 1, 2)$], [$3$],
        table.hline(),
    )], [
      #drawings.interval-graph((4,2,4,4))
    ]
  )
]<exm-nga>

We enunciate the first major result of this section.
#theorem()[
  Let $G$ be a unit interval graph with $n$ vertices, then
  $
    omega(chi_G^q (X;q)) = sum_(lambda tack.r n) p_lambda / z_lambda sum_(sigma in cal(N)_(G, lambda)) q^(tilde(op("inv"))_G (sigma)) med.
  $
]<thm-cqsf-exp-p-1>
The previous result was first conjectured by Shareshian and Wachs in @shareshian_chromatic_2012[Conjecture 4.15] and in @shareshian_chromatic_2016[Conjecture 7.6].
They also proved the special case where $lambda = (n)$ in @shareshian_chromatic_2016[Lemma 7.4].
Here, the idea of the proof was to obtain an expansion in the powersum basis from the expansion in the Schur basis seen in @thm-cqsf-exp-s-1 using the Murnaghan-Nakayama rule.
At this point, a sign-reversing involution preserving $tilde(op("inv"))_G$ is constructed and the identity sought is obtained.
The general case was later proved by Athanasiadis in @athanasiadis_power_2015[Theorem 3.1], relating to the previous case in a similar way to what we shall later see at the end of this section.

There is also another structure, $tilde(cal(N))_(G, alpha)$, very similar to $cal(N)_(G,alpha)$, from which we obtain another expansion in the powersum basis.
This structure also first appeared in @shareshian_chromatic_2012[Conjecture 4.16] and in @shareshian_chromatic_2016[Lemma 7.7].
Here is the definition.
#definition(name: [$tilde(cal(N))_(G, alpha)$])[
  Let $G$ be a unit interval graph on $n$ vertices and let $alpha$ be a composition of $n$.
  Given a sequence of vertices $sigma = (sigma(1), dots, sigma(k))$ and an index $i in [n-1]$, we say that $sigma$ has a #emph[$G$-ascent] at index $i$ if $sigma(i) < sigma(i+1)$ and ${sigma(i), sigma(i+1)} in.not E$.
  We now define $tilde(cal(N))_(G, alpha)$ as the subset of $frak(S)_n$ for which $sigma in tilde(cal(N))_(G, alpha)$ if and only if, by splitting sigma according to the composition $alpha$
  $
    sigma = \( underbrace(sigma_1, alpha_1) mid(|) dots mid(|) underbrace(sigma_(l(alpha)), alpha_(l(alpha))) \)
  $
  each of the various $sigma_i$ does not have $G$-ascents and starts with the smallest vertex of the segment, that is, $sigma_i (1) < sigma_i (j)$ for each $1 < j <= alpha_i$.
]

In @shareshian_chromatic_2016[Proposition 7.8], the equivalence between the expansion of @thm-cqsf-exp-p-1 and the following was proved.
#theorem()[
  Let $G$ be a unit interval graph with $n$ vertices, then
  $
    omega(chi_G^q (X;q)) = sum_(lambda tack.r n) p_lambda / z_lambda product_(i=1)^(l(lambda)) [lambda_i]_q sum_(sigma in tilde(cal(N))_(G, lambda)) q^(tilde(op("inv"))_G (sigma)) med.
  $
]<thm-cqsf-exp-p-2>

It can be shown that both formulae just stated reduce to @thm-csf-exp-p-2 for $q |-> 1$.

Let us now turn to the case of interval graphs.
Since the chromatic quasisymmetric function is no longer symmetric, we cannot expect an expansion in the basis $p_lambda$.
For this reason, we try to use the refinement given by $Psi_alpha$ (see @def-qsym-fun-psi).
In @dadderio_chromatic_2023[Conjecture 11.1], D'Adderio et al. conjecture the following formula.
#theorem()[
  Let $G$ be an interval graph with $n$ vertices, then
  $
    omega(chi_G^q (X;q)) = sum_(alpha tack.r.double n) Psi_alpha / z_alpha sum_(sigma in cal(N)_(G, alpha)) q^(tilde(op("inv"))_G (sigma)) med.
  $
]<thm-cqsf-exp-psi-nga-1>
This formula was later proved quite directly by D'Adderio and Interdonato (personal communication), although this proof is not currently found in the literature.
In the next section we see a second proof using a theorem on acyclic orientations (@thm-nga-ao-bij).

We close this section by showing some ways to directly calculate the coefficients of the expansion in the powersum basis.

We begin by showing how to relate the case of a generic $alpha$ to the case $alpha = (n)$.
The idea shown below is reused several times in the rest of the thesis.

Let us fix a composition $alpha tack.r.double n$.
Let us denote by $Pi_alpha$ the set of partitions $pi = \( underbrace(pi_1, alpha_1), dots, underbrace(pi_(l(alpha)), alpha_(l(alpha))) \)$ of $V = [n]$, and given a partition $pi$, let $G(pi_i)$ be the subgraph of $G$ obtained from the restriction to the set of vertices of $pi_i$.
Let us then pose
$
  tilde(op("inv"))_G (pi) = \#lr(|{ {sigma(i), sigma(j)} in E mid(|) sigma(i) in pi_r, sigma(j) in pi_s, r < s, sigma(i) > sigma(j) }|) med.
$
As before, for $sigma in frak(S)_n$ we define.
$
  sigma = \( underbrace(sigma_1, alpha_1) mid(|) dots mid(|) underbrace(sigma_(l(alpha)), alpha_(l(alpha))) \) med.
$
Let $pi in Pi_alpha$ be the partition associated with $sigma$, i.e., such that $sigma_i in frak(S)(pi_i)$ for every $i$, then it is verified that
$
  tilde(op("inv"))_G (sigma)
    &= tilde(op("inv"))_G (pi) + sum_(i=0)^(l(alpha)) tilde(op("inv"))_G (sigma_i) \
    &= tilde(op("inv"))_G (pi) + sum_(i=0)^(l(alpha)) tilde(op("inv"))_(G(pi_i)) (sigma_i) med.
$
And so we can rewrite
$
  sum_(sigma in cal(N)_(G, alpha)) q^(tilde(op("inv"))_G (sigma))
    = sum_(pi in Pi_alpha) q^(tilde(op("inv"))_G (pi)) product_(i=1)^(l(alpha)) sum_(sigma_i in cal(N)_(G_i, (alpha_i))) q^(tilde(op("inv"))_(G(pi_i)) (sigma_i)) med.
$

We now turn to study the case $alpha = n$. Note that this has already been similarly discussed in @shareshian_chromatic_2016[Lemma 7.5].

#lemma()[
  Let $G$ be an interval graph with $n$ vertices.
  Let
  #grid(columns: (1fr, 1fr), align: center, [
    $K_i = { j in [i-1] mid(|) {j,i} in E }$ med,
  ], [
    $k_i = \#lr(|K_i|)$ med.
  ])
  Let us denote by $G_i$, for $i in [n]$, the subgraph of $G$ with only the first $i$ vertices.
  We then pose
  #grid(columns: (1fr, 1fr), align: center, [
    $B_(i,j) = {sigma in cal(N)_(G_i, (i)) mid(|) sigma(1) = j}$ med,
  ], [
    $b_(i,j) = sum_(sigma in B_(i,j)) q^(tilde(op("inv"))_(G_i) (sigma))$ med.
  ])
  Then the following relations hold:
  $
    cases(
      b_(1,1) &= 1 med\; ,
      b_(i,j) &= [k_i]_q b_(i-1,j) quad "if" 1 <= j < i med\; ,
      b_(i,i) &= q^(k_i) sum_(j in K_i) b_(i-1, j) med.
    )
  $
]
#proof[
  That $b_(1,1) = 1$ follows directly from the definition, while as for the other relations, unless we restrict to a subgraph, we can assume $i=n$.

  Let us now focus on $b_(n,j)$ for $j < n$.
  Let $sigma$ be in $B_(n,j)$ and let $tilde(sigma) = sigma backslash {n}$, that is, let us remove $n$ from the sequential writing of $sigma$.
  Then we have $tilde(sigma) in B_(n-1,j)$, in fact, letting $i = sigma^(-1) (n)$, we verify the following statements.
  - $tilde(sigma)$ has no $G$-descents.
    Since $sigma$ had no $G$-descents, the only possible $G$-descent of $tilde(sigma)$ is at index $i-1$, so suppose $tilde(sigma)(i-1) > tilde(sigma)(i)$ and ${tilde(sigma)(i-1), tilde(sigma)(i)} in.not E$, that is, $sigma(i-1) > sigma(i+1)$ and ${sigma(i-1), sigma(i+1)} in.not E$.
    But also $n > sigma(i+1)$ and by the interval graph property ${sigma(i+1), n} in.not E$, so $sigma$ would have a $G$-descent at index $i$, absurd.
  - $tilde(sigma)$ has no nontrivial left-to-right $G$-maxima.
    Suppose there is a left-to-right $G$-maximum at index $s > 1$.
    If $s < i$, $sigma$ would also have a left-to-right $G$-maximum at index $s$, absurd, so $s >= i$.
    Since ${tilde(sigma)(r), tilde(sigma)(s)} in.not E$ for every $r < i <= s$, by the interval graph property also ${sigma(r), sigma(i)} in.not E$ for every $r < i$.
    It follows that $sigma$ would have a left-to-right $G$-maximum at index $i$, which is absurd.

  On the other hand, we consider $tilde(sigma) in B_(n-1,j)$ and try to add $n$ to it to get an element of $B_(n,j)$.
  Since we do not have to form $G$-descents, we are forced to put $n$ just before an element of $K_n$ or as the last element. Also, in order not to introduce a left-to-right $G$-maximum, we must put $n$ after the first element of $K_n$.
  Thus we have $n$ possible choices, which introduce, respectively from the leftmost to the rightmost choice, $k_n-1, k_n-2, dots, 1, 0$ inversions.

  Since we can get each element of $B_(n,j)$ uniquely from an element of $B_(n-1,j)$ by introducing an additional number of inversions between $0$ and $k_n-1$, we have
  $
    b_(n,j) = (1 + q + dots + q^(k_n-1)) b_(n-1,j) = [k_n]_q b_(n-1,j) med.
  $

  Let us now focus on the case $j=n$.
  As before, let $sigma$ be in $B_(n,n)$ and let $tilde(sigma) = sigma backslash {n}$.
  Since there are no $G$-descents in $sigma$, we have that ${n, sigma(2)} in E$, therefore $sigma(2) in K_n$.
  We show that $tilde(sigma) in B_(n-1, sigma(2))$ by verifying the following statements.
  - $tilde(sigma)$ has no $G$-descents. This is immediate.
  - $tilde(sigma)$ has no nontrivial left-to-right $G$-maxima.
    Suppose there exists a left-to-right $G$-maximum at index $i > 1$.
    Then $tilde(sigma)(i) > tilde(sigma)(1)$ and ${tilde(sigma)(i), tilde(sigma)(1)} in.not E$, by the interval graph property also ${n, tilde(sigma)(1)} in.not E$.
    We have established that $n > sigma(2)$ and ${n, sigma(2)} in.not E$, but this is absurd since $sigma$ has no $G$-descents.

  On the other hand, fix $j in K_n$ and consider $tilde(sigma) in B_(n-1,j)$
  We show that it is possible to add $n$ to the beginning of $tilde(sigma)$ and get an element of $B_(n,n)$.
  This introduces no nontrivial left-to-right $G$-maximum and, given the choice of $j$, no $G$-descents either.
  Instead, $k_n$ additional inversions are added.

  We have shown that the elements of $B_(n,n)$ are in bijection with those of $union.big_(j in K_n) B_(n-1,j)$ and have $k_n$ more inversions, therefore
  $
    b_(n,n) = q^(k_n) sum_(j in K_n) b_(n-1,j) med.
  $
]

Given $b_i = vec(b_(i,1), dots, b_(i,i))$, we can rewrite the previous relations as
$
  b_i &= mat(
    [k_i]_q, 0, dots, 0;
    0, [k_i]_q, dots, 0;
    dots.v, dots.v, dots.down, dots.v;
    0, 0, dots, [k_i]_q;
    q^(k_i) delta_(1 in K_i), q^(k_i) delta_(2 in K_i), dots, q^(k_i) delta_(i-1 in K_i);
    ) dot b_(i-1)
$
and thus obtain
$
  sum_(sigma in cal(N)_(G, (n))) q^(tilde(op("inv"))_G (sigma))
    &= sum_(j=1)^n b_(n,j)
      = vec(1, dots.v, 1)^T dot b_n \
    &= vec(1, dots.v, 1)^T dot product_(j=1)^n mat(
      [k_j]_q, 0, dots, 0;
      0, [k_j]_q, dots, 0;
      dots.v, dots.v, dots.down, dots.v;
      0, 0, dots, [k_j]_q;
      q^(k_j) delta_(1 in K_j), q^(k_j) delta_(2 in K_j), dots, q^(k_j) delta_(j-1 in K_j);
      ) med.
$

By specializing $q |-> 1$, the previous relations are simplified as follows
$
  cases(
    b_(1,1) &= 1,
    b_(i,j) &= k_i b_(i-1,j)
  )
  quad ==> quad
  b_(i,j) = product_(l=2)^i k_l
$
in particular $b_(i,j)$ is independent of $j$.
This gives us a cleaner formula for the chromatic symmetric function.
It also tells us that, within $cal(N)_(G, (n))$, the first vertex of the permutation is equally distributed.

Also in the case of unit interval graphs a more compact formula can be found.
#corollary(name: [@shareshian_chromatic_2016[Lemma 7.5]])[
  Let $G$ be a unit interval graph with $n$ vertices.
  Then
  $
    sum_(sigma in cal(N)_(G, (n))) q^(tilde(op("inv"))_G (sigma)) = [n]_q product_(j=2)^n [k_j]_q med.
  $
]
#proof[
  Let us rewrite
  $
    sum_(sigma in cal(N)_(G, (n))) q^(tilde(op("inv"))_G (sigma)) = sum_(j=1)^n b_(n,j) med.
  $
  At this point, we can proceed by (strong) induction on $n$.
  For $n=1$ the thesis is trivially true, so let us verify the inductive step for $n=i$.

  We first calculate $b_(i,i)$, bearing in mind that $K_i = {i-k_i, i-k_i+1, dots, i-1}$:
  $
    b_(i,i)
      &= q^(k_i) sum_(j=i-k_i)^(i-1) b_(i-1, j) \
      &= q^(k_i) (sum_(j=1)^(i-1) b_(i-1, j) - sum_(j=1)^(i-k_i-1) b_(i-1, j)) \
      &= q^(k_i) ([i-1]_q product_(j=2)^(i-1) [k_j]_q - product_(j=i-k_i)^(i-1) [k_j]_q dot sum_(j=1)^(i-k_i-1) b_(i-k_i-1, j)) \
      &= q^(k_i) ([i-1]_q product_(j=2)^(i-1) [k_j]_q - product_(j=i-k_i)^(i-1) [k_j]_q dot [i-k_i-1]_q product_(j=2)^(i-k_i-1) [k_j]_q) \
      &= q^(k_i) product_(j=2)^(i-1) [k_j]_q dot ([i-1]_q - [i-k_i-1]_q) \
      &= q^(k_i) product_(j=2)^(i-1) [k_j]_q dot q^(i-k_i-1) [k_i]_q \
      &= q^(i-1) product_(j=2)^(i) [k_j]_q med.
  $

  We can now conclude
  $
    sum_(j=1)^i b_(i,j)
      &= sum_(j=1)^(i-1) b_(i,j) + b_(i,i) \
      &= [k_i]_q sum_(j=1)^(i-1) b_(i-1,j) + q^(i-1) product_(j=2)^(i) [k_j]_q \
      &= [k_i]_q [i-1]_q product_(j=2)^(i-1) [k_j]_q + q^(i-1) product_(j=1)^(i) [k_j]_q \
      &= ([i-1]_q + q^(i-1)) product_(j=1)^(i) [k_i]_q \
      &= [i]_q product_(j=2)^n [k_j]_q med.
  $
]
