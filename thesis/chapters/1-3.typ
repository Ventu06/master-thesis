#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Posets

A partially ordered set, poset for short, is a set of elements $V$ on which a partial order relation $prec$ is defined.
All posets we consider are finite.

We can represent a poset $P$ by its Hasse diagram: we draw the vertices as points and draw a line from $x$ to $y$ going upward if and only if $y$ covers $x$, that is, if $x prec y$ and there is no $z$ such that $x prec z prec y$.
Then we have $x prec y$ if and only if there is a path starting from $x$ and ending in $y$ always going upward.

For example, in @fig-poset-0-hasse we can see the Hasse diagram of the poset ${0,1,2,3}$ with the order relation of the natural numbers, while @fig-poset-1-hasse concerns the poset on powerset of ${x,y,z}$ ordered by inclusion.

#grid(
  columns: (1fr, 1fr),
  align: bottom,
  [
    #figure(caption: [$P = ({0,1,2,3}, <=)$])[#drawings.poset-0-hasse]<fig-poset-0-hasse>
  ], [
    #figure(caption: [$P = (cal(P)({x,y,z}), subset.eq)$])[#drawings.poset-1-hasse]<fig-poset-1-hasse>
  ])

Given a poset $P$, a subposet is a subset of the elements of $P$ with the induced order relation.
We say that a poset is $(n_1 + n_2 + dots + n_k)$-free if it has no subposet isomorphic to the disjoint union of a $n_1$-chain, a $n_2$-chain, etc., up to a $n_k$-chain.
In other words, if there are no subsets $V_1, dots, V_k$ of elements with $\#lr(|V_i|) = n_i$ for every $i$ and such that $x in V_i$ and $y in V_j$ are comparable if and only if $i = j$.

For example, the poset in @fig-poset-0-hasse is $(2+1)$-free, and more generally is $(n_1 + n_2)$-free for every $n_1$ and $n_2$, while the poset in @fig-poset-1-hasse is not $(2+1)$-free, since ${{x}, {x,y}}$ and ${{z}}$ are two disjoint chains of size $2$ and $1$.

Let us introduce one last notion before dealing with more specific cases.
#definition(name: [Incomparability graph])[
  Given a poset $P$, the associated #emph[incomparability graph] $op("inc")(P)$ is the graph in which the vertices are the elements of $P$ and two vertices $x$ and $y$ are connected by an edge if and only if they are not comparable in $P$.
]<def-incomp-graph>

For example, the incomparability graph of the poset in @fig-poset-0-hasse is the empty graph $E_4$, while the incomparability graph of the poset in @fig-poset-1-hasse is represented in @fig-poset-1-inc.
#figure(caption: [$G = op("inc")((cal(P)({x,y,z}), subset.eq))$])[#drawings.poset-1-inc]<fig-poset-1-inc>

We now want to focus on some particular classes of posets: those $(3+1)$-free, those $(2+2)$-free and finally both $(3+1)$-free and $(2+2)$-free posets.

As mentioned above, a poset is not $(3+1)$-free if it has a subposet isomorphic to a $(3+1)$-chain, such as the one shown on the left in @fig-poset-claw.
This condition translates into requiring the incomparability graph to have no subset of four vertices whose induced graph is isomorphic to the one shown on the right in @fig-poset-claw.
The latter four-vertex graph is called #emph[claw], and a graph with no subgraphs isomorphic to a claw is called #emph[claw-free].
#figure(caption: [left, $(3+1)$-chain; right, claw graph])[#drawings.poset-claw]<fig-poset-claw>

We have the following characterization for claw-free graphs, and consequently also for $(3+1)$-free posets, which can be found in @stanley_graph_1998.
Let $lambda$ be a partition, then we say that a graph is $lambda$-colorable if it admits a coloring in which the first color is used $lambda_1$ times, the second $lambda_2$ times, and so on.
In other words, we require that the monomial $m_lambda$ appears in the expansion of $chi_G (X)$ in the monomial basis.
We call a graph $G$ #emph[nice] if it satisfies the following condition for every $lambda$: if $G$ is $lambda$-colorable, then it is also $mu$-colorable for every $mu <= lambda$ (dominance order).
We can now state the following proposition.
#proposition(name: [@stanley_graph_1998[Proposition 1.6]])[
  A graph $G$ is claw-free if and only if $G$ and all its induced subgraphs are nice.
]<lmm-claw-free-iff-nice>

We would like to point out, however, that not all claw-free graphs are incomparability graphs of $(3+1)$-free poset.
We see a counterexample in the next section.

In the next chapter, we also see a way to represent all and only $(3+1)$-free posets, i.e., the so-called #emph[part listings].
The characterization we give is effective enough to prove one of the most important results contained in this thesis, however, it is not entirely satisfactory as a classification since we may associate several different part listings to one poset.

We now turn to the case of $(2+2)$-free posets.
We begin by introducing the notion of interval order and interval graph.

Let us consider $n$ closed intervals $[l_i, r_i] subset RR$ for $1 <= i <= n$ such that the left extreme is increasing, i.e. $l_i < l_(i+1)$ for every $i$.
To these, we associate the poset consisting of the elements $[n]$ in which, for $1 <= i < j <= n$, we have $i prec j$ if and only if the intervals $[l_i, r_i]$ and $[l_j, r_j]$ are disjoint, that is, if and only if $r_i < l_j$.
We call a poset that can be obtained in this way #emph[interval order].
It is easily seen that an interval order satisfies the following property: if $i prec j$ and $j < l$, then $i prec l$.
Conversely, it can be verified that every poset that satisfies this property can be obtained as an interval order.

We call #emph[interval graph] a graph obtained as an incomparability graph of an interval order.
Similarly to interval orders, an interval graph has the property that if $i < j < l$ and ${i,l} in E$, then also ${i,j} in E$.
Starting from a graph $G$ of vertices $[n]$ with such a property, we can construct a poset $P_G$ on $[n]$ such that it has $i prec j$ if and only if $i < j$ and ${i,j} in.not E$.
It is easily verified that $prec$ is an order relation and satisfies the property of interval orders, so $P_G$ is an interval order.
Furthermore, we can see that $op("inc")(P_G) = G$ for every graph $G$ that satisfies the above property, and that $P_(op("inc")(P)) = P$ for every interval order $P$, so we can conclude that the enunciated property completely characterizes interval graphs and that they are in direct correspondence with interval orders.

Finally, given an interval order $P$, it follows from the condition expressed above that $P$ is $(2+2)$-free.
In fact, it can be shown that the opposite is also true: every $(2+2)$-free poset is isomorphic to an interval order.
So from the above discussion, we can extract the following result.
#proposition()[
  A poset $P$ is $(2+2)$-free if and only if $op("inc")(P)$ is isomorphic to an interval graph.
]<prop-ig-iff-22-free>

For further details regarding interval orders and interval graphs, we refer to @fishburn_intransitive_1970.

Let us now look at some ways of representing interval graphs.
Given an interval graph $G$, let $h : [n] -> [n]$ be the function defined by
$
  h(i) = { max {i} union { j in [i+1, n] mid(|) {i,j} in E } } med.
$
Notice that $i <= h(i) <= n$ and that, by the interval graph property, there is an edge between $i$ and $j$, with $i < j$, if and only if $j <= h(i)$.

We can represent the function $h$ as a path on a $n times n$ grid that always lies above the diagonal, where in the $i$-th column this path makes a step to the right at the height of the $h(i)$-th row.
In this way, we have an edge between $i$ and $j$, with $i<j$, if and only if the cell at coordinates $(i,j)$ lies between the diagonal and the path.
See @fig-graph-1 for an example with $h = (4,3,5,4,5)$.

#figure(caption: [$h = (4,3,5,4,5)$])[
  #grid(columns: 2, gutter: 5em, align: center + horizon,
  [
    #drawings.interval-graph((4,3,5,4,5))
  ], [
    #drawings.graph-1
  ]
  )
]<fig-graph-1>

From these observations, it can be seen, for example, that on $n$ vertices $n!$ interval graphs can be made.

In this case, it is also easy to calculate the chromatic polynomial.
Let $g(i)$ be the number of cells to the left of cell $(i,i)$ below the path, that is, $g(i) = \#lr(|{1 <= j < i mid(|) h(j) >= i}|)$.
Then we can start coloring the vertices in order, from number $1$ to number $n$.
Once at the $i$-th vertex, this has $g(i)$ neighbors already colored.
Given $j$ and $l$ two such neighbors, that is, $j,l < i$ and ${j,i}, {l,i} in E$, by the interval graph property we also have ${j,l} in E$.
It follows that such neighbors all have different colors, so we are left with $r - g(i)$ possible colors to choose from.
We conclude that
$
  p_G (r) = product_(i=1)^n (r - g(i)) med.
$

We conclude this section with the particular case in which $P$ is both $(3+1)$-free and $(2+2)$-free.
For this case, we introduce unit interval orders and unit interval graphs.
A #emph[unit interval order] is an interval order in which each interval is $1$ long, that is, in which $r_i = l_i+1$ for $1 <= i <= n$.
In this case, the poset in question satisfies a stronger property: if $i < j < l$ and $i prec.not l$, then $i prec.not j$ and (unlike before) also $j prec.not l$.
Again it can be verified that every poset satisfying this property can be obtained as a unit interval order.

We call #emph[unit interval graph] a graph obtained as an incomparability graph of a unit interval order.
Similar to interval graphs, we see that unit interval graphs satisfy the following property: if $i < j < l$ and ${i,l} in E$, then ${i,j} in E$ and (unlike before) also ${j,l} in E$.
Exactly as before, we have a correspondence between unit interval orders and graphs satisfying the latter property.

Given a unit interval order $P$ and using the property just stated, we can see that $P$, in addition to being $(2+2)$-free, is also $(3+1)$-free.
It can be shown that this property indeed characterizes unit interval orders: every $(2+2)$-free and $(3+1)$-free poset is isomorphic to a unit interval order.
Let us then summarize the previous discussion in the following proposition.
#proposition()[
  A poset $P$ is both $(2+2)$-free and $(3+1)$-free if and only if $op("inc")(P)$ is isomorphic to a unit interval graph.
]<prop-uig-iff-22-31-free>

For the details of the statements made so far, one can start from the characterization of the $(2+2)$-free posets and try to add the new hypothesis of $(3+1)$-free.
See also @scott_foundational_1958 and @dean_natural_1968 for further details.

In this case, the function $h$ is weakly increasing.
When we go to represent it as a path in the grid, we get a path that proceeds only to the right and upward.
See the example in @fig-graph-2.

#figure(caption: [$h = (3,3,5,5,5)$])[
  #grid(columns: 2, gutter: 5em, align: center + horizon,
  [
    #drawings.interval-graph((3,3,5,5,5))
  ], [
    #drawings.graph-2
  ]
  )
]<fig-graph-2>

The possible unit interval graphs on a graph of $n$ vertices correspond to the $2n$ long Dyck paths, i.e., the paths on the $n times n$ grid made by upward and rightward steps that are always above the diagonal.
Notoriously, the number of such paths is the $n$-th Catalan number, i.e., $binom(2n,n)/(n+1)$.

Furthermore, to calculate the chromatic polynomial we can proceed as done in the case of interval graphs, but starting from vertex $n$ decreasing to vertex $1$.
In this way, we find that
$
  p_G (r) = product_(i=1)^n (r - h(i)) med.
$
