#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

#set heading(supplement: [chapter])
= Non-constructive identities

In the previous chapter, we saw how to use various structures to express the chromatic quasisymmetric function of unit interval graphs and interval graphs in the powersum basis.
The techniques used to achieve those results consisted mainly of bijections and rewritings of the chromatic quasisymmetric function.

In this chapter, we look at a different approach to obtain other expansions in other bases in the case of unit interval graphs, which do not make use of explicit bijections.

In the first section, we take up the modular law introduced in the second chapter and try to generalize it to the quasisymmetric case.
To achieve this it is necessary to impose fairly stringent conditions on the cases in which this relation would be applicable.
We see heuristically how to arrive at such cases and why it is difficult to have different or more general linear relations.

In the second section, we use the new modular law to obtain expansions of the chromatic quasisymmetric function in the Hall-Littlewood basis and of LLT polynomials in the elementary basis, in the case of unit interval graphs.
Similar to the previous chapter, we use various structures within these expansions, all strongly related to each other: sets of permutations, increasing spanning forests, and, more indirectly, orientations.

Previously we have seen how some valid results on unit interval graphs lend themselves to generalization in the case of interval graphs.
In the third section, we ask precisely this question: how can the modular law or increasing spanning forests be used on interval graphs?
For the modular law, the answer is not very satisfactory, in fact, we just analyze what goes wrong in following the path laid out for unit interval graphs and what different approaches could be applied to reach a useful result.
Instead, regarding increasing spanning forests we show some results obtained by D'Adderio in the case of interval graphs using the same structure but with a different and more constructive approach.

#include "5-1.typ"
#include "5-2.typ"
#include "5-3.typ"
