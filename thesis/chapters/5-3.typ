#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Generalizations to interval graphs

In previous sections, we have seen how to use the modular law and increasing spanning forests to obtain important results on unit interval graphs.
However, one cannot help but notice that both tools have been defined in a greater generality and consequently wonder how these results can be extended to a broader class of graphs.
As might be expected, the class in which we are most interested is interval graphs, to which we can apply a variety of techniques that work with unit interval graphs and which, as seen in the previous chapter, have shown themselves to be fertile ground on which to find new results.

Unfortunately, we were unable to reuse the modular law, while the increasing spanning forests proved useful in a different way in the @dadderio_chromatic_2023 article by D'Adderio et al.
Let us briefly look at the problems encountered and the ideas introduced.

A first question was whether there are other local linear relations, similar to the modular law, within the interval graphs.
Conjecturing such relations should not be complicated.
Having taken a certain set of graphs, we choose a basis of the quasisymmetric functions and write the chromatic quasisymmetric functions of the graphs in this basis.
At this point, we only need to check for the existence of linear relations on $QQ(q)$ between the vectors constructed.
This process, apart from the initial choice of graphs, has been fully automated.
Taking into account that there are $n!$ interval graphs with $n$ vertices, while the space of quasisymmetric functions of degree $n$ has dimension $2^(n-1)$, it is natural to obtain many linear relations.
However, the relations we found involving similar graphs, that is, with a limited number of differences in the edges, can be obtained by applying the already known modular law one or more times.
In conclusion, we believe that we have not obtained any new linear relations or particularly useful relations from the modular law.

On the other hand, the preceding observation leads us to think that the modular law is capable of generating all the linear relations between the chromatic quasisymmetric functions of interval graphs, and in particular that it is possible, in analogy to @thm-mod-law-det-cmp, to choose base graphs to which to recur from any interval graph.

We begin by reformulating the modular law as expressed in @def-mod-law in the case of interval graphs, similar to what was done in @def-mod-law-uig.
Recall that interval graphs are indexed by the functions $h: [n] -> [n]$ that lie above the diagonal, let $cal(I)$ be the set of these.
#definition(name: [Modular law for interval graphs])[
  A certain function $f : cal(I) -> cal(A)$ satisfies the modular law if
  $
    f(h_2) + q f(h_0) = (1+q) f(h_1)
  $
  holds in the following cases.
  - There exists $i in [n-1]$ such that $h^(-1)(h(i)) = {i}$ and $h_1(h_1(i)) = h_1(h_1(i)+1)$.
    Moreover $h_0(j) = h_1(j) = h_2(j)$ for every $j eq.not i$, while $h_0(i) = h_1(i)-1$ and $h_2(i) = h_1(i)+1$.
  - There exists $i in [n-1]$ such that $h_1(i+1) = h_1(i)+1$ and $h_1^(-1)(i) = emptyset$.
    Moreover $h_0(j) = h_1(j) = h_2(j)$ for every $j eq.not i, i+1$, while $h_0(i) = h_0(i+1) = h_1(i)$ and $h_2(i) = h_2(i+1) = h_1(i+1)$.
]<def-mod-law-ig>

Experimentally, we have noticed that the span of the chromatic quasisymmetric functions of interval graphs is all $"QSym"[X]$, so it makes sense to look for a minimal subset of interval graphs of the same space dimension, i.e., $2^(n-1)$.
A possible candidate might be
$
  cal(I)_"col" = {h in cal(I) mid(|) h(i) in {i,n} med forall i in [n]} med.
$
We can verify experimentally how the chromatic quasisymmetric functions of $cal(I)_"col"$ graphs have as their span all $"QSym"[X]$, moreover, graphs of this form also seem to lend themselves well as "extremal points" of the modular law.
Unfortunately, we have not been able to reduce, by modular law, each graph to a combination of graphs in $cal(I)_"col"$.

Reflecting on the proof of @thm-mod-law-det-cmp, it can be seen that the original algorithm does not lead each graph back to a combination of $K_lambda$, with $lambda$ partition, but only $K_alpha$, with $alpha$ composition.
Only later is it shown that the order in which the complete graphs are joined is irrelevant.

In the first part of the proof, the coefficients appearing in the modular law are (almost) irrelevant: what matters is only the property of "being in a relation", a property that satisfies the following conditions:
- ${h_0, h_1, h_2}$ are in a relation for every $h_0$, $h_1$ and $h_2$ that satisfy the modular law;
- if $H$ and $K$ are two sets of elements in relation, $H eq.not K$ and $h in H sect K$, then $(H union K) backslash {h}$ are in relation.
In this way, we are looking for a minimal subset of interval graphs such that for each interval graph there exists a relation containing him and other graphs taken from this subset.
This formulation is completely combinatoric and potentially presents two problems in being applied to linear relations:
- it does not take into account any cancellations between terms, with the risk of arriving at a relation of the type $0=0$;
- it ignores any distinct relations on the same set of elements, which could lead to a reduction of the variables involved.
However, we do not consider these two cases an obstacle to finding a first subset of graphs to reduce to.
For example, with this formulation, it is possible to reduce from the unit interval graphs to the set of complete graphs $K_alpha$, with $alpha$ composition.
However, we could not reduce from the interval graphs to $cal(I)_"col"$, a sign that we should consider a larger subset of $cal(I)$.

The second part of the proof, on the other hand, makes greater use of the coefficients of the modular law, to derive equalities that would not be possible to obtain by the first method alone.
In the case of interval graphs, as well as unit interval graphs, we think about the decision of which equalities to obtain and by which methods should be made after finding a satisfactory subset to which to reduce.

Finally, in the case of unit interval graphs, we immediately led back to the disjoint union of complete graphs whose chromatic quasisymmetric function is a multiple of an elementary symmetric function.
In contrast, for interval graphs, we have not found basis graphs with a vaguely clean expression, or one that is at least expressible by a closed formula in one of the known bases.

The modular law continues to be a valuable tool for studying chromatic quasisymmetric functions in the case of interval graphs, however, for the reasons shown so far, we do not think it too likely that we will be able to obtain a result similar to that for unit interval graphs.
Nevertheless, it is definitely worthwhile to conduct additional studies possibly similar to those shown here.

Let us conclude the discussion of the modular law with an example: in @tab-mod-law-ig-4 we list all the relations between interval graphs with $4$ vertices.
Recall the modular law expressed as
$
  (1+q) f(h_1) = q f(h_0) + f(h_2) med.
$
We report in above the relations only between unit interval graphs and below the remaining ones.

#figure(caption: [Modular law for interval graphs with 4 vertices])[#table(
  columns: 3,
  stroke: none,
  align: horizon+center,
  table.hline(),
  table.header([$h_1$], [$h_0$], [$h_2$]),
  table.hline(stroke: .5pt),
  [$(2,3,3,4)$], [$(1,3,3,4)$], [$(3,3,3,4)$],
  [$(2,3,3,4)$], [$(2,2,3,4)$], [$(3,3,3,4)$],
  [$(1,3,4,4)$], [$(1,2,4,4)$], [$(1,4,4,4)$],
  [$(1,3,4,4)$], [$(1,3,3,4)$], [$(1,4,4,4)$],
  [$(2,3,4,4)$], [$(2,2,4,4)$], [$(3,3,4,4)$],
  [$(2,3,4,4)$], [$(2,2,4,4)$], [$(2,4,4,4)$],
  [$(3,3,4,4)$], [$(3,3,3,4)$], [$(3,4,4,4)$],
  [$(2,4,4,4)$], [$(1,4,4,4)$], [$(3,4,4,4)$],
  [$(3,4,4,4)$], [$(2,4,4,4)$], [$(4,4,4,4)$],
  [$(3,4,4,4)$], [$(3,3,4,4)$], [$(4,4,4,4)$],
  table.hline(stroke: .5pt),
  [$(3,4,3,4)$], [$(3,3,3,4)$], [$(4,4,3,4)$],
  [$(3,2,4,4)$], [$(2,2,4,4)$], [$(4,2,4,4)$],
  [$(4,3,4,4)$], [$(4,2,4,4)$], [$(4,4,4,4)$],
  [$(4,3,4,4)$], [$(4,3,3,4)$], [$(4,4,4,4)$],
  table.hline(),
)]<tab-mod-law-ig-4>

Let us now discuss increasing spanning forests.
This structure was already known before the article @abreu_symmetric_2021, e.g., in @hallam_factoring_2015 it is shown how the $k$-th coefficient of the chromatic polynomial of a $G$ graph counts the number of increasing spanning forests consisting of exactly $k$ trees.

In @dadderio_chromatic_2023[Proposition 6.12], D'Adderio et al. constructed, for each interval graph $G$, a function $Phi_G : op("PC")(G) -> op("ISF")(G)$ such that, for each proper coloring $kappa$, we have
$
  op("wt")_G (Phi_G (kappa)) + n - l(lambda(Phi_G (kappa))) = op("coinv")_G (kappa)
$
where $l(lambda(Phi_G (kappa)))$ is the number of trees of $Phi_G (kappa)$, so $n - l(lambda(Phi_G (kappa)))$ is the number of edges in the forest.
Starting from the definition of a chromatic quasisymmetric function, we obtain the following formula
$
  chi_G^q (X;q)
  &= sum_(F in op("ISF")(G)) q^(op("wt")_G (F) + n - l(lambda(F))) sum_(kappa in Phi_G^(-1) (F)) x_kappa \
  &= sum_(F in op("ISF")(G)) q^(op("wt")_G (F) + n - l(lambda(F))) cal(Q)_F^((G)) med.
$
Also in the same article, several expressions are proposed for $cal(Q)_F^((G))$ in the monomial, fundamental and, as a conjecture, in the powersum basis.

All these results are based on the fact that, by the construction of the map $Phi_G$, $Phi_G (kappa)$ depends exclusively on the set of coinversions of $kappa$.
It therefore turns out to be possible to group different colorings according to their set of coinversions and simply find a formula for each of these groups, a much easier task since, having fixed this set, the possible colorings are easy to determine.
We can therefore say that the idea behind increasing spanning forests is their ability to catalog all possible sets of coinversions of proper colorings.

Similar reasoning to the one made for the chromatic quasisymmetric functions can be applied to LLT polynomials, arriving at a formula similar to the previous one.
By putting these two identities together, one is able to extend the Carlsson-Mellit reciprocity formula (@thm-cqsf-rel-llt) to the case of interval graphs (see @dadderio_chromatic_2023[Theorem 10.1]).

Unfortunately, no formula similar to that of @thm-cqsf-exp-rho-isf-1 for interval graphs is currently known, or at any rate, one that depends almost exclusively on increasing spanning forests and only secondarily on the graph.

As we had already mentioned at the beginning of the chapter on magic spanning forests, the idea of introducing the latter structure came about after we realized that the increasing spanning forests were too small as a set to be related to $cal(N)_(G, alpha)$, but we did not want to give up on the forests as they have shown great ability in providing information about the graph.
Although each increasing spanning forest is also a magic spanning forest, so far the two have been used quite differently, considering different weights and obtaining unrelated formulas.
For this reason, we do not think it is correct to call magic spanning forests a generalization of the increasing ones.
