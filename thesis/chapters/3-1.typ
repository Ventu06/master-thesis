#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Chromatic quasisymmetric functions

As we saw in the first chapter, the chromatic symmetric function represents a significantly more sophisticated and refined invariant than the chromatic polynomial, in fact, it also contains information about the colors used.
However, it does not give us, at least directly, any information about the arrangement of these colors, except for the hypothesis of proper coloring.
To obtain a little additional information we are going to consider a $q$-analog of the chromatic symmetric function, that is a generalization of the latter in which an additional variable $q$ is added.
This new invariant is called the chromatic quasisymmetric function, and it was introduced by Shareshian and Wachs in the articles @shareshian_chromatic_2012 and @shareshian_chromatic_2016.

From now on, all the graphs we are going to consider have as their set of vertices $V = [n]$, ordered naturally.
This is important because the chromatic quasisymmetric function depends on the order of the vertices.

Let us begin with a preliminary definition.
#definition(name: [Coinversion])[
  Let $G$ be a graph and $kappa$ a proper coloring.
  A #emph[coinversion] is an edge ${i,j} in E$, with $i<j$ and $kappa(i) < kappa(j)$.
  Let
  $
    op("CoInv")_G (kappa) = { {i,j} in E mid(|) i < j " and " kappa(i) < kappa(j)}
  $
  the set of coinversions and
  $
    op("coinv")_G (kappa) = \#lr(|op("CoInv")_G (kappa)|)
  $
  the number of coinversions.
]<def-coinv>

We can now define our generalization of the chromatic symmetric function.
#definition(name: [Chromatic quasisymmetric function])[
  The #emph[chromatic quasisymmetric function] of a graph $G$ is
  $
    chi_G^q (X;q)
      = sum_(kappa in op("PC")(G)) q^(op("coinv")_G (kappa)) x_kappa
      = sum_(kappa in op("PC")(G)) q^(op("coinv")_G (kappa)) product_(v in V) x_(kappa(v)) med.
  $
]<def-cqsf>

The first thing that can be noticed is that by posing $q |-> 1$ we regain the chromatic symmetric function.

Then see that $chi_G^q (X;q)$ is no longer necessarily a symmetric function: in fact, by permuting colors, the number of coinversions, and thus the coefficient in front of the monomial $x_kappa$, can vary; it follows that the argument used with chromatic symmetric functions can no longer be applied.
However, the function $chi_G^q (X;q)$ is quasisymmetric: in fact, by shifting the colors, the relative order of the colors does not change, and consequently, the number of coinversions does not change either.

Let us look at a (counter)example right away.
#counterexample(name: [Chromatic quasisymmetric function which is not symmetric])[
  #grid(columns: (3fr, 1fr), align: horizon,
  [
    Let $G$ be the interval graph given by $h = (3,2,3)$. Then we can calculate:
    $
      chi_G (X)
        &= 6 m_(1^3) + m_(2,1) = 6 M_(1,1,1) + (M_(1,2) + M_(2,1)) med, \
      chi_G^q (X;q)
        &= 2(q^2+q+1) M_(1,1,1) + (q^2 M_(1,2) + M_(2,1)) med.
    $
    As can be seen, $chi_G^q (X;1) = chi_G (X)$, but already in this simple case $chi_G^q (X;q)$ is not symmetric.
  ],[
    #align(center)[#drawings.interval-graph((3,2,3))]
  ])

]<cntexm-cqsf-not-sym>

Let us look at a couple more examples.
#example(name: [Chromatic quasisymmetric function of some graphs])[
  - For the empty graph $E_n$ we have
    $
      chi_(E_n)^q (X;q) = e_(1^n)
    $
    since there are no edges and so $chi_(E_n)^q (X;q) = chi_(E_n) (X)$.
  - For the complete graph $K_n$ we have
    $
      chi_(K_n)^q (X;q) = [n]_q ! dot m_(1^n) = product_(i=1)^n (1-q^n)/(1-q) dot m_(1^n) med.
    $
    In fact, we have to choose a different color for each vertex and then we need to assign $n$ different colors to $n$ vertices.
    It follows that $chi_(K_n)^q (X;q) = sum_(sigma in frak(S)_n) q^(op("coinv")(sigma)) m_(1^n)$, where $sigma$ represents the relative order of the colors and $op("coinv")(sigma) = \#lr(|{ 1 <= i < j <= n mid(|) sigma(i) < sigma(j) }|)$.
    $op("coinv")(sigma)$ is a so-called Mahonian statistic and can easily be shown by induction on $n$ that $sum_(sigma in frak(S)_n) q^(op("coinv")(sigma)) = [n]_q !$.
    We refer to @macmahon_indices_1913 for additional details on similar statistics.
  - #grid(columns: (2fr, 1fr), align: horizon,
    [
      For the graph in the figure, which we recognize to be the interval graph given by $h = (4,2,4,4)$, we have
      $
        chi_G^q (X;q)
          &= &(3 q^4 + 6 q^3 + 6 q^2 + 6 q + 3) &M_(1,1,1,1) \
          &+ &(q^4 + 2 q^3 + q^2) &M_(1,1,2) \
          &+ &(q^4 + q^3 + q + 1) &M_(1,2,1) \
          &+ &(q^2 + 2 q + 1) &M_(2,1,1) med.
      $
      This time we skip the calculation.
    ],[
      #align(center)[#drawings.graph-0]
    ])
]<exm-cqsf>

We have seen that the chromatic quasisymmetric function of the empty and complete graphs, which we note are both unit interval graphs, is actually symmetric.
This is no accident as the following theorem holds.
#theorem(name: [@shareshian_chromatic_2016[Theorem 4.5]])[
  Let $G$ be a unit interval graph, then $chi_G^q (X;q)$ is a symmetric function.
]<thm-uig-cqsf-sym>
The idea behind this fact lies in constructing an involution $psi_a : op("PC")(G) -> op("PC")(G)$ that exchanges the number of vertices colored with the colors $a$ and $a+1$ while preserving the rest of the coloring and the coinversion statistics.

As in @prop-csf-exp-m-1, we have an expansion of the chromatic quasisymmetric function in the monomial basis (see @brosnan_unit_2018[Proposition 19]).
We say that an ordered partition $pi = (B_1, dots, B_k)$ of $V$ is stable if each block is totally disconnected, that is, if there are no edges connecting vertices within the same block.
The proof of this result is almost identical to the one of @prop-csf-exp-m-1, that is, just color each block the same color and essentially rewrite the definition, but this time we notice that the order of the blocks in the vertex partition is relevant.
#proposition()[
  For a graph $G$ we have
  $
    chi_G^q (X;q) = sum_(pi "stable") q^(op("coinv")_G (pi)) M_(alpha(pi))
  $
  where $alpha(pi) = (\#lr(|B_1|), dots, \#lr(|B_k|))$ and
  $
    op("coinv")_G (pi)
      &= \#lr|{ {i,j} in E mid(|) i < j, i in B_r, j in B_s, r < s}|) \
      &= sum_(1 <= r < s <= k) \#lr(|{ (i,j) in B_r times B_s mid(|) {i,j} in E, i < j}|) med.
  $
]<prop-cqsf-exp-m-1>

As proved again in @shareshian_chromatic_2016[Theorem 3.1] along the lines of a Chow result obtained in @chow_descents_1999[Corollary 2], we also have an expansion in the fundamental basis, this time only for unit interval graphs.
Given a permutation $sigma in frak(S)_n$, let
$
  op("inv")_G (sigma) &= \#lr(|{ {i,j} in E mid(|) i < j, sigma(i) > sigma(j) }|) \
  op("Des")_G (sigma) &= { 1 <= i < n mid(|) {i,i+1} in.not E, sigma(i) > sigma(i+1) } med.
$
#theorem()[
  Let $G$ be a unit interval graph, then
  $
    chi_G^q (X;q) = sum_(sigma in frak(S)_n) q^(op("inv")_G (sigma)) F_(n, [n-1] backslash op("Des")_G (sigma)) med.
  $
]
The proof of this result is quite technical and is not covered.

As in the case of chromatic symmetric functions, we have the following result.
#proposition()[
  If $G_1$, $G_2$ are graphs and $G = G_1 union.sq G_2$ is their disjoint union, then
  $
    chi_G^q (X;q) = chi_(G_1)^q (X;q) chi_(G_2)^q (X;q) med.
  $
]<prop-cqsf-union>
#proof[
  The proof is similar to that of @prop-csf-union.
  This time, for a coloring $kappa = (kappa_1, kappa_2) in op("PC")(G)$ with $kappa_1 in op("PC")(G_1)$ and $kappa_2 in op("PC")(G_2)$ we have
  $
    q^(op("coinv")(kappa)) x_kappa
      &= q^(op("coinv")(kappa_1) + op("coinv")(kappa_2)) x_(kappa_1) x_(kappa_2) \
      &= (q^(op("coinv")(kappa_1)) x_(kappa_1)) (q^(op("coinv")(kappa_2)) x_(kappa_2))
  $
  and from here the thesis follows.
]

The modular law we enunciated and verified in @lmm-csf-mod-law still turns out to be valid, provided we consider a $q$-analog of it and do the appropriate verification.
We will see this result in more detail in @def-mod-law and the important implications it will have in determining chromatic quasisymmetric functions on unit interval graphs.

In contrast, we do not have a similar expansion to @thm-csf-exp-p-1 or @thm-csf-exp-p-2 in a refinement of the powersum basis.
In fact, following the proof of these results, it is inconvenient to keep track of coinversions in the various colorings, clearly unless expanding everything into the monomial basis.
However, other combinatorial methods use particular structures to obtain an expansion in the power basis $Psi_alpha$: the next chapter will be devoted precisely to investigating these results.
