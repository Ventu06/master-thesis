#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

#set heading(numbering: none)
= Introduction

The main topic of this thesis is chromatic quasisymmetric functions.
These are a particular algebraic invariant associated with colorings of finite graphs, and consist of a generalization of the well-known symmetric functions.
In recent times they have received a fair amount of attention following several developments that have both advanced the theory, thus opening up new research directions, and found new connections with other topics, even outside combinatorics.

There are two main reasons to study these functions.
The first one is the Stanley-Stembridge conjecture and the subsequent generalization by the Shareshian-Wachs conjecture.
These are important long-standing open problems within algebraic combinatorics that assert a positive expansion of chromatic quasisymmetric functions in the elementary basis of symmetric functions for certain classes of graphs.
As typical in this branch of mathematics, a positive expansion is often indicative of some hidden structure with which a (quasi)symmetric function can be associated, and proving positivity is often equivalent to discovering such a structure.
An immediate example is provided by the theory of symmetric group representations, in which it is possible to establish a correspondence between these representations and the algebra of symmetric functions.
In this way, being able to associate particular representations with chromatic quasisymmetric functions could lead to a proof of the afromentioned conjectures.

It is precisely this last topic that introduces us to the second reason, namely, the links that chromatic quasisymmetric functions have with other structures found in mathematics, such as LLT polynomials or, even better, with Hessenberg varieties.
The connection with the latter is special, given that combinatorial objects are related to geometric objects in a nontrivial way, so much so that the proof of this link is relatively recent.
In particular, it is established that a certain action of the symmetric group on the cohomology groups of Hessenberg varieties associated with particular graphs corresponds to the chromatic quasisymmetric function of these graphs.
This is an example of the hidden structure mentioned earlier, from which positivity in the Schur basis can be deduced, but unfortunately not the positivity in the elementary basis.

In the first three chapters of the thesis, we analyze in more details the ideas just expressed.
We keep a very general approach, showing step-by-step how we came to formulate these definitions and problems from the simplest and most natural ideas.
We also do not disdain to digress a bit to show similar facts and problems that arise during the construction of this theory and that can help to frame chromatic quasisymmetric functions in a broader context.

Specifically, in the first chapter we start from the chromatic polynomial to arrive at chromatic symmetric functions and we see the research problems that already emerge at this level of generality, above all, the Stanley-Stembridge conjecture.
In the second chapter, we show a fundamental reduction for this conjecture due to Guay-Paquet.
In the third chapter, we finally present the central topics of our thesis, namely, chromatic quasisymmetric functions and the Shareshian-Wachs conjecture, which, thanks to the proof carried out in the previous chapter, we can assert it generalizes the Stanley-Stembridge conjecture.
Also in this last chapter, we will look at some recent results on this conjecture and its links with the other topics already mentioned, namely Hessenberg varieties and LLT polynomials.

The second part of the thesis, consisting of the fourth and fifth chapters, focuses on more specific topics, intending to illustrate some results in the literature, how they relate to each other, and, in a small part, to propose some new ideas as well.
The goal is to obtain formulas for the expansion of chromatic quasisymmetric functions of unit interval graphs in different bases and then try to generalize these results to interval graphs.

In these two chapters, we address this problem with approaches that share some similarities but differ substantially.
In the fourth chapter, we introduce three structures, the set of permutations $cal(N)_(G,alpha)$, acyclic orientations, and magic spanning forests, which we use to write the chromatic quasisymmetric function in the powersum basis.
- The first one was already in the literature and it was used to obtain a formula in the case of unit interval graphs, while for interval graphs a similar formula has recently been proved, although it has not yet been published.
- The second structure was also well known, in particular, it was used to obtain the expansion of the chromatic quasisymmetric function in the powersum basis for any finite graph.
- The third structure, on the other hand, is treated for the first time in this thesis and was constructed by taking inspiration from increasing spanning forests, an additional structure that we will see in the next chapter, trying to replicate the properties characterizing acyclic orientations.
Known results using such structures are particularly constructive in the sense that the main tools used are bijections and rewritings of known identities.
Working with these structures, one realizes that they are essentially expressing the same concept with different languages.
For this reason, we have constructed bijections between acyclic orientations, $cal(N)_(G, alpha)$ and magic spanning forests, in order to make these languages interchangeable.
This leads to a second proof of the formula concerning $cal(N)_(G, alpha)$ in the case of interval graphs and to a new formula for magic spanning forests, again in the generality of interval graphs.

The fifth chapter is similar to the fourth since we try to obtain expansions for the chromatic quasisymmetric function using other structures, but similar to the previous ones: sets of permutations, increasing spanning forests and acyclic orientations.
Here the relations between these structures are less complicated and, in particular, already known in the literature.
What is different from the previous chapter is the way we obtain the formulas: the main tool is the modular law, a linear relation between functions defined on graphs similar to each other that is satisfied by chromatic quasisymmetric functions.
This law allows a formula to be locally checked to obtain a global identity, therefore resulting in a less constructive identity than the ones found in the previous chapter.
The currently known results involving increasing spanning forests mostly concern unit interval graphs.
In the wake of the results verified in the previous chapter for interval graphs and of some identities found by D'Adderio using increasing spanning forests, we attempt to extend the modular law and some formulas to this broader class of graphs.
Since this attempt is unsuccessful, we analyze what reasons prevent this generalization and provide some ideas for future approaches.


== Acknowledgements

First of all, I would like to express my gratitude to my supervisor, Prof. Michele D'Adderio, who introduced me to this topic, guided me throughout my thesis work and was always ready to listen to the ideas I proposed and reciprocate with helpful advice.
Beyond the dissertation, I really appreciated his efforts to create a small mathematical community with courses and lectures in which he immediately involved me, as well as the engaging informal discussions we had about the future, academia, and other disparate topics.

I want also to thank PhD student Giovanni Interdonato, with whom I shared many afternoons discussing and tackling together the problems I reported in this thesis.
Without his help, most of the innovative ideas present in this paper would never have existed, and this experience would have been much less enjoyable.

I am then grateful to Prof. Andrea Maffei, my bachelor's thesis advisor from whom I later also received good advice for the future.

I then thank the organizers of the Math Olympiads, who through these competitions directed me toward this wonderful subject, making me have fun and at the same time realize that I had potential to express.
First of all, my professor in my high school days, Alberto Branciari, who always encouraged and supported me in participating in these competitions.

Next, I would like to thank all the people with whom I spent these 5 years in Pisa, first of all Chiara, Giacomo, Matteo, Federico, Sebastiano, Antonio, Antonio, Bernardo and all my course and college friends.
Thanks to you, this college experience has gone far beyond academics, leaving me with wonderful experiences and friendships that I intend to continue to cultivate despite the distance.
I doubt that I will find myself living in another community so welcoming and well suited to me.

Finally, I thank infinitely my family, who have always provided me with all the support I needed, both in these and previous years.
They have always supported me in the decisions and paths I chose to take without ever burdening me with anything, although they have endured my distance.