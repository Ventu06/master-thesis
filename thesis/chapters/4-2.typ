#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Acyclic orientations

As already seen in @thm-cqsf-e-coeff-ao, acyclic orientations are a useful and natural structure to consider on a graph.
In this section, we see how they can be used to express the chromatic quasisymmetric function of a generic graph through reverse $P$-partitions, another very recurrent structure in combinatorics.
In the recent article @alexandersson_p-partitions_2021 by Alexandersson and Sulzgruber, a way of expanding the generating functions of the reverse $P$-partitions in the quasisymmetric powersum basis is explained, leading to the main identity of the section.
Finally, we establish a bijection between acyclic orientations and $cal(N)_(G, alpha)$, concluding with a proof of @thm-cqsf-exp-psi-nga-1.


Let us first recall a definition given earlier.
#definition(name: [Acyclic orientation])[
An #emph[acyclic orientation] of a graph $G$ is a choice of a direction for each edge so that no direct cycles, that is, closed paths along the chosen direction, are formed.
We denote by $op("AO")(G)$ the set of all acyclic orientations of $G$.
]<def-ao>
Given an acyclic orientation $theta in op("AO")(G)$ and vertices $i,j$, we write $i -> j$ or $theta(i, j) = i -> j$ if there is an edge between $i$ and $j$ and it is oriented from $i$ towards $j$.
We further say that a pair of vertices $i,j$ forms an #emph[inversion] if $i > j$ and $i -> j$.
Let us denote by $op("inv")_G (theta)$ the number of inversions, thus
$
  op("inv")_G (theta) = \#lr(|{ {i,j} in E mid(|) i > j "and" i -> j }|) med.
$

We then say that a coloring $kappa in op("PC")(G)$ is adapted to an orientation $theta in op("AO")(G)$ if $kappa(i) > kappa(j)$ for each $i -> j$.
Clearly, for each coloring, there is only one orientation to which it is adapted.
Conversely, given an orientation $theta$, the set of adapted colorings is
$
  { kappa in op("PC")(G) mid(|) forall {i,j} in E, i -> j => kappa(i) > kappa(j) } med.
$

Let us rephrase this in a more general way.
An acyclic orientation $theta$ induces a poset $P_theta$ on $V$, where $i prec.eq_theta j$ if there exists a path from $i$ to $j$ along the edges oriented in the direction given by $theta$.
That $prec.eq_theta$ is indeed an order relation follows from the acyclicity condition.

Given a poset $P$, we can consider weakly decreasing functions $f : P -> NN^+$, i.e. such that $f(i) >= f(j)$ if $i prec.eq_P j$; these are called #emph[reverse P-partitions] and the corresponding generating function is
$
  K_P = K_P (X) = sum_(f : P -> NN^+ \ i prec.eq_P j => f(i) >= f(j)) product_(i in P) x_(f(i)) med.
$
Similarly, strictly decreasing functions $f : P -> NN^+$, i.e. such that $f(i) > f(j)$ if $i prec_P j$ are called #emph[strict reverse P-partitions] and the corresponding generating function is
$
  K_P^s = K_P^s (X) = sum_(f : P -> NN^+ \ i prec_P j => f(i) > f(j)) product_(i in P) x_(f(i)) med.
$

Note how the colors adapted to a certain orientation $theta in op("AO")(G)$ are precisely the strict reverse $P_theta$-partitions.
From this, we can deduce the following formula:
$
  chi_G^q (X;q)
    &= sum_(kappa in op("PC")(G)) q^(op("coinv")_G (kappa)) x_kappa \
    &= sum_(theta in op("AO")(G)) q^(op("inv")_G (theta)) K_(P_theta)^s med.
$

It is therefore sufficient to study $K_P^s$ for a generic poset $P$.
From the definition, we can immediately see that $K_P$ and $K_P^s$ are quasisymmetric functions, which we can then expand in the known bases.
First of all, a preliminary result (@alexandersson_p-partitions_2021[Lemma 4.4]) tells us that $omega(K_P^s) = K_P$.

Then, to expand $K_P$, we must introduce two new elements: $cal(L_alpha^* (P))$ and $cal(O)_alpha^* (P)$.
We are not particularly interested in the former, and it is sufficient to know that it is a set of $alpha$-unimodal linear extensions of $P$.
The second is more important in our case.
We start by defining $cal(O) (P)$ as the set of surjections $f : P -> [k]$, for some $k in NN^+$, that preserves the order of $P$.
Given $f in cal(O) (P)$, we define the #emph[type] of $f$ as
$
  alpha(f) = (\#lr(|f^(-1) (1)|), dots, \#lr(|f^(-1) (k)|))
$
and denote by $cal(O)_alpha (P)$ the functions in $cal(O) (P)$ of type $alpha$.
Finally, let $cal(O)^* (P)$ be the set of functions $f in cal(O) (P)$ such that $f^(-1) (i)$ has a unique minimal element for each $1 <= i <= k$ and we set $cal(O)_alpha^* (P) = cal(O)_alpha (P) sect cal(O)^* (P)$.

Let us now enumerate a couple of results.
The first (@alexandersson_p-partitions_2021[Proposition 5.2]) establishes a bijection between $cal(L)_alpha^* (P)$ and $cal(O)_alpha^* (P)$, so $\#lr(|cal(L)_alpha^* (P)|) = \#lr(|cal(O)_alpha^* (P)|)$.
The second(@alexandersson_p-partitions_2021[Theorem 4.2]) gives us the following expansion:
$
  K_P = sum_(alpha tack.r.double n) \#lr(|cal(L)_alpha^* (P)|) Psi_alpha / z_alpha med.
$

Given an acyclic orientation, we also define a source as a vertex at which all incident edges have direction exiting the vertex.
Putting together all the results seen so far, we can state the following.
#theorem(name: [@alexandersson_p-partitions_2021[Theorem 7.4]])[
  Let $G$ be a generic graph with $n$ vertices, then
  $
    omega(chi_G^q (X;q))
      &= sum_(theta in op("AO")(G)) q^(op("inv")_G (theta)) sum_(alpha tack.r.double n) Psi_alpha / z_alpha \#lr(|cal(O)_alpha^* (P_theta)|) \
      &= sum_(theta in op("AO")(G)) q^(op("inv")_G (theta)) sum_(f in cal(O)^* (theta)) Psi_(alpha(f)) / z_(alpha(f))
  $
  where $cal(O)^* (theta)$ are the surjections $f : V -> [k]$, for some $k in NN^+$, weakly increasing along the orientation of the edges and such that $f^(-1) (i)$ has only one source for each $1 <= i <= k$.
]<thm-cqsf-exp-psi-ao-1>

For convenience, let us define
$
  op("AO")^* (G) &= { (theta, f) in op("AO")(G) times (NN^+)^V mid(|) f in cal(O)^* (theta)} med, \
  op("AO")_alpha^* (G) &= { (theta, f) in op("AO")(G) times (NN^+)^V mid(|) f in cal(O)^* (theta) "and" alpha(f) = alpha } med.
$
Then the previous identity becomes
$
  omega(chi_G^q (X;q))
    &= sum_((theta, f) in op("AO")^* (G)) q^(op("inv")_G (theta)) Psi_(alpha(f)) / z_(alpha(f)) \
    &= sum_(alpha tack.r.double n) Psi_alpha / z_alpha sum_((theta, f) in op("AO")_alpha^* (G)) q^(op("inv")_G (theta)) med.
$

If $G$ is a unit interval graph, we know that $chi_G^q (X;q)$ is a symmetric function, so we can rewrite the previous formula as
$
  omega(chi_G^q (X;q))
    &= sum_((theta, f) in op("AO")^* (G)) q^(op("inv")_G (theta)) binom(l(lambda(f)), m_1 (lambda(f)), dots, m_n (lambda(f)))^(-1) p_(lambda(f)) / z_(lambda(f)) \
    &= sum_(lambda tack.r n) p_lambda / z_lambda sum_((theta, f) in op("AO")_lambda^* (G)) q^(op("inv")_G (theta))
$
where $m_i (lambda)$ is the multiplicity of $i$ in the partition $lambda$.

The rest of the section is devoted to studying the relation between $op("AO")_alpha^* (G)$ and $cal(N)_(G, alpha)$ for an interval graph $G$, and, in particular, to prove the following theorem.
#theorem()[
  Let $G$ be an interval graph, then there exists a bijection $Phi : cal(N)_(G, alpha) -> op("AO")_alpha^* (G)$ such that $tilde(op("inv"))_G (sigma) = op("inv")_G (Phi(sigma))$.
]<thm-nga-ao-bij>
Notice that @thm-cqsf-exp-psi-nga-1 immediately follows from this.

#proof[
  We proceed as follows.
  + Given $sigma in cal(N)_(G, alpha)$, we construct a total order $prec_sigma$ on $V$, which in turn uniquely determines $sigma$.
  + Given $(theta, f) in op("AO")_alpha^* (G)$, we construct a total order $prec_(theta, f)$ on $V$, which in turn uniquely determines the pair $(theta, f)$.
  + We construct $Phi : cal(N)_(G, alpha) -> op("AO")_alpha^* (G)$ such that $i prec_(sigma) j$ if and only if $i prec_(Phi(sigma)) j$.
  + We construct $Psi : op("AO")_alpha^* (G) -> cal(N)_(G,alpha)$ such that $i prec_(theta, f) j$ if and only if $i prec_(Psi(theta, f)) j$.

  At this point, we would have that $\( prec_sigma \) = \( prec_(Phi(sigma)) \) = \( prec_(Psi(Phi(sigma))) \)$, and since the order uniquely determines $sigma$, we get $Psi compose Phi = op("id")_(cal(N)_(G, alpha))$.
  Similarly $\( prec_(theta, f) \) = \( prec_(Psi(theta, f)) \) = \( prec_(Phi(Psi(theta, f))) \)$, from which $Phi compose Psi = op("id")_(op("AO")_alpha^* (G))$.
  It follows that $Phi$ and $Psi$ are bijections.

  To see that they preserve the weight, we begin by remembering that
  $
    tilde(op("inv"))_G (sigma) &= \#lr(|{ {i,j} in E mid(|) i < j "and" sigma^(-1) (i) > sigma^(-1) (j) }|) med, \
    op("inv")_G (theta) &= \#lr(|{ {i,j} in E mid(|) i < j "and" i <- j}|) med.
  $
  Then, once the total orders $prec_sigma$ and $prec_(theta, f)$ have been constructed, it suffices to note that, for ${i,j} in E$, both have $sigma^(-1) (i) < sigma^(-1) (j)$ if and only if $i prec_sigma j$, and $i -> j$ if and only if $i prec_(theta, f)j$.

  Let us therefore proceed with the proof.

  + Given $sigma in cal(N)_(G, alpha)$, we define the total order $prec_sigma$ on $V$ by $i prec_sigma j$ if $sigma^(-1) (i) < sigma^(-1) (j)$, that is, $prec_sigma$ is the natural order induced by $sigma^(-1)$.
    It is clear that this is a total order and that one can univocally reconstruct $sigma$ from it.

  + Given $(theta, f) in op("AO")_alpha^* (G)$, we define the total order $prec_(theta, f)$ on $V$ where, for $i,j in V$, we have $i prec_(theta, f) j$ if:
    - $f(i) < f(j)$;
    - $f(i) = f(j)$ and there is a path from $i$ to $j$ traveling on the edges in the direction given by $theta$;
    - $i$ and $j$ are incomparable by the previous two criteria and $i < j$.

    We want to show that $prec_(theta, f)$ is indeed an order relation.
    To do this, we can restrict ourselves to considering vertices contained in the same subset $f^(-1) (h)$ for $1 <= h <= k$.
    Furthermore, any paths between vertices of $f^(-1) (h)$ along the orientation $theta$ passes entirely through vertices contained in $f^(-1) (h)$ since $f$ is increasing along $theta$, so we can restrict further to the case $alpha = (n)$ and forget about $f$.

    We write $i tilde(prec)_theta j$ if $i prec_theta j$ for the second criterion, i.e. if there is a path from $i$ to $j$ traveling on the edges in the direction given by $theta$.

    First, we prove a lemma: is $i < j < l$ or $i > j > l$ and $i tilde(prec)_theta l$, then $i tilde(prec)_theta j$ or $j tilde(prec)_theta l$.
    Let $i = p_0 -> p_1 -> dots -> p_k = l$ the oriented path between $i$ and $l$.
    Then there is $1 <= h < k$ such that $p_h < j < p_(h+1)$ or $p_(h+1) < j < p_h$.
    By the interval graph property, we have that ${p_h, j} in E$ or ${p_(h+1), j} in E$, without loss of generality assume the former.
    If $p_h -> j$, we get $i tilde(prec)_theta j$, otherwise $j tilde(prec)_theta l$.

    Now, given $i prec_theta j$ and $j prec_theta l$, let us consider some cases.
    - $i tilde(prec)_theta j$ and $j tilde(prec)_theta l$:
      then $i tilde(prec)_theta l$, so $i prec_theta l$.
    - $i$, $j$ are $tilde(prec)_theta$-incomparable, as are $j$, $l$:
      then $i < j < l$ and by the lemma also $i$ and $l$ are $tilde(prec)_theta$-incomparable, so $i prec_theta l$.
    - $i tilde(prec)_theta j$ and $j$, $l$ are $tilde(prec)_theta$-incomparable:
      then $j < l$.
      Let us split again some cases:
      - $l tilde(prec)_theta i$: then $l tilde(prec)_theta j$, absurd since $j$, $l$ are $tilde(prec)_theta$-incomparable.
      - $i tilde(prec)_theta l$: then $i prec_theta l$.
      - $i$, $l$ are $tilde(prec)_theta$-incomparable and $i < l$: then $i prec_theta l$.
      - $i$, $l$ are $tilde(prec)_theta$-incomparable and $i > l$: then, by the lemma $i prec_theta l$ or $l prec_theta j$, absurd since both couples are $prec_theta$-incomparable.
    - $i$, $j$ are $tilde(prec)_theta$-incomparable and $j prec_theta l$:
      this case can be solved similarly to the previous one.
    Thus we can conclude that $prec_theta$ is a total order on $V$.

    Next, we want to show that $prec_(f, theta)$ uniquely determines $(f, theta)$.
    Observe that, if $f(i) = h$, then
    $
      sum_(l=0)^(h-1) alpha_l <= \#lr(|{ j mid(|) j prec_theta i }|) < sum_(l=0)^h alpha_l med.
    $
    In this way, we know how to determine $f(i)$ for each $i$.
    Then note that, for ${i,j} in E$, we have $i -> j$ if and only if $i prec_theta j$, so we can also reconstruct $theta$.

  + We want to construct $Phi : cal(N)_(G, alpha) -> op("AO")_alpha^* (G)$ such that $i prec_(sigma) j$ if and only if $i prec_(Phi(sigma)) j$.

    Suppose we have constructed such a $Phi$ when $alpha = (n)$.
    Then, if $sigma = \( underbrace(sigma_1, alpha_1), dots, underbrace(sigma_(l(alpha)), alpha_(l(alpha))) \)$, set $Phi(sigma) = (theta, f)$ where
    - if $i in sigma_h$, then $f(i) = h$;
    - for ${i,j} in E$ with $f(i) <= f(j)$:
      - if $f(i) < f(j)$, then $theta(i,j) = i -> j$;
      - if $f(i) = f(j)$, then $theta(i,j) = Phi(sigma_(f(i))) (i,j)$.
    From this construction and the assumption in the case $alpha = (n)$, it is evident that $(theta, f) in op("AO")_alpha^* (G)$.
    It is also immediate to see that $i prec_(sigma) j$ if and only if $i prec_(Phi(sigma)) j$.

    Let us now turn to the case $alpha = (n)$, thus forgetting $f$.
    If ${i, j} in E$ and $sigma^(-1)(i) < sigma^(-1) (j)$, set $Phi(sigma) (i,j) = i -> j$.

    First, we show that $Phi$ is well defined.
    That $Phi(sigma)$ is an acyclic orientation is obvious.
    Let us now show that this orientation has a single source, namely $sigma(1)$.
    This is easily verified to be a source, so suppose absurdly that there is another source $sigma(i)$ with $i eq.not 1$.

    Suppose that $sigma(j) < sigma(i)$ for every $j in [i]$, then, since $sigma$ has no nontrivial left-to-right $G$-maxima, there exists $j in [i]$ such that ${sigma{j}, sigma{i}} in E$.
    It follows that $Phi(sigma)(sigma(j), sigma(i)) = sigma(j) -> sigma(i)$, absurd since $sigma(i)$ is a source.

    Therefore there exists $j in [i]$ such that $sigma(j) > sigma(i)$, let us choose the maximum $j$ that satisfies these conditions.
    Then $sigma(j) > sigma(i) >= sigma(j+1)$, but since $sigma$ has no $G$-descents, we have ${sigma(j), sigma(j+1)} in E$.
    If $i eq.not j+1$, by the interval graph property we have ${sigma(j+1), sigma(i)} in E$, otherwise we already knew that ${sigma(j), sigma(i)} in E$.
    In either case, there exists $j in [i]$ with ${sigma(j), sigma(i)} in E$, from which, as done a few lines above, we find the absurd that $Phi(sigma)(sigma(j), sigma(i)) = sigma(j) -> sigma(i)$.

    Finally, we only need to show that $i prec_(sigma) j$ if and only if $i prec_(Phi(sigma)) j$ for each $i,j$.
    Note that it is sufficient to show that $sigma(i) prec_(Phi(sigma)) sigma_(i+1)$ for each $i$.
    If ${sigma(i), sigma(i+1)} in E$, then $Phi(sigma) (sigma(i), sigma(i+1)) = sigma(i) -> sigma(i+1)$ and thus $sigma(i) prec_(Phi(sigma)) sigma_(i+1)$.
    Otherwise, given the absence of $G$-descents in $sigma$, we would have $sigma(i) < sigma(i+1)$.
    We also see that there is no path along $Phi(sigma)$ between $sigma(i)$ and $sigma(i+1)$, so again $sigma(i) prec_(Phi(sigma)) sigma_(i+1)$.

  + We want to construct $Psi : op("AO")_alpha^* (G) -> cal(N)_(G, alpha)$ such that $i prec_(f, theta) j$ if and only if $i prec_(Psi(f, theta)) j$.

    As done in the previous case, we can reduce to $alpha = (n)$.
    In fact, defined $Psi$ in this case, we can set
    $
      Psi(theta, f) = (Psi(theta|_(f^(-1) (1))) mid(|) dots mid(|) Psi(theta|_(f^(-1) (l(alpha))))) med.
    $
    From the assumptions on $Psi$ in the case $alpha = (n)$, it is immediately seen that $Psi(theta, f) in cal(N)_(G, alpha)$ and that $i prec_(theta, f) j$ if and only if $i prec_(Psi(theta, f)) j$.

    Let us then deal with the case $alpha = (n)$, forgetting $f$.
    We define
    $
      Psi(theta) = ([n] "sorted according to" prec_theta) med.
    $

    Once it is shown that $Psi$ is well defined, it is immediate to see that $i prec_theta j$ if and only if $i prec_(Psi(theta)) j$.
    Let then us show that $Psi(theta)$ has neither $G$-descents nor nontrivial left-to-right $G$-maxima.

    Suppose $sigma(i) > sigma(i+1)$.
    If $sigma(i)$ and $sigma(i+1)$ are $tilde(prec)_theta$-incomparable, then $sigma(i)$ and $sigma(i+1)$ are sorted by label in $prec_theta$ and so $sigma(i) < sigma(i+1)$, absurd.
    This means there is a path between $sigma(i)$ and $sigma(i+1)$, and if there were an intermediate vertex $sigma(j)$ in this path we would have that $sigma(i) prec_theta sigma(j) prec_theta sigma(i+1)$, therefore $i < j < i+1$, which is impossible.
    Since there are no intermediate vertices, there is an edge between $sigma(i)$ and $sigma(i+1)$, so at index $i$ there is no $G$-descent.

    Let now $i>1$ such that $sigma(j) < sigma(i)$ for every $j in [i]$.
    We know that $theta$ has only one source and it is the minimum in the order $prec_theta$, i.e. $sigma(1)$, in particular, $sigma(i)$ is not a source.
    Consequently, there is some $j$ with $sigma(j) -> sigma(i)$, then $sigma(j) prec_theta sigma(i)$ and so $j < i$.
    Briefly, we found some $j < i$ with ${sigma(j), sigma(i)} in E$, we can therefore conclude that $i$ is not a left-to-right $G$-maximum.
]

The weight-preserving bijection we have just constructed not only allows us to apply the results obtained from the theory of reverse $P$-partitions to the set $cal(N)_(G, alpha)$, for example, to prove @thm-cqsf-exp-psi-nga-1, but also to use the formulas found in the previous chapter to discover properties of acyclic orientations.

For example, in the previous chapter we noticed that, within $cal(N)_(G, (n))$, the first vertex of the permutation was evenly distributed.
Recalling that in our bijection the first vertex corresponds to the source, we can translate this result by establishing that the number of acyclic orientations where the only source is a fixed vertex does not depend on the choice of vertex.

Finally, here is an example of this bijection where $G$ is the interval graph with $h = [4,2,4,4]$ and $alpha = (4)$.
#example(name: [Bijection between $cal(N)_(G, (4))$ and $op("AO")_(4)^* (G)$ for $h = [4,2,4,4]$])[
  #align(center)[#table(
      columns: 6,
      stroke: none,
      align: horizon+center,
      column-gutter: 1em,
      table.hline(),
      table.header([$sigma$], [$theta$], [$op("inv")_G$], table.vline(stroke: .5pt), [$sigma$], [$theta$], [$op("inv")_G$]),
      table.hline(stroke: .5pt),
      [$(1, 2, 3, 4)$], [#drawings.ao-1((0, 1, 2, 3))], [$0$],
      [$(1, 2, 4, 3)$], [#drawings.ao-1((0, 1, 3, 2))], [$1$],
      [$(2, 1, 3, 4)$], [#drawings.ao-1((1, 0, 2, 3))], [$1$],
      [$(2, 1, 4, 3)$], [#drawings.ao-1((1, 0, 3, 2))], [$2$],
      [$(3, 1, 2, 4)$], [#drawings.ao-1((1, 2, 0, 3))], [$1$],
      [$(3, 4, 1, 2)$], [#drawings.ao-1((2, 3, 0, 1))], [$2$],
      [$(4, 1, 2, 3)$], [#drawings.ao-1((1, 2, 3, 0))], [$2$],
      [$(4, 3, 1, 2)$], [#drawings.ao-1((2, 3, 1, 0))], [$3$],
      table.hline(),
  )]
]<exm-nga-ao-bij>
