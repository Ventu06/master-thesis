#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Magic spanning forests

In this section, we introduce an additional structure that, similar to the previous ones, allows us to express the chromatic quasisymmetric functions in the $Psi$ basis.
We have called these structures #emph[magic spanning forests] and they represent a generalization of the #emph[increasing spanning forests] that are introduced in the next chapter, although we go on to consider different statistics about them.
The idea of considering these forests arose precisely from the increasing spanning forests, which on the one hand proved definitely useful in proving certain results (see next chapter), but on the other hand were insufficient to be related to the structures $cal(N)_(G, alpha)$ and $op("AO")_alpha^* (G)$.

#definition(name: [Magic spanning forest])[
  Let $G$ be an interval graph.
  An #emph[magic spanning forest] $F$ is an ordered collection of subgraphs $(T_1, dots T_k)$, with $T_h = (V_h, E_h)$, such that:
  - the vertices of $T_h$, as $h$ varies, form a partition of $V$, i.e., $V = union.big.sq_(1 <= h <= k) V_h$;
  - $T_h$ is a rooted tree for every $h$;
  - for every $h$, if crossing $T_h$ from the root to the leaves we meet orderly three vertices $i$, $j$ and $l$, with ${i,j} in E$ and ${i,l} in E$, then we have $j < l$.
  We call this third condition #emph[magic property].
]<def-msf>

For a magic spanning forest $F = (T_1, dots, T_k)$, we define the type of $F$ as
$
  alpha(F) = (\#lr(|T_1|), dots, \#lr(|T_k|)) med.
$
We denote by $op("MSF") (G)$ the set of magic spanning forests, and by $op("MSF")_alpha (G)$ the subset of forests of type $alpha$.

To represent a forest graphically, we draw the trees in order from left to right.
For each tree, we place the root at the top and, given a vertex, draw its children below it all on the same level, ordered by label from left to right.
Here is an example.
#example(name: [Magic spanning forest of type $(4,2,1)$ for $h = (7,3,3,7,6,7,7)$])[
  #grid(columns: (1fr, 1fr), align: horizon+center,
  [
    #drawings.msf-1
  ], [
    #drawings.interval-graph((7,3,3,7,6,7,7))
  ]
  )
]<exm-msf>

Given a magic spanning forest $F$ and two vertices $i$, $j$ belonging to the same tree, we say that $i$ lies above $j$ (or $j$ lies below $i$) if $j$ belongs to the subtree rooted in $i$.
Assuming that $i$ lies neither above nor below $j$, named $l$ the lowest common ancestor of $i$ and $j$, and named $l_i$ and $l_j$ the children of $l$ that stand above $i$ and $j$ respectively, we say that $i$ lies to the left of $j$ (or $j$ lies to the right of $i$) if $l_i < l_j$.
In other words, $i$ lies to the left of $j$ if $i$ lies in a subtree to the left of the subtree of $j$.

We now show that the magic property in @def-msf is equivalent to an apparently weaker condition, in which we require that $i$ be the parent of $j$.
#lemma(name: [Reduced magic property])[
  Let $G$ be an interval graph and let $F$ be an ordered set of trees whose vertices constitute a partition of $V$.
  Then $F$ is a magic spanning forest if and only if, for each tree and for each triplet of vertices $i$, $j$ and $l$, where $i$ is the parent of $j$ (thus ${i,j} in E$), $j$ lies above $l$ and ${i,l} in E$, we have $j < l$.
]<lmm-red-msf>
#proof[
  Clearly, if $F$ is a magic spanning forest the reduced property holds.
  Now suppose we have three vertices $i$, $j$, $l$, with $i$ above $j$, $j$ above $l$, ${i,j} in E$ and ${i,l} in E$.
  If $i$ is the parent of $j$, we can just apply the reduced property and conclude.
  Otherwise, suppose by contradiction $j > l$.
  Let $tilde(i)$ be the children of $i$ lying in the path between $i$ and $j$.
  Applying the reduced property to $i$, $tilde(i)$ and $l$ we obtain $tilde(i) < l$, so $tilde(i) < l < j$.
  Consider the path in the tree between $tilde(i)$ and $j$, that is $tilde(i) = p_1 -> dots -> p_k = j$.
  Then $p_h eq.not l$ for each $h$ and there is some $1 <= h < k$ such that $p_h < l < p_(h+1)$.
  Since $G$ is an interval graph and ${p_h, p_(h+1)} in E$, we have ${p_h, l} in E$, and so applying the reduced property to $p_h$, $p_(h+1)$ and $l$ we get $p_(h+1) < l$, absurd.
]

Let $F = (T_1, dots, T_k)$ be a magic spanning forest and $i$, $j$ two vertices with $i < j$ and ${i,j} in E$.
We say that ${i,j}$ is an inversion if one of the following holds:
- $i in T_r$, $j in T_s$ and $r > s$;
- $i$ and $j$ belong to the same tree, $i$ lies below $j$ or $i$ lies to the left of $j$.
We denote by $op("inv")_G (F)$ the total number of inversions, so
$
  op("inv")_G (F)
    &= sum_(1 <= r < s <= k) \#lr(|{ {i, j} in E mid(|) i in T_r, j in T_s, i > j }|) \
    &+ sum_(h=1)^k \#lr(|{ {i, j} in E mid(|) i, j in T_h, i "lies below or to the left of" j}|) med.
$

The following is the main result of this section.
#theorem()[
  Let $G$ be an interval graph, then there exists a bijection $Phi : op("MSF")_alpha (G) -> op("AO")_alpha^* (G)$ such that $op("inv")_G (F) = op("inv")_G (Phi(F))$.
]<thm-msf-ao-bij>

Before turning to the proof, let us look at some consequences.
The most important is the following reformulation of @thm-cqsf-exp-psi-ao-1.
#corollary()[
  Let $G$ be an interval graph with $n$ vertices, then
  $
    omega(chi_G^q (X;q))
    &= sum_(F in op("MSF") (G)) q^(op("inv")_G (F)) Psi_(alpha(F)) / z_(alpha(F)) \
    &= sum_(alpha tack.r.double n) Psi_alpha / z_alpha sum_(F in op("MSF")_alpha (G)) q^(op("inv")_G (F)) med.
  $
]<cor-cqsf-exp-psi-msf-1>

If $G$ is a unit interval graph, using the fact that $chi_G^q (X;q)$ is a symmetric function, we can rewrite the previous formula as
$
  omega(chi_G^q (X;q))
    &= sum_(F in op("MSF") (G)) q^(op("inv")_G (F)) binom(l(lambda(F)), m_1 (lambda(F)), dots, m_n (lambda(F)))^(-1) p_(lambda(F)) / z_(lambda(F)) \
    &= sum_(lambda tack.r n) p_lambda / z_lambda sum_(F in op("MSF")_lambda (G)) q^(op("inv")_G (F))
$
where $m_i (lambda)$ is the multiplicity of $i$ in the partition $lambda$.

Similar to what has been done between acyclic orientations and the set $cal(N)_(G, alpha)$, we can use this bijection to study one structure with the tools provided by the others.
Let us give an example.
In constructing the bijection we see how the sources are in correspondence with the roots of the trees.
Recalling that in $op("AO")_(n) (G)$ the sources were evenly distributed among the vertices, just as in $cal(N)_(G, (n))$ the first element of the permutation was, in our case the number of magic spanning forests consisting of a single tree with a certain fixed root does not depend on the choice of root.

Let us now prove @thm-msf-ao-bij.
#proof[
  The proof is very similar to that of @thm-nga-ao-bij, and it consists of the following steps.
  + Given $F in op("MSF")_alpha (G)$, we construct a partial order $tilde(prec)_F$ on $V$, which in turn uniquely determines $F$.
  + Given $(theta, f) in op("AO")_alpha^* (G)$, we retrieve the partial order $tilde(prec)_(theta, f)$ defined in the proof of @thm-nga-ao-bij.
  + We construct $Phi : op("MSF")_alpha (G) -> op("AO")_alpha^* (G)$ such that $i tilde(prec)_F j$ if and only if $i tilde(prec)_(Phi(F)) j$.
  + We construct $Psi : op("AO")_alpha^* (G) -> op("MSF")_alpha (G)$ such that $i tilde(prec)_(theta, f) j$ if and only if $i tilde(prec)_(Psi(theta, f)) j$.

  At this point, similarly to how we did in the other proof, we can show that $Phi$ and $Psi$ are bijections and they are inverse of each other, that ${i,j} in E$, with $i < j$, form an inversion if and only if $j tilde(prec)_F i$ and so that $Phi$ and $Psi$ are weight-preserving.

  Furthermore, we can use these facts to extend the partial order $tilde(prec)_F$ to a total order $prec_F$ using the already defined order $prec_(theta, f)$.
  In particular we have $i prec_F j$ if and only if $i tilde(prec)_F j$ or $i$, $j$ are $tilde(prec)_F$-incomparable and $i < j$.

  Let us therefore proceed with the proof.

  + Given $F in op("MSF")_alpha (G)$, we define the partial order $tilde(prec)_F$ on $V$ where, for $i, j in V$, we have $i prec_F j$ if
    - $i in T_r$, $j in T_s$ and $r < s$;
    - $i$ and $j$ belong to the same tree $T_h = (V_h, E_h)$ and $j$ can be reached from $i$ traversing the edges in $E|_(V_h)$ going only down and to the left.

    That the one defined is an order relation is easy to see: first we can restrict ourselves to considering vertices contained in the same tree, then just notice that the order is "monotonic" going down and to the left.

    We now show how to reconstruct the magic spanning forest $F$ starting from $tilde(prec)_F$.
    First, we can partition the vertices into the various trees in a similar way as done for $prec_(theta, f)$:
    if $i in T_h$, then
    $
      sum_(l=0)^(h-1) alpha_l <= \#lr(|{ j mid(|) j tilde(prec)_F i }|) < sum_(l=0)^h alpha_l
    $
    so we know in which tree $i$ lies based on the number of elements smaller than $i$.

    Let us now restrict ourselves to a tree, and identify the root, that is, the smaller of all the elements.
    At this point we recursively construct subtrees, starting each time from the leftmost vertex whose subtree we have not yet been constructed.
    Given a vertex $i$, we consider the smallest neighbor $j$ that has not yet been added to the tree and such that $i tilde(prec)_F j$.
    Then we add $j$ as a child of $i$ and repeat the process starting from $i$.
    When there are no more neighbors available, we backtrack to the parent and continue the algorithm from that vertex.

    Let us try to understand why this algorithm works.
    First, we note that each vertex is added, since each vertex is greater than or equal to the root and thus, if not added to the rest of the tree, is a child of the root.
    Consider now the step when we are visiting $i$ and adding a child vertex $j$.
    Since ${i,j} in E$ and $i prec_F j$, $j$ lies below or to the left of $i$.
    But if $j$ was to the left of $i$, it would already have been added to the tree, since the left subtree would already have been completely built,
    so $j$ lies indeed below $i$.
    Finally, if $j$ was not directly under $i$, we consider the child $l$ of $i$ in the path between $i$ and $j$.
    By the property of magic spanning forests we would have $l < j$, but this is absurd since $l$ has not yet been added (we have added all and only the vertices above and to the left of $i$, between which there is no $l$), so the algorithm should have chosen $l$ instead of $j$.

  +  Given $(theta, f) in op("AO")_alpha^* (G)$, remember that we defined tha partical order $tilde(prec)_theta$ in a way that, for $i, j in V$, we have $i prec_F j$ if $f(i) < f(j)$ or if $f(i) = f(j)$ and there is a path from $i$ to $j$ traveling on the edges in the direction given by $theta$.

  + We want to construct $Phi : op("MSF")_alpha (G) -> op("AO")_alpha^* (G)$ such that $i tilde(prec)_F j$ if and only if $i tilde(prec)_(Phi(F)) j$.

    Suppose we have constructed such a $Phi$ when $alpha = (n)$.
    Then, if $F = (T_1, dots, T_k)$, set $Phi(sigma) = (theta, f)$ where
    - if $i in T_h$, then $f(i) = h$;
    - for ${i,j} in E$ with $f(i) <= f(j)$:
      - if $f(i) < f(j)$, then $theta(i,j) = i -> j$;
      - if $f(i) = f(j)$, then $theta(i,j) = Phi(T_(f(i))) (i,j)$.
    From this construction and the assumption in the case $alpha = (n)$, it is evident that $(theta, f) in op("AO")_alpha^* (G)$.
    It is also immediate to see that $i tilde(prec)_F j$ if and only if $i tilde(prec)_(Phi(F)) j$.

    Let us now turn to the case $alpha = (n)$, in which $F$ consists of a single tree, thus forgetting $f$.
    For ${i, j} in E$, set $Phi(F)(i,j) = i -> j$ if $i tilde(prec)_F j$, that is, if $j$ is below or to the left of $i$.

    To show that $Phi$ is well-defined, let us check the followings.
    - $Phi(F)$ is defined for every ${i,j} in E$. \
      This is true because, for every ${i,j} in E$, one between $i$ and $j$ lies below or to the left of the other.
    - $Phi(F)$ is acyclic. \
      This follows from the fact that the arrows are oriented according to the order $tilde(prec)_F$;
    - $Phi(F)$ has only one source, i.e. the root of $F$. \
      It's easy to check that the root of $F$ is a source, while for any other vertex $j$ we can consider his parent $i$ and notice that $i -> j$, so $j$ is not a source.

    We finally show that $i tilde(prec)_F j$ if and only if $i tilde(prec)_(Phi(F)) j$.
    Since both relations $tilde(prec)_F$ and $tilde(prec)_(Phi(F))$ are path-based, thus generated by the relations on the edges, it suffices to check that they are equal on individual edges, that is, that the previous condition holds for ${i,j} in E$.
    This is equivalent to requiring that $j$ lies below or to the left of $i$ if and only if $i -> j$, which holds by definition of $Phi$.

  + We want to construct $Psi : op("AO")_alpha^* (G) -> op("MSF")_alpha (G)$ such that $i tilde(prec)_(f, theta) j$ if and only if $i tilde(prec)_(Psi(f, theta)) j$.

    As done in the previous case, we can reduce to $alpha = (n)$.
    In fact, defined $Psi$ in this case, we can set
    $
      Psi(theta, f) = (Psi(theta|_(f^(-1) (1))) mid(|) dots mid(|) Psi(theta|_(f^(-1) (l(alpha))))) med.
    $
    From the assumptions on $Psi$ in the case $alpha = (n)$, it is immediately seen that $Psi(theta, f) in op("MSF")_alpha (G)$ and that $i prec_(theta, f) j$ if and only if $i prec_(Psi(theta, f)) j$.

    Let us then deal with the case $alpha = (n)$, forgetting $f$.
    We define $Psi$ algorithmically, performing a depth-first search (DFS) on $(G, theta)$ starting from the single source.
    Here is an implementation of $Psi$ written in pseudocode.
    ```
      def Psi(G = (V,E), theta):

        def DFS(v):
          label v as discovered
          for w in sorted(V):
              if (theta(v,w) == v -> w) and (w is not discovered):
                  T.append(parent(w) = v)
                  DFS(w)

        T = []
        for v in V:
          label v as undiscovered
        DFS(source(G))
        return T
    ```
    This algorithm returns a list in which the parent of each vertex other than the root is marked.
    Briefly, it starts from a vertex and explores the graph as far as possible along each branch before backtracking, recording the path followed.
    Since there are only a finite number of vertices and each one is visited at most once, the algorithm ends.

    To show that $Psi$ is well-defined, let us check the followings.
    - $Psi(theta)$ is a tree, rooted in the only source of $theta$. \
      First, let us observe that every vertex belongs to $Psi(theta)$: if there was a vertex that has not been visited, choose a minimal vertex $j$ according to $tilde(prec)_theta$ that has not been visited.
      Then, since $j$ can not be the source, there is a vertex $i$ with $i -> j$, which at one point has been visited by the minimality condition.
      Hence, after visiting $i$, the algorithm should have visited $j$, absurd.
      In particular, we note that $T$ is connected.
      Then notice that $Psi(theta)$ is indeed a tree, as we add only one edge for each visited vertex.
    - $Psi(theta)$ satisfies the magic property. \
      Thanks to @lmm-red-msf, it is sufficient to check that $Psi(theta)$ satisfies the reduced magic property.
      Consider $i$, $j$, $l$ such that $i$ is the parent of $j$, $j$ is above $l$ and ${i,l} in E$.
      Then $j < l$, otherwise, after visiting $i$, the algorithm would have visited $l$ (which at that time had not yet been visited) instead of $j$ and $l$ would not have been below $j$.

    Finally, we want to prove that $i tilde(prec)_theta j$ if and only if $i tilde(prec)_(Psi(theta)) j$.
    As seen in the previous point, it is enough to check this condition for ${i,j} in E$.
    Since the vertices on every edge are comparable for both relations, it is enough to show one implication, so suppose $i -> j$.
    If, while performing to algorithm, $i$ gets visited before $j$, then $j$ lies below $i$.
    Instead, if $j$ is visited before $i$, then $i$ can't lie in the subtree rooted in $j$, otherwise there would be a cycle between $i$ and $j$.
    Hence $i$ lies to the right of $j$.
    In both cases, we find that $i tilde(prec)_(Psi(theta)) j$.
]

Let us now show an example by extending @exm-nga-ao-bij further, this time including magic spanning forests.
#example(name: [Correspondence between $cal(N)_(G, (4))$, $op("AO")_(4)^* (G)$ and $op("MSF")_(4) (G)$ for $h = [4,2,4,4]$])[
  #align(center)[#table(
      columns: 4,
      stroke: none,
      align: horizon+center,
      row-gutter: 2em,
      column-gutter: 1em,
      table.hline(),
      table.header([$sigma$], [$theta$], [$F$], [$op("inv")_G$]),
      table.hline(stroke: .5pt),
      [$(1, 2, 3, 4)$], [#drawings.ao-1((0, 1, 2, 3))], [#drawings.msf-2((-1,0,0,2))], [$0$],
      [$(1, 2, 4, 3)$], [#drawings.ao-1((0, 1, 3, 2))], [#drawings.msf-2((-1,0,0,0))], [$1$],
      [$(2, 1, 3, 4)$], [#drawings.ao-1((1, 0, 2, 3))], [#drawings.msf-2((1,-1,0,2))], [$1$],
      [$(2, 1, 4, 3)$], [#drawings.ao-1((1, 0, 3, 2))], [#drawings.msf-2((1,-1,0,0))], [$2$],
      [$(3, 1, 2, 4)$], [#drawings.ao-1((1, 2, 0, 3))], [#drawings.msf-2((2,0,-1,0))], [$1$],
      [$(3, 4, 1, 2)$], [#drawings.ao-1((2, 3, 0, 1))], [#drawings.msf-2((2,0,-1,2))], [$2$],
      [$(4, 1, 2, 3)$], [#drawings.ao-1((1, 2, 3, 0))], [#drawings.msf-2((3,0,0,-1))], [$2$],
      [$(4, 3, 1, 2)$], [#drawings.ao-1((2, 3, 1, 0))], [#drawings.msf-2((3,0,3,-1))], [$3$],
      table.hline(),
  )]
]<exm-nga-ao-msf-bij>

Unlike the proof of @thm-nga-ao-bij, in the proof of @thm-msf-ao-bij we never used a total order, but only a partial order.
The choice to extend $tilde(prec)_F$ to a total order using the order on vertex labels comes from the total order placed on $tilde(prec)_(theta, f)$, which in turn comes from the definition of $cal(N)_(G, alpha)$.
However, in the case of magic spanning forests, there is another extension of order that would seem more natural: we could place $i overline(prec)_F j$ if $j$ lies below or to the left of $i$.
Through the bijection just defined, this would give us another total order on $op("AO")_alpha^* (G)$, again consistent with orientations, and a subset of $frak(S)_n$ different from $cal(N)_(G, alpha)$, possibly characterizable in a different way and with other properties.
In this discussion we do not study this new order, nor other total orders obtained as extensions of $tilde(prec)_F$.

Finally, let us see a couple of ideas regarding how to use these structures to address the $e$-positivity problem.
Given the equivalence of these structures, we choose to use magic spanning forests for the following statements.

For a unit interval graph $G$, we can expand the powersum basis into the elementary basis as described in @tab-sym-fun-bases-coeff, and starting from the identity of @cor-cqsf-exp-psi-msf-1 obtain the following:
$
  chi_G^q (X;q)
    &= sum_(F in op("MSF") (G)) q^(op("inv")_G (F)) (-1)^(n-l(lambda(F))) binom(l(lambda(F)), m_1 (lambda(F)), dots, m_n (lambda(F)))^(-1) p_(lambda(F)) / z_(lambda(F)) \
    &= sum_(F in op("MSF") (G)) q^(op("inv")_G (F)) (-1)^(n-l(lambda(F))) / (l(lambda(F))! product_(i=1)^n i^(m_i (lambda(F)))) sum_(lambda tack.r n) (-1)^(n - l(lambda)) w(B_(lambda, lambda(F))) e_lambda \
    &= sum_(lambda tack.r n) e_lambda sum_(F in op("MSF") (G)) q^(op("inv")_G (F)) (-1)^(l(lambda)-l(lambda(F))) w(B_(lambda, lambda(F))) / (l(lambda(F))! product_(i=1)^n i^(m_i (lambda(F)))) med, \
  \
  \
  chi_G^q (X;q)
    &= sum_(mu tack.r n) (-1)^(n - l(mu)) p_mu / z_mu sum_(F in op("MSF")_mu (G)) q^(op("inv")_G (F)) \
    &= sum_(mu tack.r n) (-1)^(n - l (mu)) / z_mu sum_(lambda tack.r n) (-1)^(n - l(lambda)) w(B_(lambda, mu)) e_lambda sum_(F in op("MSF")_mu (G)) q^(op("inv")_G (F)) \
    &= sum_(lambda tack.r n) e_lambda sum_(mu tack.r n) (-1)^(l(lambda) - l (mu)) w(B_(lambda, mu)) / z_mu sum_(F in op("MSF")_mu (G)) q^(op("inv")_G (F)) med.
$

At this point, following a standard combinatorics approach, it would be desirable to construct a "weighted involution" on the magic spanning forests that preserves the number of $G$-inversions and changes the parity of the number of trees.
Unfortunately, the above formula (especially the first one) is particularly complicated, partly because of the denominators, and makes this approach impractical: one cannot just eliminate pairs of terms with the opposite sign but would have to divide each term into several parts and have these parts cancel each other.
For these reasons, we could not find any pattern by which to make terms cancel each other out.
However, we think that, for such an approach, expression through magic spanning forests is preferable to other structures.
In fact, to go from one forest to another, we can break or join trees with a certain degree of freedom and conditions to be verified that are quite clear.

An additional idea would be to look for a pattern in the remaining terms after simplifying the expression.
To do this, it is useful for us to consider @thm-cqsf-e-coeff-ao, by which the Shareshian-Wachs conjecture is equivalent to finding a function $phi : op("AO")(G) -> {lambda tack.r n}$ such that $l(phi(theta))$ equals the number of sources of theta and $chi_G^q (X;q) = sum_(theta in op("AO")(G)) q^(op("inv")_G (theta)) e_(phi(theta))$.
We choose a $f_theta in cal(O)^* (theta)$ for each $theta in op("AO")(G)$ and define $op("MSF")_f (G) = { Psi(theta, f_theta) mid(|) theta in op("AO")(G) }$.
At this point, using the bijection between $op("AO")^* (G)$ and $op("MSF") (G)$, the previous condition is equivalent to finding a function $tilde(phi) : op("MSF")_f (G) -> {lambda tack.r n}$ such that $l(tilde(phi)(F)) = l(lambda(F))$ and $chi_G^q (X;q) = sum_(F in op("MSF")_f (G)) q^(op("inv")_G (F)) e_(tilde(phi)(F))$.
A natural choice for $tilde(phi)(F)$ is clearly $lambda(F)$, so we can formulate the following problem, which implies the Shareshian-Wachs conjecture.
#problem()[
  For each $theta in op("AO")(G)$, find $f_theta in cal(O)^* (theta)$ so that
  $
    chi_G^q (X;q) = sum_(F in op("MSF")_f (G)) q^(op("inv")_G (F)) e_(lambda(F)) med.
  $
]<prb-cqsf-e-coeff-msf>

For example, an idea for constructing $f_theta$ might be as follows: we choose an order for the sources of $theta$; we now iterate source by source, considering all vertices reachable from it by paths along $theta$ that have not been reached before by starting from previous sources, and put all these vertices in the same component.
As expected, this choice for $f_theta$ does not seem to solve the problem posed above: by sorting the sources by label the identity holds for most unit interval graphs with a limited number of vertices, but not for all of them; by appropriately changing permutations we were able to obtain the identity for other graphs as well, however, we do not expect this relation continuing to hold, mainly because of the large number of arbitrary choices made.

However @prb-cqsf-e-coeff-msf already seems to be more reasonable, and the particular case just described might give some clue about both the choice of $f_theta$ and thus the pattern of magic spanning forests associated with the expansion in the elementary basis.
