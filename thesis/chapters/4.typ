#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

#set heading(supplement: [chapter])
= Constructive structures and #box([$Psi$-positivity])

In the case of chromatic symmetric functions we have seen some expansions in the powersum basis, in particular, we had also obtained the $p$-positivity (modulo the $omega$ involution, see @thm-csf-exp-p-1, @thm-csf-exp-p-2 and @omega-csf-p-pos).
In the quasisymmetric case the $p$-positivity, or possibly the $Psi$-positivity, continues to hold, however, the identities shown in the preceding chapters do not lend themselves to a generalization.

In this chapter, we look at three strongly related ways of expressing chromatic quasisymmetric functions in the powersum basis.

The first, discussed in the first section, uses subsets of permutations called $cal(N)_(G, alpha)$ to obtain an expansion in the symmetric powersum basis for unit interval graphs.
We also enunciate a formula for interval graphs, analogous to the previous one but in the quasisymmetric powersum basis.

The second way uses acyclic orientations and holds in the most complete generality, that is, for any finite graph.
We specialize this result to the cases of interval graphs and unit interval graphs, after which we find a bijection with the structure $cal(N)_(G, alpha)$ seen in the previous section.
This allow us to transfer results between the two structures, in particular, we prove the formula stated for $cal(N)_(G, alpha)$ in the case of interval graphs.

The third way uses a new structure, the so-called magic spanning forests, to express the chromatic quasisymmetric function in the quasisymmetric powersum basis in the case of interval graphs.
The idea for the construction of these forests comes from the increasing spanning forests that we see in the next chapter, while to prove the identities we construct a bijection with the acyclic orientations, similarly to what we did for $cal(N)_(G, alpha)$.

#include "4-1.typ"
#include "4-2.typ"
#include "4-3.typ"
