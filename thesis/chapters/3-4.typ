#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== LLT polynomials

LLT polynomials, introduced by Lascoux, Leclerc and Thibon in @lascoux_ribbon_1997 (hence the name), are a large family of symmetric functions and can be seen as a $q$-analogue of skew Schur functions.
These are fundamental in the study of modified Macdonald polynomials (see @haglund_combinatorial_2005), and they also have relations with generalized Kostka coefficients and Kazhdan-Lusztig polynomials (see @leclerc_littlewoodrichardson_2000).
Finally, they have been used for the study of diagonal harmonics (see @blasiak_shuffle_2023).

As far as our interests are concerned, there is a particular subfamily, the unicellular LLT polynomials, which admits an interpretation based on unit interval graphs and satisfies a strong relation, a kind of duality, with chromatic quasisymmetric functions.
This relation has been shown by Carlsson and Mellit in @carlsson_proof_2018[Proposition 3.4], and has allowed the enunciation of results, problems and conjectures equivalently between chromatic quasisymmetric functions and LLT polynomials.
For example, the positivity in the elementary basis of unicellular shifted LLT polynomials, conjectured in @alexandersson_llt_2021[Conjecture 10] and later proved independently in @dadderio_e-positivity_2020 and @alexandersson_combinatorial_2022, led to a positive expansion of the chromatic quasisymmetric function in a variant of the Hall-Littlewood basis.
The latter expansion was then reformulated and proved without the aid of LLT polynomials in @abreu_symmetric_2021, as we shall see more fully in two chapters.

Recently, D'Adderio in @dadderio_chromatic_2023[Theorem 10.1] proved a generalization of the Carlsson-Mellit identity to interval graphs, using an extended definition of LLT polynomials and some combinatoric techniques inspired by Abreu and Nigro's article.
This opens up new questions.
On the one hand, how does this new definition of LLT polynomials relate to the original one?
On the other, what other valid results on unit interval graphs can be generalized to interval graphs?
This last question has been central to this thesis work.
In the next chapter and the following one, we go on to see various results that hold in the generality of interval graphs.

We now proceed by introducing the LLT polynomials in their full generality.
This is not necessary to understand the rest of the section and subsequent chapters, so the hurried reader can skip directly to @def-graph-llt-poly, where our particular case is addressed (and in that case, $op("C")(G)$ are all the colorings of a $G$, even those that are not proper).
However, many of the results on LLT polynomials that can be reused in the case of chromatic quasisymmetric functions hold in a generality greater than the unicellular case, which is why we have chosen to provide a more complete introduction.

Consider a $k$-tuple of skew shapes $nu = (nu_1, dots, nu_k) = (lambda_1 backslash mu_1, dots, lambda_k backslash mu_k)$.
We define the set of semi-standard Young tableaux of shape $nu$ as
$
  op("SSYT")(nu) = op("SSYT")(nu_1) times dots times op("SSYT")(nu_k) med.
$
Given now $T = (T_1, dots, T_k) in op("SSYT")(nu)$, let $x_T = x_(T_1) x_(T_2) dots x_(T_k)$, where $x_(T_i)$ is usually the monomial given by the product of the variables associated with the entries of $T_i$.
Given also a cell $u$ of $T_i$ of coordinates $(r,c)$, we define its content $op("cont")(u) = c-r$.
Given two cells $u$ of $T_i$ and $v$ of $T_j$, we say that they form an inversion if $T_i (u) > T_j (v)$ and one of the following holds
- $i < j$ and $op("cont")(u) = op("cont")(v)$;
- $i > j$ and $op("cont")(u) = op("cont")(v)+1$.
We denote by $op("inv")_nu (T)$ the number of inversions.
At this point, we can define LLT polynomials.
#definition(name: [LLT polynomials])[
  Given a $k$-tuple of skew shapes $nu$, we define the #emph[LLT polynomial] associated with $nu$ as
  $
    "LLT"_nu (X;q) = sum_(T in op("SSYT")(nu)) q^(op("inv")_nu (T)) x_T med.
  $
]<def-llt-poly>

We graphically visualize a semi-standard Young tableau $T$ of shape $nu$ in the following way: we draw the various tableaux $T_1, T_2, dots, T_k$ in a grid going left-to-right and bottom-to-top, so that cells with the same content are aligned on the same diagonal.
Look at @fig-ssyt-content for an example with $nu = ((3,2) backslash (1), (3,1), (3,3) backslash (2,1))$.
In this example, inside the cells are represented not the indices of the variables, but rather the #emph[reading order]: we sort the cells first descending along the contents, then from right to left.
#grid(columns: (1fr, 1fr), align: center+bottom, [
    #figure(caption: [
      Content of SSYT of shape \ $((3,2) backslash (1), (3,1), (3,3) backslash (2,1))$
    ])[
      #drawings.ssyt-content
    ]<fig-ssyt-content>
  ], [
    #figure(caption: [
      The graph associated with SSYT of shape \ $((3,2) backslash (1), (3,1), (3,3) backslash (2,1))$
    ])[
      #drawings.interval-graph((3,4,5,6,7,8,9,9,10,11,11))
    ]<fig-graph-ssyt>
  ]
)

Let us now consider a $k$-tuple of skew-shapes $nu$ and set $n = \#lr(|nu|) = \#lr(|nu_1|) + dots + \#lr(|nu_k|)$.
We construct the following associated graph of $nu$: as vertices we take $[n]$ and put an edge between vertices $i$ and $j$ if, given $u$ and $v$ the cells in the representation of $nu$ with index in the reading order $i$ and $j$ respectively, one of the following conditions holds:
- $op("cont")(u) = op("cont")(v)$;
- $op("cont")(u) = op("cont")(v)+1$ and $u$ appears to the right of $v$;
- $op("cont")(u) = op("cont")(v)-1$ and $u$ appears to the left of $v$.

For example, the graph associated with $nu = ((3, 2) backslash (1), (3, 1), (3, 3) backslash (2, 1))$ is the unit interval graph depicted in @fig-graph-ssyt.

It can be verified that the graph $G$ associated with $nu$ is always a unit interval graph, later we give more details.
At this point, to each $T in op("SSYT")(nu)$ we can associate a coloring $kappa(T)$ of $G$, not necessarily proper, made by assigning to the vertex with label $i$ the index present in the cell at the $i$-th place in the reading order.
It can be verified directly that $x_T = x_(kappa(T))$ and $op("inv")_nu (T) = op("coinv")_G (kappa(T))$, so we get the following formula
$
  "LLT"_nu (X;q)
    &= sum_(T in op("SSYT")(nu)) q^(op("coinv")_G (kappa(T))) x_(kappa(T)) \
    &= sum_(kappa in kappa(op("SSYT")(nu))) q^(op("coinv")_G (kappa)) x_kappa med.
$

This formula differs from the chromatic quasisymmetric function in the set of colorings over which we are summing.
In the second case it is the proper colorings $op("PC")(G)$, while in the first, called $op("C")(G)$ the set of all colorings of $G$, some subset of $op("C")(G)$.

Suppose now that $nu = (nu_1, dots, nu_n)$ is unicellular, that is, each $nu_i$ has only one cell.
We note the similarities between this case and the part listings defined in @def-part-lis with parts consisting of only single vertices: just match the contents of cells with the level of vertices and read the vertices from right to left instead of left to right.
Moreover, this correspondence leads to an equality between the graph associated with $nu$ and the incomparability graph of the poset of the related part listing.
Because of this, we can follow the proofs of @prop-part-lis-31-free and @prop-part-lis-vert-31-22-free to show that the graph associated with $nu$ is a unit interval graph, even in the general case where $nu$ is not necessarily unicellular.
Similarly, following the proof of @prop-31-22-free-part-lis-vert, we find that for every unit interval graph $G$ it is possible to find an $n$-tuple of skew shapes $nu$ that is unicellular and such that the associated graph is just $G$.

Also in this case no condition is imposed on the values in the cells of Young's semi-standard tablueaux of form $nu$, so
$
  "LLT"_nu (X;q) = sum_(kappa in op("C")(G)) q^(op("coinv")_G (kappa)) x_kappa med.
$
From the last formula, it is immediate that the associated LLT polynomial does not depend on the choice of $nu$ among the $n$-tuples of skew-shapes inducing the graph $G$, so we can define the LLT polynomial of $G$ as follows.
#definition(name: [LLT polynomial for graphs])[
Given a graph $G$, we define its LLT polynomial as
$
  "LLT"_G (X;q) = sum_(kappa in op("C")(G)) q^(op("coinv")_G (kappa)) x_kappa med.
$
]<def-graph-llt-poly>
Note that this definition does not require $G$ to be a unit interval graph, and in fact, we use this definition in the more general case of interval graphs.

The main property of LLT polynomials for a tuple of skew-shapes, and therefore also for a unit interval graph, lies in the fact that they are symmetric functions (see @haglund_combinatorial_2005[Theorem 3.3 and Section 10]).

Let us now turn to the fact that interests us most.
#theorem(name: [Carlsson-Mellit identity (@carlsson_proof_2018[Proposition 3.4])])[
  Let $G$ be a unit interval graph, then the following plethystic relation holds:
  $
    omega(chi_G^q [X]) = ("LLT"_G [X(1-q)]) / (1-q)^n med.
  $
]<thm-cqsf-rel-llt>
As anticipated, D'Adderio et al. in @dadderio_chromatic_2023[Theorem 10.1] generalized this fact to interval graphs.
The statement is virtually identical so we do not rewrite it, but this time the definition of plethysm is more complicated, see @def-plt-qsym.

Furthermore, we can rewrite the previous identity as
$
  chi_G^q [X] = ("LLT"_G [X(q-1)]) / (q-1)^n med.
$

A first consequence of this identity is the correspondence in linear relations between chromatic quasisymmetric functions and LLT polynomials, such as the modular law (see @def-mod-law-ig).

We already mentioned at the beginning of the section how different expansions can be matched.
More generally, the above formula gives us a correspondence between $chi_G^q (X;q)$ and $"LLT"_G (X;q+1)$, and some of the corresponding properties are given in the table below (see also @alexandersson_llt_2018[Table 1]).

#figure(caption: [Property correspondence between $chi_G^q$ and $"LLT"_G$])[#table(
  columns: 3,
  stroke: none,
  align: (left, center, center),
  column-gutter: 1em,
  table.hline(),
  table.header([Property],[$chi_G^q (X;q)$],[$"LLT"_G (X;q+1)$]),
  table.hline(stroke: .5pt),
  [Schur positive],[Yes],[Yes],
  [$p$-positive under $omega$],[Yes],[Yes],
  [$e$-coefficients],[Acyclic orientations],[$q$-acyclic orientations],
  [Fixed length $e$-coefficients],[Number of sinks],[Numer of half-sinks],
  [Positive expansion in], [$rho_lambda$ basis], [$e_lambda$ basis],
  table.hline(),
)]
