#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Introduction to quasisymmetric functions

Quasisymmetric functions are a generalization of the symmetric functions introduced by Gessel in @gessel_multipartite_1984.
They have many similarities with their symmetric counterpart: they form a graded algebra, they have various bases indexed by compositions, a variant of plethysm can be defined on them, and a Hopf algebra structure can also be defined.
Unlike symmetric functions, quasisymmetric functions have weaker ties to other branches of mathematics, such as representation theory, and are less widely used even within combinatorics.
For this reason, the theory concerning them is still an open field of research, so much so that in recent times new bases have been introduced and new identities found among them.
As a main reference, we recommend @stanley_enumerative_1999[Section 7.19].

We begin by defining quasisymmetric functions.
As done in the case of symmetric functions, they can be defined as limits of polynomials in finite variables that satisfy certain properties.
This time, however, we choose to give a less constructive and more operational definition.
#definition(name: [Quasisymmetric functions])[
  The #emph[quasisymmetric functions] on a domain $F$ are the formal series in infinite variables of finite degree invariant by shifts of variables.
  In other words, $f in F[[x_1, x_2, dots]]$ is a quasisymmetric function if it has finite degree and for each sequence of positive integers $alpha_1, alpha_2, dots, alpha_k$ and for each pair of index sequences $i_1 < i_2 < dots < i_k$ and $j_1 < j_2 < dots < j_k$ we have
  $
    [x_(i_1)^(alpha_1) x_(i_2)^(alpha_2) dots x_(i_k)^(alpha_k)] f
    = [x_(j_1)^(alpha_1) x_(j_2)^(alpha_2) dots x_(j_k)^(alpha_k)] f med.
  $
  We denote by $"QSym"[X]$ the set of quasisymmetric functions.
]<def-qsym-fun>
Clearly $"QSym"[X]$ is closed by sum, and it is not difficult to observe that it is also closed by product.
We can therefore consider a graded $F$-algebra structure on $"QSym"[X]$.
Note also that every symmetric function is quasisymmetric, but opposite containment does not hold.

Let us now introduce the concept of composition, similar to that of partition, and then see some examples.
#definition(name: [Composition])[
  Given $n in NN$, a #emph[composition] $alpha$ of $n$, briefly $alpha tack.r.double n$, is a sequence of positive integers $(alpha_1, alpha_2, dots, alpha_k)$ such that $sum_(i=1)^k alpha_i = n$.
]<def-comp>
Given a composition $alpha = (alpha_1, dots, alpha_k)$, we denote by $\#lr(|alpha|)$ the size of $alpha$, i.e. $n$, and by $l(alpha)$ the length of $alpha$, i.e. $k$.
We note that by rearranging the parts of a composition $alpha$ we get a certain partition $lambda$: in this case, we write $alpha tilde lambda$.

We can associate with a composition $alpha tack.r.double n$ a subset $S_alpha$ of $[n-1]$ by letting
$
  S_alpha = {alpha_1, alpha_1 + alpha_2, dots, alpha_1 + dots + alpha_(l(alpha)-1)} med.
$
Conversely, to $S subset [n-1]$ with $S = {s_1, s_2, dots, s_k}$ (with $s_i < s_(i+1)$ for $i in [k-1]$) we can associate the composition
$
  alpha(S) = (s_1, s_2 - s_1, dots, s_k - s_(k-1), n - s_k) med.
$
We immediately see that the two maps are the inverse of each other and thus there is a bijection between the compositions of $n$ and the subsets of $[n-1]$.
In particular, we see that there are $2^(n-1)$ compositions of $n$.
This also allows us to put the #emph[refinement] order relation on compositions, where, for each $alpha, beta tack.r.double n$, we have $alpha prec beta$ if $S_beta subset S_alpha$.

We now turn to a couple of classes of quasisymmetric functions indexed on the compositions.
#definition(name: [Monomial quasisymmetric functions])[
  Given a composition $alpha$, we define the #emph[monomial quasisymmetric function] $M_alpha$ as
  $
    M_alpha = sum_(i_1 < i_2 dots < i_(l(alpha))) x_(i_1)^(alpha_1) x_(i_2)^(alpha_2) dots x_(i_(l(alpha)))^(alpha_(l(alpha))) med.
  $
]<def-qsym-fun-m>
For example
$
  M_(1,2,1) = x_1 x_2^2 x_3 + x_1 x_2^2 x_4 + x_1 x_3^2 x_4 + x_2 x_3^2 x_4 + dots med.
$

Notice that for each partition $lambda$ we have
$
  m_lambda = sum_(alpha tilde lambda) M_alpha med.
$
In this case we say that ${M_alpha}_(alpha tack.r.double n)$ refines ${m_lambda}_(lambda tack.r n)$.

Note also that for each $f in "QSym"[X]$ we have
$
  f = sum_(alpha "composition") ([x_1^(alpha_1) dots x_(l(alpha))^(alpha_(l(alpha)))] f) M_alpha med.
$
This last expression shows that monomial quasisymmetric functions are a basis of $"QSym"[X]$ as a vector space.

#definition(name: [Fundamental quasisymmetric functions])[
  Given a composition $alpha$, we define the #emph[fundamental quasisymmetric function] $F_alpha$ as
  $
    F_alpha = sum_(i_1 <= i_2 <= dots <= i_(\#lr(|alpha|)) \ j in S_alpha => i_j < i_(j+1)) x_(i_1) x_(i_2) dots x_(i_(\#lr(|alpha|))) med.
  $
]<def-qsym-fun-f>
From the definition of the fundamental quasisymmetric functions and of the refinement ordering, it follows that
$
  F_alpha = sum_(beta prec.eq alpha) M_beta med.
$
By exploiting the structure of the poset $(cal(P)([n-1]), subset.eq)$ one is able to invert the previous formula, for example by using a Möbius inversion, and obtain the following identity
$
  M_alpha = sum_(beta prec.eq alpha) (-1)^(l(beta) - l(alpha)) F_beta med.
$
In particular, we find that also the fundamental quasisymmetric functions are a basis of $"QSym"[X]$ as a vector space.

We introduce a final class of functions, the quasisymmetric powersum functions $Psi_alpha$.
These first appeared in the recent article @ballantine_quasisymmetric_2020 with the purpose of refining the powersum basis of symmetric functions.
Before stating the definition, let us look at some more general facts.

In addition to quasisymmetric functions, another generalization of symmetric functions is the non-commutative symmetric functions, which form the algebra $"NSym"[X]$.
For some symmetric functions, it is convenient to define non-commutative analogs, for example, the elementary non-commutative symmetric functions $bold(e)_alpha$ and the complete non-commutative symmetric functions $bold(h)_alpha$, see @gelfand_noncommutative_1995 for further details.
In the same paper, a class of functions analogous to powersum symmetric functions was defined, the powersum non-commutative symmetric functions $bold(Psi)_alpha$.

There is a special relation between $"QSym"[X]$ and $"NSym"[X]$, in fact, they are the dual of each other.
In more detail, on $"Sym"[X]$ there exists a particular bilinear form, the Hall inner product $angle.l dot, dot angle.r$ defined by $angle.l m_lambda, h_mu angle.r = delta_(lambda, mu)$, which makes $"Sym"[X]$ the dual of itself.
This extends to a coupling between $"QSym"[X]$ and $"NSym"[X]$, defined by $angle.l M_alpha, bold(h)_beta angle.r = delta_(alpha, beta)$.

It is therefore natural to try to use such a bilinear form to carry functions of $"NSym"[X]$ to $"QSym"[X]$ (or vice versa).
From the theory, it is known that $angle.l p_lambda, p_mu angle.r = delta_(lambda, mu) z_lambda$, so we propose the following generalization.
#definition(name: [Powersum quasisymmetric functions])[
  Given a composition $alpha$, we define the #emph[powersum quasisymmetric function] $Psi_alpha$ as the unique basis such that
  $
    angle.l Psi_alpha, bold(Psi)_beta angle.r = delta_(alpha, beta) z_alpha med.
  $
]<def-qsym-fun-psi>
To write more explicitly the functions $Psi_alpha$ there are some formulas that relate them to the bases $M_alpha$ and $F_alpha$, see @tab-qsym-fun-bases-coeff.
However, there is no more immediate expression like the ones for the other two bases.
Furthermore, the basis ${Psi_alpha}$ refines the basis ${p_lambda}$, meaning that
$
  p_lambda = sum_(alpha tilde lambda) Psi_alpha med.
$

We would like to point out the generality of the method by which we obtained this last basis: the duality with the non-commutative functions makes it possible to study one with the tools of the other, and we do not exclude that it may lead to the definition of interesting new bases or the discovery of new identities.

In addition to the bases mentioned so far, there are others in the literature.
For example, again in the article @ballantine_quasisymmetric_2020, a second refinement of powersum functions, the $Phi_alpha$ basis, is constructed.
Then there exists refinements of the Schur basis: quasisymmetric Schur functions, defined in @haglund_quasisymmetric_2011, and a second version, called Young quasisymmetric Schur functions, defined in @luoto_quasisymmetric_2013.
These other bases are not discussed in this thesis.

In the section on symmetric functions, we introduced the $omega$ involution.
Although there is no unambiguous way to extend it to quasisymmetric functions, the most reasonable choice is to place
$
  omega(F_alpha) = F_alpha([n-1] backslash (n - S(alpha)))
$
where $(n - S)$ is the set ${n - s mid(|) s in S}$.
In this way we also get
$
  omega(Psi_alpha) = (-1)^(\#lr(|alpha|) - l(alpha)) Psi_(alpha(n - S(alpha)))
$
as proved in @ballantine_quasisymmetric_2020[Theorem 4.6].
This is very similar to what happens in the symmetric case, that is, $omega(p_lambda) = (-1)^(\#lr(|lambda|) - l(lambda)) p_lambda$.

Let us see how it is possible to change from one basis to another.
In @tab-qsym-fun-bases-coeff we report the coefficients in the expansion
$
  V_alpha = sum_(beta tack.r.double \#lr(|alpha|)) c_(alpha, beta) U_beta
$
for different choices of bases ${V_alpha}$ and ${U_beta}$.
Although the table is not complete, it is still sufficient to go from any of the three bases to any other.
In addition to the references mentioned so far, we also indicate @alexandersson_p-partitions_2021[Theorem 1.1] for the formulas mentioned below.

#figure(caption: [Bases change coefficients for quasisymmetric functions])[#text(size: 10pt)[#table(
  columns: 4,
  stroke: none,
  align: horizon+center,
  inset: 7.5pt,
  table.hline(),
  table.header([], table.vline(stroke: .5pt), [$F_alpha$], [$Psi_alpha$], [$M_alpha$]),
  table.hline(stroke: .5pt),
  [$F_beta$], [$I$], [$$], [$delta_(beta prec.eq alpha) (-1)^(l(beta) - l(alpha))$],
  [$Psi_beta$], [$delta_(S_alpha in op("Uni")(beta)) (-1)^(\#lr(|S_alpha backslash S_beta|)) / z_beta$], [$I$], [$$],
  [$M_beta$], [$delta_(beta prec.eq alpha)$], [$delta_(beta succ.eq alpha) z_alpha / (pi(alpha, beta))$], [$I$],
  table.hline(),
)]]<tab-qsym-fun-bases-coeff>

Let us extend the poset in @fig-sym-fun-pos-exp by including quasisymmetric functions.
Recall that ${U_alpha} succ {V_alpha}$ if the elements of ${U_alpha}$ expand positively in the basis ${V_alpha}$.
The dashed lines indicate that the coefficients of the expansion are not necessarily integers, while dotted lines indicate that a basis of symmetric functions expands positively into a basis of quasisymmetric functions.

#figure(caption: [Positive expansion poset for quasisymmetric functions])[
  #drawings.qsym-fun-pos-exp
]<fig-qsym-fun-pos-exp>

At this point, let us treat plethysm in the quasisymmetric case.
In defining this operation on symmetric functions we kept well in mind the idea of substituting the monomials of one function into the variables of the other.
Part of the success of this idea is due to the fact that symmetric functions are invariant by permutation of variables.
This is no longer true in the case of quasisymmetric functions, in particular by substituting monomials in place of variables in different orders we cannot expect to get the same result.
In @ballantine_quasisymmetric_2020 this problem is solved by ordering the monomials to be substituted.
Let us see some more details about this approach.

We introduce the concept of #emph[combinatorial alphabet], that is, a quadruple $cal(A) = (A, op("sgn"), op("wt"), prec)$ where $A$ is a set of letters, $op("sgn") : A -> {+1, -1}$ is a function that specifies a sign for each letter, $op("wt") : A -> Z$ is a function that assigns a weight, that is, a monic monomial to each letter, and finally $prec$ is a total order on $A$.
We then define $op("wt")(cal(A)) = sum_(a in A) op("sgn")(a) op("wt")(a)$, where the various terms do not commute with each other and the sum is performed following the ordering given by $prec$.
Intuitively, we introduced this structure to define the plethysm $f[sum_(a in A) op("sgn")(a) op("wt")(a)] = f(op("wt")(cal(A)))$.
Let us quickly see how to do it.

#definition(name: [Plethysm (for quasisymmetric functions)])[
  Given a combinatorial alphabet $cal(A) = (A, op("sgn"), op("wt"), prec)$, let $A^+ = {a in A mid(|) op("sgn")(a) = +1}$ and $A^- = {a in A mid(|) op("sgn")(a) = -1}$.
  For each $alpha tack.r.double n$, we define $W(alpha, cal(A))$ to be the set of words $w = w_1 w_2 dots w_(\#lr(|alpha|))$ such that:
  - $w_i in A$ for each $i$;
  - $w_1 prec.eq w_2 prec.eq dots prec.eq w_(\#lr(|alpha|))$;
  - for every $i in [\#lr(|alpha|)-1]$ if $w_i = w_(i+1)$ and $w_i in A^+$, then $i in.not S_alpha$;
  - for each $i in [\#lr(|alpha|)-1]$ if $w_i = w_(i+1)$ and $w_i in A^-$, then $i in S_alpha$.
  Given a word $w in W(alpha, cal(A))$, let
  $
    op("wt")(w) = product_(i=1)^(\#lr(|alpha|)) op("sgn")(w_i) op("wt")(w_i) med.
  $
  Finally, let
  $
    F_alpha^(cal(A)) = sum_(w in W(alpha, cal(A))) op("wt")(w) med.
  $
  At this point, we can define the plethysm of $F_alpha$ by $cal(A)$ as
  $
    F_alpha [cal(A)] = F_alpha [op("wt")(cal(A))] = F_alpha^(cal(A))
  $
  and extend that definition to all quasisymmetric functions in the first member by linearity.
]<def-plt-qsym>

Note, for example, that by taking an alphabet with all positive signs, the definition given coincides with the intuitive idea of ordered substitution.

The definition given depends strongly on the order $prec$ chosen.
However, in the case where the first argument of plethysm is a symmetric function, it can be shown that the result of the operation is independent of $prec$ and coincides with the definition of plethysm given for symmetric functions (@ballantine_quasisymmetric_2020[Theorem 11]).
With this result, we conclude our generalization.

Finally, a Hopf algebra structure can also be defined on $"QSym"[X]$, so that it extends the structure previously defined on $"Sym"[X]$.
For further details, we refer to @luoto_hopf_2013[Section 3.3].
