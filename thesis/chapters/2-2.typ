#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== The modular law

When we introduced the chromatic polynomial of a $G$ graph, one of the first things we noticed was the linear relation of deletion-contraction  (@lmm-del-con-rel):
$
  p_G (r) = p_(G backslash e) (r) - p_(G slash e) (r) med.
$

We then observed that this relation does not generalize to the chromatic symmetric function, for example, because $chi_G (X)$ is homogeneous of degree $n$, while $chi_(G slash e)$ is homogeneous of degree $n-1$.
Since every relation between chromatic symmetric functions results in a relation between chromatic polynomials, let us try to find a linear relation between chromatic polynomials with the same number of vertices.

Let $G$ be a graph and let $v$, $w_1$ and $w_2$ be vertices of $G$, all connected to each other.
Let us call $e_1$ and $e_2$ the edges connecting $v$ to $w_1$ and $w_2$ respectively.
We further set $G_1 = G backslash e_1$, $G_2 = G backslash e_2$ and $G_(12) = G backslash e_1 backslash e_2$.
Applying the deletion-contraction relation, we obtain the following:
$
  cases(
    p_G &= p_(G_1) - p_(G slash e_1) \
    p_(G_2) &= p_(G_(12)) - p_(G_2 slash e_1)
  )
  quad ==> quad
  p_G + p_(G_(12)) = p_(G_1) + p_(G_2) - (p_(G slash e_1) - p_(G_2 slash e_1)) med.
$
Since $w_1$ and $w_2$ are connected, we have $G slash e_1 = G_2 slash e_1$, so the previous relation becomes
$
  p_G + p_(G_(12)) = p_(G_1) + p_(G_2) med.
$

At this point, it is natural to ask whether this identity is still valid for chromatic symmetric functions, and the answer is positive.
#lemma(name: [Modular law for chromatic symmetric functions (@guay-paquet_modular_2013[Proposition 3.1])])[
  Let $G$ be a graph and let $v$, $w_1$ and $w_2$ be vertices of $G$, all connected to each other.
  Let $e_1$ and $e_2$ be the edges connecting $v$ to $w_1$ and $w_2$ respectively.
  Then we have:
  $
    chi_G + chi_(G_(12)) = chi_(G_1) + chi_(G_2) med.
  $
]<lmm-csf-mod-law>
#proof[
  Recall that the chromatic symmetric function is the sum over all proper colorings of the monomial associated with the coloring, and in particular the latter does not depend on the edges of the graph.
  It is therefore sufficient to verify that each coloring has the same contribution to both members of the identity.
  Let $kappa$ be a generic coloring of $G$, and let us distinguish some cases.
  - If $kappa$ is not a proper coloring for $G_(12)$, neither is it a proper coloring for $G$, $G_1$ and $G_2$, so $k$ does not contribute to any member.
    Assume henceforth that $kappa in op("PC")(G_(12))$, so $kappa(w_1) eq.not kappa(w_2)$.
  - If $kappa(v) eq.not kappa(w_1)$ and $kappa(v) eq.not kappa(w_2)$, then $kappa$ is a proper coloring for all four graphs, so it contibutes twice to both the left and right members.
  - If $kappa(v) = kappa(w_1)$, then $kappa$ is a proper coloring only for $G_1$ and $G_(12)$, so it contributes once to both members.
  - If $kappa(v) = kappa(w_2)$, then $kappa$ is a proper coloring only for $G_2$ and $G_(12)$, and again it contributes once to both members.
]

Let us now see an application of the modular law to the incomparability graphs of $(3+1)$-free posets through part listings.
Let $G$ be a bicolored graph with three vertices $v$, $w_1$ and $w_2$ such that $w_1$ and $w_2$ are of the same color, different from that of $v$, and are both connected to $v$.
Let us call $e_1$ and $e_2$ the edges connecting $v$ to $w_1$ and $w_2$ respectively.
Let us consider the incomparability graph of the poset associated with $G$, i.e., $op("inc")(b_(i,i+1) (G))$: in this graph two vertices are connected if and only if they were not connected in $G$, in particular, $w_1$ and $w_2$ are connected.
Applying the modular law we get
#align(center)[#grid(
  columns: 7,
  align: bottom+center,
  column-gutter: .5em,
  row-gutter: .75em,
  [$chi_(op("inc")(b_(i,i+1) (G_(12))))$], [$+$], [$chi_(op("inc")(b_(i,i+1) (G_(12))) backslash e_1 backslash e_2)$],
  [$=$], [$chi_(op("inc")(b_(i,i+1) (G_(12))) backslash e_1)$], [$+$], [$chi_(op("inc")(b_(i,i+1) (G_(12))) backslash e_2)$],
  [$=$], [], [$=$], [], [$=$], [], [$=$],
  [$chi_(op("inc")(b_(i,i+1) (G_(12))))$], [$+$], [$chi_(op("inc")(b_(i,i+1) (G)))$],
  [$=$], [$chi_(op("inc")(b_(i,i+1) (G_2)))$], [$+$], [$chi_(op("inc")(b_(i,i+1) (G_1))) .$]
)]

More generally, suppose we have a bicolored graph part $b_(i,i+1) (G)$ in a part listing $L$, where $G$ has vertices and edges as described above.
Again, taking the incomparability graph of the poset associated with $L$, two vertices of $G$ are connected in $op("inc")(L)$ if and only if they are not connected in $G$.
Furthermore, adding or removing edges within $G$ does not change the order relation between pairs of vertices of $L$ that are not both contained in $G$, so no edges are added or removed in $op("inc")(L)$ that are not between vertices of $G$.
Thus we can extend the identity expressed above to the following.
#lemma(name: [Modular law for part listings])[
  Let $L$ be a part listing and let $v$, $w_1$ and $w_2$ be vertices belonging to the same bicolored graph part in $L$.
  Suppose that $w_1$ and $w_2$ are on the same level, while $v$ is on a different level, and that $v$ is connected to $w_1$ and $w_2$ by the edges $e_1$ and $e_2$ respectively.
  Then the following identity holds
  $
    chi_(op("inc")(L)) + chi_(op("inc")(L) backslash e_1 backslash e_2) = chi_(op("inc")(L) backslash e_1) + chi_(op("inc")(L) backslash e_2) med.
  $
]<lmm-mod-law-part-list>
