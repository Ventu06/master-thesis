#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Introduction to symmetric functions

In this section, we give some basics about symmetric functions.
We begin with their definition, both in the finite and infinite cases, then we look at the main bases and how they relate to each other, and finally, we introduce an operation called plethysm.
The main reference for this section and the next one is @macdonald_symmetric_1995.

Let us fix for the rest of the chapter a domain $F$ of characteristic $0$ on which to construct the symmetric and quasisymmetric functions, and let $overline(F)$ be its faction field.
In the rest of the thesis, this domain will be $ZZ$, $ZZ[q]$ or $ZZ[[q]]$ (the formal power series in the variable $q$).

#definition(name: [Symmetric functions])[
  Consider an infinite succession of variables $x_1, x_2, dots$.
  For each $n in N$ we have an action of $frak(S)_n$ on $F[x_1, dots, x_n]$ by permutation of the variables, i.e., where $sigma(x_i) = x_(sigma(i))$.
  We define the ring of #emph[symmetric functions in $n$ variables] as the fixed points of this action, i.e.
  $
    "Sym"_n [X] = F[x_1, dots, x_n]^(frak(S)_n) med.
  $
  Then we note that, for $m < n$, there exists a natural immersion $"Sym"_m [X] -> "Sym"_n [X]$.
  This allows us to take the inverse limit of this sequence and thus define the ring of #emph[symmetric functions] as
  $
    "Sym"[X] = lim_(<--) "Sym"_n [X] med.
  $
]<def-sym-fun>
Since the immersions $"Sym"_m [X] -> "Sym"_n [X]$ are homomorphisms of $F$-graded algebras, we also get a structure of $F$-graded algebras on $"Sym" [X]$.
The elements of $"Sym"[X]$, i.e., symmetric functions, can be thought of as formal series in infinite variables of finite degree in which monomials that differ by permutation of variables have the same coefficient.

Before showing some examples, let us introduce the concept of partition.
#definition(name: [Partition])[
  Given $n in NN$, a #emph[partition] $lambda$ of $n$, briefly $lambda tack.r n$, is a sequence of positive integers $(lambda_1, lambda_2, dots, lambda_k)$ with $lambda_1 >= lambda_2 >= dots >= lambda_k > 0$ and $sum_(i=1)^k lambda_i = n$.
]<def-part>
Given a partition $lambda = (lambda_1, dots, lambda_k)$ of $n$, we denote by $\#lr(|lambda|)$ the size of $lambda$, i.e. $n$, and by $l(lambda)$ the length of $lambda$, i.e. $k$.

Let us now introduce some classes of symmetric functions, all indexed on partitions.
#definition(name: [Monomial symmetric functions])[
  Given a partition $lambda$, let us introduce the #emph[monomial symmetric function] $m_lambda$.
  This consists of the sum of all monomials $x_(i_1)^(e_1) x_(i_2)^(e_2) dots x_(i_k)^(e_k)$, each considered once, varying indices $i_1, dots, i_k$ all
  different from each other and where $(e_1, dots e_k)$ is a permutation of the parts of $lambda$.
]<def-sym-fun-m>

For example
$
  m_(2,1,1) = x_1^2 x_2 x_3 + x_1 x_2^2 x_3 + x_1 x_2 x_3^2 + x_1^2 x_2 x_4 + dots med.
$

Note also that for each $f in "Sym"[X]$ we have
$
  f = sum_(lambda "partition") ([x_1^(lambda_1) dots x_(l(lambda))^(lambda_(l(lambda)))] f) m_lambda med.
$
This last expression shows that monomial symmetric functions are a basis of $"Sym"[X]$ as a vector space.

#definition(name: [Elementary symmetric functions])[
  For each $n in NN$ we set
  $
    e_n = sum_(i_1 < i_2 < dots < i_n) x_(i_1) x_(i_2) dots x_(i_n) med.
  $
  Given a partition $lambda$, we define the #emph[elementary symmetric function] $e_lambda$ as
  $
    e_lambda = product_(i=1)^(l(lambda)) e_(lambda_i) med.
  $
]<def-sym-fun-e>

For example
$
  e_(2,1,1) = (x_1 x_2 + x_1 x_3 + x_2 x_3 + x_1 x_4 + dots) (x_1 + x_2 + x_3 + dots)^2 med.
$

#definition(name: [Complete symmetric functions])[
  For each $n in NN$ we set
  $
    h_n = sum_(i_1 <= i_2 <= dots <= i_n) x_(i_1) x_(i_2) dots x_(i_n) med.
  $
  Given a partition $lambda$, we define the #emph[complete symmetric function] $h_lambda$ as
  $
    h_lambda = product_(i=1)^(l(lambda)) h_(lambda_i) med.
  $
]<def-sym-fun-h>

For example
$
  h_(2,1,1) = (x_1^2 + x_1 x_2 + x_2^2 + x_1 x_3 + dots) (x_1^2 + x_1 x_2 + x_2^2 + dots)^2 med.
$

#definition(name: [Powersum symmetric functions])[
  For each $n in NN$ we set
  $
    p_n = sum_(i) x_i^n med.
  $
  Given a partition $lambda$, we define the #emph[powersum symmetric function] $p_lambda$ as
  $
    p_lambda = product_(i=1)^(l(lambda)) p_(lambda_i) med.
  $
]<def-sym-fun-p>

For example
$
  p_(2,1,1) = (x_1^2 + x_2^2 + x_3^2 + dots) (x_1 + x_2 + x_3 + dots)^2 med.
$

Note how elementary, complete and powersum symmetric functions are multiplicative, that is, obtained by multiplying other symmetric functions according to parts of $lambda$.

Let us now state a fundamental theorem in the theory of symmetric functions.
#theorem(name: [Bases of symmetric functions])[
  + For each $n in NN$, the sets of symmetric functions ${m_lambda mid(|) lambda tack.r n}$, ${e_lambda mid(|) lambda tack.r n}$, ${h_lambda mid(|) lambda tack.r n}$ and ${p_lambda mid(|) lambda tack.r n}$ are bases of the component of degree $n$ of $"Sym"[X]$ viewed as a vector space.
  + The sets ${e_n mid(|) n in NN}$, ${h_n mid(|) n in NN}$ and ${p_n mid(|) n in NN}$ consist, within them, of algebraically independent elements.
  Consequently,
  $
    "Sym"[X] = F[e_0, e_1, e_2, dots] = F[h_0, h_1, h_2, dots] = F[p_0, p_1, p_2, dots] med.
  $
]<thm-sym-fun-mehp-basis>

Let us now define an important involution that periodically comes up when working with symmetric functions.
#definition(name: [$omega$ involution])[
  Let $omega : "Sym"[X] -> "Sym"[X]$ be the homomorphism defined by $omega(p_n) = (-1)^(n-1) p_n$.
  Since the various $p_n$ are algebraically independent and they generate $"Sym"[X]$, $omega$ is well-defined.
  Note that $omega^2 = op("id")$, so $omega$ is an automorphism and an involution.
]<def-omega>

It follows immediately from the definition that $omega(p_lambda) = (-1)^(\#lr(|lambda|) - l(lambda)) p_lambda$.
Furthermore, it can be verified that $omega(e_lambda) = h_lambda$ and $omega(h_lambda) = e_lambda$.
As for the monomial basis, we have $omega(m_lambda) = f_lambda$ where the $f_lambda$ are the so-called #emph[forgotten symmetric functions], of which we do not have an immediate description like the one of the other bases.

Finally, we introduce a final class of symmetric functions, the Schur symmetric functions.
#definition(name: [Schur symmetric functions])[
  Given a sequence $alpha_1, dots, alpha_n$, we define
  $
    a_(alpha_1, dots, alpha_n) = det mat(
      x_1^(alpha_1), x_2^(alpha_1) dots, x_n^(alpha_1);
      x_1^(alpha_2), x_2^(alpha_2) dots, x_n^(alpha_2);
      dots.v, dots.v, dots.down, dots.v;
      x_1^(alpha_n), x_2^(alpha_n) dots, x_n^(alpha_n);
    ) med.
  $
  Notice that $a_(n-1, n-2, dots, 0) = product_(1 <= i < j <= n) (x_i - x_j)$ is the Vandermonde determinant and that $a_(alpha_1, dots, alpha_k)$ is always divisible by $a_(n-1, n-2, dots, 0)$ since the determinant is alternating.
  For a partition $lambda$, possibly padded with zeros at the end, let us set
  $
    s_lambda = a_(lambda_1 + n-1, lambda_2 + n-2, dots, lambda_n) / a_(n-1, n-2, dots, 0) med.
  $
  Since both the numerator and the denominator are alternating, we find that $s_lambda$ is a symmetric function in $n$ variables.
  In addition, it can be verified that $s_lambda (x_1, dots, x_n, 0) = s_lambda (x_1, dots, x_n)$, thus it is possible to take the limit on $n$ and obtain an element $s_lambda in "Sym"[X]$.
]<def-sym-fun-s>

As with the previous bases, it can be verified that ${s_lambda mid(|) lambda tack.r n}$ is a basis of the component of degree $n$ of $"Sym"[X]$ viewed as a vector space.
In addition, we have $omega(s_lambda) = s_(lambda')$, where $lambda'$ is the partition conjugated to $lambda$, i.e.,
$
  lambda' = (\#lr(|{i in [l(lambda)] mid(|) lambda_i >= 1}|), \#lr(|{i in [l(lambda)] mid(|) lambda_i >= 2}|), dots, \#lr(|{i in [l(lambda)] mid(|) lambda_i >= lambda_1}|)) med.
$
There is another equivalent definition of Schur symmetric functions by means of Young's tableaux, which we do not discuss here.

Now that we have all the bases, let us see how it is possible to change from one basis to another.
In the table below, we show the coefficients in the expansion
$
  v_lambda = sum_(mu tack.r \#lr(|lambda|)) c_(lambda, mu) u_mu
$
for different choices of bases ${v_lambda}$ and ${u_mu}$.

#figure(caption: [Bases change coefficients for symmetric functions])[#text(size: 10pt)[#table(
  columns: 6,
  stroke: none,
  align: horizon+center,
  inset: 7.5pt,
  table.hline(),
  table.header([], table.vline(stroke: .5pt), [$h_lambda$], [$e_lambda$], [$s_lambda$], [$p_lambda$], [$m_lambda$]),
  table.hline(stroke: .5pt),
  [$h_mu$], [$I$], [$(-1)^(n-l(mu)) \#lr(|B_(mu, lambda)|)$], [$(K_(lambda, mu))^(-1)$], [$(-1)^(l(mu)-l(lambda)) w(B_(mu, lambda))$], [$(I M_(lambda, mu))^(-1)$],
  [$e_mu$], [$(-1)^(n-l(mu)) \#lr(|B_(mu, lambda)|)$], [$I$], [$(K_(lambda', mu))^(-1)$], [$(-1)^(n-l(mu)) w(B_(mu, lambda))$], [$(B M_(lambda, mu))^(-1)$],
  [$s_mu$], [$K_(lambda, mu)$], [$K_(lambda', mu)$], [$I$], [$chi_(mu, lambda)$], [$(K_(lambda, mu))^(-1)$],
  [$p_mu$], [$\#lr(|O B_(mu, lambda)|) slash z_mu$], [$(-1)^(n - l(mu)) \#lr(|O B_(mu, lambda)|) slash z_mu$], [$chi_(lambda, mu) slash z_mu$], [$I$], [$(-1)^(l(mu) - l(lambda)) w(B_(lambda, mu)) slash z_mu$],
  [$m_mu$], [$ M_(lambda, mu)$], [$B M_(lambda, mu)$], [$K_(lambda, mu)$], [$\#lr(|O B_(lambda, mu)|)$], [$I$],
  table.hline(),
)]]<tab-sym-fun-bases-coeff>

The main references for this table are given by @egecioglu_brick_1991 and @kulikauskas_lyndon_2006.

We do not explain here the combinatorial interpretation of the various coefficients.
However, we would like to remark that, except for the line concerning $p_mu$, all the coefficients are integers.
In particular, by choosing a suitable ordering on partitions, the matrices $B M_(lambda, mu)$, $I M_(lambda, mu)$ and $K_(lambda, mu)$ are triangular with all $1$ on the diagonal, thus invertible always with integer coefficients.

We report in @fig-sym-fun-pos-exp the poset in which ${u_lambda} succ {v_lambda}$ if the elements of ${u_lambda}$ expand positively in the base ${v_lambda}$.
The dashed line indicates that the coefficients of the expansion are not necessarily integers.

#figure(caption: [Positive expansion poset for symmetric functions])[
  #drawings.sym-fun-pos-exp
]<fig-sym-fun-pos-exp>

Finally, let us introduce an operation between symmetric functions called #emph[plethysm].
For this last part, the recommended reference is @loehr_computational_2011.

Given a symmetric function $f(X) = f(x_1, x_2, dots)$, it is sometimes necessary to evaluate it by specializing the variables ${x_i}_(i in NN^+)$ in particular ways, for example, we may want to consider $f(x_1^3, x_2^3, dots)$ or $f(1, q, q^2, q^3, dots)$.
Very often, as in the two examples just shown, we would like to put monomials satisfying a certain pattern in place of variables.
Since the functions in question are invariant by permutation of the variables, we can consider the set of monomials: if the sum of all these monomials is represented by a particular function $g$, we call $f[g]$ the substitution of the monomials appearing in $g$ into the variables of $f$.
For example, in the first of the previous cases, we have $g = x_1^3 + x_2^3 + dots = p_(3)$, so we write $f(x_1^3, x_2^3, dots) = f[p_(3)]$.
Similarly, in the second case we have $g = 1 + q + q^2 + dots = 1/(1-q)$, so we write $f(1, q, q^2, q^3, dots) = f[1/(1-q)]$.
Furthermore, for every $f in "Sym"[X]$ we have $f = f(X) = f[p_(1)]$, so with a slight abuse of notation we consider $X = x_1 + x_2 + dots = p_(1)$.

The operation denoted by $dot[dot]$ is called plethysm.

Let us now proceed to give a solid foundation to the plethysm operation we have intuitively introduced.
From now on we fix an $F$-algebra $Z$, which in practice is often $"Sym"[X]$ or at least contains $"Sym"[X]$.
#definition(name: [Plethysm (for symmetric functions)])[
  Our goal is to define a function
  $
    dot[dot] : "Sym"[X] times Z -> Z
  $
  that behaves in the way described above.
  Let us proceed in the following way.
  - We choose a set of homomorphisms of $F$-algebras $L_m$ for $m in NN^+$ that represents the operation $p_m [dot]$, i.e., such that $L_m (g) = p_m [g]$ for every $g in Z$.
    If $"Sym"[X]$ is a subalgebra of $Z$ we also require that $L_m (p_n) = p_m [p_n] = p_(m n)$.
    For example, if $F = ZZ$ and $Z$ is the algebra of symmetric functions on the formal series $ZZ[[q]]$, we can define $L_m$ by requiring that $L_m (p_n) = p_(m n)$ and $L_m (q) = q^m$.
  - For each $g in Z$, we construct the homomorphism of $F$-algebras $R_g : "Sym"[X] -> Z$ defined on the basis $p_m$ as $R_g (p_m) = L_m (g)$.
  - We finally pose $f[g] = R_g (f)$.
  It can be verified that there is a single operation $dot[dot] : "Sym"[X] times Z -> Z$ with the listed properties.
]<def-plt-sym>
From the conditions imposed, we see immediately that $dot[g]$ is a homomorphism of $F$-algebras, while the same can be said of $f[dot]$ only if $f = p_n$ for some $n in NN^+$.
In particular, plethysm is not symmetric in the two arguments.

Let us look at some rules that make it easier to calculate plethysm.

#proposition(name: [Negation rule])[
  If $f$ is homogeneous symmetric function of degree $n$, then
  $
    f[-g] = (-1)^n omega(f) [g] med.
  $
]<prop-plt-neg>

#proposition(name: [Monomial substitution rule])[
  Let $f in "Sym"[X]$ and let $f_n in "Sym"_n [X]$ be the respective symmetric function with $n$ variables.
  If $g in Z$ is a finite sum of $n$ monomials, i.e., $g = g_1 + dots + g_n$ with $g_i$ monomials for every $i$, then
  $
    f[g] = f_n (g_1, dots, g_n) med.
  $
]<prop-plt-subst-fin>

#proposition(name: [Addition rule])[
  Let $f, g in Z$, then
  $
    e_n [f+g] &= sum_(i=0)^n e_i (f) e_(n-i) (g) med, \
    h_n [f+g] &= sum_(i=0)^n h_i (f) h_(n-i) (g) med.
  $
  There is actually an identity that generalizes both of those just expressed using a variant of Schur's symmetric functions. We refer to @loehr_computational_2011[Theorem 8] for further details.
]<prop-plt-add>

We close this section by showing how plethysm gives us a Hopf algebra structure on symmetric functions.
- For the counit $epsilon : "Sym"[X] -> F$ we set
  $
    epsilon(f) = f[0] med.
  $
  In this way $epsilon(p_n) = delta_(0,n)$.
- For the antipode $S : "Sym"[X] -> "Sym"[X]$ we set
  $
    S(f) (X) = f[-X] med.
  $
  In this way $S(p_n) = (-1)^n omega(p_n) = (-1)^(delta_(0,n)) p_n$.
- For the comultiplication $Delta : "Sym"[X] -> "Sym"[X] times.circle "Sym"[X]$ we set
  $
    Delta(f) (X,Y) = f[X+Y] med.
  $
  In this way $Delta(p_n) = p_n times.circle 1 + 1 times.circle p_n$.

The one defined is indeed a Hopf algebra, in fact, considering the unit $nu : F -> "Sym"[X]$, we have
$
  (nabla compose S times.circle op("id") compose Delta) (f)
    = (nabla compose S times.circle op("id")) (f[X+Y])
    = nabla (f[-X+Y])
    = f[0]
    = nu(epsilon(f))
$
and similarly it occurs that $nabla compose op("id") times.circle S compose Delta = nu compose epsilon$.
