#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== Shareshian-Wachs conjecture

With the introduction of the chromatic quasisymmetric function, one might wonder if any analog of the Stanley-Stembridge conjecture is valid.
In general, for the incomparability graph of $(3+1)$-free poset, such a function is not symmetric (see @cntexm-cqsf-not-sym), so it cannot be expanded in the elementary basis of symmetric functions.
At present, there does not seem to be any refinement of the elementary basis in quasisymmetric functions in the literature, so there is still no conjecture that refines Stanley-Stembridge in the generality of $(3+1)$-free posets.
However, in the case of unit interval graphs, by @thm-uig-cqsf-sym we know that the chromatic quasisymmetric function is symmetric.
In these cases, we can therefore ask the question of whether the coefficients in the expansion in the elementary basis belong to $NN[q]$ (we also say in this case that the function is $e$-positive).
This question appears to be experimentally true, so much so that it was conjectured by Shareshian and Wachs in @shareshian_chromatic_2012[Conjecture 4.9] and @shareshian_chromatic_2016[Conjecture 1.1].
#conjecture(name: [Shareshian-Wachs])[
  Let $G$ be a unit interval graph, then $chi_G^q (X;q)$ is $e$-positive, that is, $chi_G^q (X;q) in NN[{e_lambda}_(lambda tack.r n), q]$.
]

Clearly, by placing $q |-> 1$, we regain the Stanley-Stembridge conjecture for unit interval orders.
However, thanks to the Guay-Paquet theorem seen in the previous chapter, we know that it is sufficient to verify the latter case, so we can say that the Shareshian-Wachs conjecture generalizes the Stanley-Stembridge conjecture.

As in the case of the Stanley-Stembridge conjecture, the Shareshian-Wachs conjecture would imply the positivity of chromatic quasisymmetric functions in their expansion in the Schur basis.
In the previous case, this problem was solved by Gasharov, see @thm-csf-exp-s-1.
In the article @shareshian_chromatic_2016[Theorem 6.3], Shareshian and Wachs were able to adapt the proof to the quasisymmetric case and obtain the following.
#theorem()[
  Let $G$ be a unit interval graph, then
  $
    chi_G (X) = sum_(A in op("SPT")(P)) q^(op("inv")_G (A)) s_(lambda(A))
  $
  where
  $
    op("inv")_G (A) = \#lr(|{ {a_{i_1,j_1}, a_{i_2,j_2}} in E mid(|) a_{i_1,j_1} < a_{i_2,j_2}, i_1 > i_2 }|) med.
  $
]<thm-cqsf-exp-s-1>
The idea of the proof is similar to the one used by Gasharov, however, the original involution did not preserve $G$-inversions, so a different involution had to be found.

Further evidence in support of the Shareshian-Wachs conjecture is provided by the identity we now enunciate.
First, we must introduce the concept of acyclic orientation of a graph, an idea that is developed more fully in the next chapter.
An acyclic orientation is a choice of a direction for each edge so that no direct cycles, that is, closed paths along the chosen direction, are formed.
Given an acyclic orientation, a source is a vertex at which all incident edges have direction exiting the vertex.
We denote by $op("AO")(G,j)$ the set of acyclic orientations of $G$ with exactly $j$ sources, and by $op("AO")(G)$ the set of all acyclic orientations of $G$.
We also denote by $[e_lambda] chi_G^q (X;q)$ the coefficient of $e_lambda$ in the expansion in the elementary basis of $chi_G^q (X;q)$ (clearly for $G$ unit interval graph).
Starting from an identity obtained by Stanley in @stanley_symmetric_1995[Theorem 3.3], Shareshian and Wachs enunciated the following in @shareshian_chromatic_2016[Theorem 5.3].
#theorem()[
  Let $G$ be a unit interval graph, then, for each $1 <= j <= n$, we have
  $
    sum_(lambda tack.r n \ l(lambda) = j) [e_lambda] chi_G^q (X;q) = sum_(theta in op("AO")(G,j)) t^(op("inv")_G (theta))
  $
  where
  $
    op("inv")_G (theta) = \#lr(|{ (i -> j) in theta mid(|) i > j }|) med.
  $
  By $(i -> j) in theta$ we denote that there is an edge between $i$ and $j$ oriented from $i$ toward $j$.
]<thm-cqsf-e-coeff-ao>
Thanks to this theorem, we can reduce the Shareshian-Wachs conjecture to the following problem: find a function $phi : op("AO")(G) -> { lambda tack.r n }$ such that $l(phi(theta))$ equals the number of sources of $theta$ and the following holds
$
  chi_G^q (X;q) = sum_(theta in op("AO")(G)) q^(op("inv")(theta)) e_(phi(theta))
$

Moreover, the conjecture has been proved in some special cases.
Let us look at some of them.
- Graphs where the complement graph is also a unit interval graph.
  This was proved by Foley et al. in @foley_classes_2019[Corollary 19], dividing the possible types of graphs into several classes and also relying on previous results.
- #grid(columns: (2fr, 1fr), align: horizon,
  [
    Triangular ladders, that is, unit interval graphs with $h(j) = min(j+2, n)$ for every $1 <= j <= n$.
    This was proved by Dahlberg in @dahlberg_triangular_2019[Theorem 4.5].
    The author considered a generalization of the chromatic quasisymmetric function with non-commuting variables which satisfies a deletion-contraction relation, obtained a formula by recursively using this identity and concluded with an involution.
  ], [
    #align(center)[#drawings.interval-graph((3,4,5,5,5))]
  ])
- #grid(columns: (2fr, 1fr), align: horizon,
  [
    Lollipop graphs, that is complete graphs with a tail of vertices attached.
    This was proved also by Dahlberg in @dahlberg_lollipop_2018[Theorem 8] using recurrence with a linear relation, i.e., the triple-deletion, then generalized to the $k$-deletion.
  ], [
    #align(center)[#drawings.graph-melting-lollipop(2,6,0)]
  ])
- #grid(columns: (2fr, 1fr), align: horizon,
  [
    Melting lollipop graphs, i.e., lollipop graphs from which some edges have been removed.
    This was proved by Huh et al. in @huh_melting_2020[Theorem 4.9], this time using a duality with LLT polynomials.
    We see this fact in more detail in two sections.
    A later generalization was given by Tom in @tom_signed_2024[Corollary 4.20].
  ], [
    #align(center)[#drawings.graph-melting-lollipop(2,6,2)]
  ])
- Generalized pyramid graphs and $(2K_2)$-free unit interval graphs (i.e., graphs without subgraphs that are isomorphic to the disjoint union of two complete graphs with 2 vertices).
  This was proved by Li and Yang in @li_e-positivity_2021[Theorem 13] by differentiating various cases, directly calculating coefficients and using previous results.
- Unit interval graphs whose poset is also $(2+1+1)$-free.
  This was proved by McDonough et al. in @mcdonough_stanley-stembridge_2024[Theorem 4.1].
  The authors used the new technique of strand diagrams.
  Similar techniques have already been widely used in representation theory with excellent success, such as the proof of the positivity of Kazhdan-Lustzig polynomials by Elias and Williamson.

Furthermore, Abreu and Nigro in @abreu_splitting_2023[Corollary 3.4] were able to prove the positivity of the coefficients of the terms $e_(lambda_1, lambda_2)$, that is, those associated with partitions of length 2.
To do this, they used a relation of the chromatic quasisymmetric functions with the cohomology of Hessenberg varieties.
We take a closer look at this relation in the next section.

To conclude this section, we leave some comments on the possibility of generalizing the Shareshian-Wachs conjecture to some larger class of graphs, where the chromatic quasisymmetric function is not necessarily symmetric.
As we had anticipated, at present there does not seem to be in the literature an elementary basis refinement in which to try to positively expand our functions.
Two approaches can therefore be attempted:
+ Construct such a refinement without regard to chromatic quasisymmetric functions.
  As mentioned in @def-qsym-fun-psi, the duality between non-commutative symmetric functions and quasisymmetric functions is used to define quasisymmetric powersum functions.
  Specifically, we generalize the identity $angle.l p_lambda, p_mu angle.r = delta_(lambda, mu) z_lambda $ valid on symmetric functions to an identity $angle.l bold(Psi)_alpha, Psi_beta angle.r = delta_(alpha, beta) z_alpha$ where $bold(Psi)_alpha$ is a noncommutative symmetric function and $Psi_beta$ is the quasisymmetric function we want to define.
  In the case of elementary symmetric functions, we would like to generalize the identity $angle.l e_lambda, f_mu angle.r = delta_(lambda, mu)$, where the $f_mu$ are the #emph[forgotten symmetric functions], or alternatively, applying the involution $omega$, the identity $angle.l h_lambda, m_mu angle.r = delta_(lambda, mu)$.
  Unfortunately, forgotten symmetric functions tend to lend themselves hardly to combinatorial manipulation and interpretation, and they do not yet have a noncommutative analog, as likewise do monomial symmetric functions.
  An alternative idea that might be more successful is to use some identity that binds the elementary basis to other bases, such as monomial or powersum, and try to generalize this identity into the quasisymmetric case.
  At the moment we do not have a definition that has good properties, either algebraic or combinatorial, but we do not rule out that it can be obtained in similar ways.
+ Start from the chromatic quasisymmetric functions of a certain set of graphs and search for a basis that refines the elementary basis in which these positively expand.
  An additional condition that could be imposed is that the elements of this basis expand positively into the monomial basis, perhaps with rational coefficients with limited denominator (e.g., dividing $n!$).
  This reduces the problem to a system of equalities and inequalities between polynomials, computationally approachable in not too large a dimension.
  To make this idea work, it is important to choose the right class of graphs on which to search for a basis: for example, on unit interval graphs a solution is given by the elementary basis, so a larger class must be used.
  We believe that the success of such an approach may be more to show that a basis satisfying certain conditions for a certain class of graphs does not exist, rather than to find such a basis: it is possible that a successful outcome will lead to a large number of bases and that it will be difficult to distinguish which ones have useful properties and how they can be constructed otherwise.
  We tried this approach briefly but without getting results, mainly because of computational optimization problems that prevented us from working on dimensions large enough to be useful (i.e., $n > 4$).
