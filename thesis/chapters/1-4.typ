#import "../misc/settings.typ": *
#import "../misc/drawings.typ"
#show: default_settings

== The Stanley-Stembridge conjecture

Now that we have introduced all the necessary elements, we can state the conjecture we want to study.

#conjecture(name: [Stanley-Stembridge])[
  Let $P$ be a $(3+1)$-free poset and let $G = op("inc")(P)$, then $chi_G (X)$ is $e$-positive.
]<conj-stan-stem>

This conjecture was introduced by Stanley and Stembridge around 1993 in the articles @stanley_immanants_1993[Conjecture 5.5] and @stanley_symmetric_1995[Conjecture 5.1].
Although it was formulated several years ago and has been widely studied, being considered one of the most important problems in algebraic combinatorics, this conjecture still remains open.

At this point, a natural question might arise: why do we care about positive expansion in elementary functions?
Generally, in algebraic combinatorics, a positive expansion in the elementary, complete or Schur basis is an indicator of some structure behind the problem, thus an invitation to discover that structure.

More specifically, remembering that the monomials of Schur's basis are in bijection with the irreducible representations of the symmetric group and the general linear group (see @prop-frob-irr-s), a positive expansion in Schur's basis might indicate the presence of $frak(S)_n$ or $op("GL")(n)$ action on a structure related to our function.
In the case of $chi_(op("inc")(P)) (X)$ with $P$ poset $(3+1)$-free we will see a couple of results on $s$-positivity: the first one in @thm-csf-exp-s-1, and later in @thm-cqsf-exp-s-1, concerning structures called $P$-tableaux; the second one appears later in @thm-cqsf-ch-hess and concerns a more specific case in which we explicitly find an action of the symmetric group.

Similarly, elementary and complete symmetric functions are in correspondence with a certain class of symmetric group representations induced by a Young subgroup, see @prop-frob-id-h and @prop-frob-sgn-e.
For this reason, one possible approach to the Stanley-Stembridge conjecture might be to look for a structure on which a group action such as the one just given is defined, and then try to make explicit the link between chromatic symmetric functions and such a structure, similar to what is done in @thm-cqsf-ch-hess for $s$-positivity.

Let us now return to the Stanley-Stembridge conjecture.
Since elementary symmetric functions expand positively in the Schur basis (see @fig-sym-fun-pos-exp), this conjecture would also imply $s$-positivity.
As anticipated, this fact was proved by Gasharov in @gasharov_incomparability_1996, an article in which an explicit formula is given that allows computing the chromatic symmetric function in Schur's basis.
Let us proceed to enunciate this result.

Given a poset $P = (P, prec.eq)$, we define a $P$-array as a grid $A = {a_(i,j)}_(1 <= i <= h \ 1 <= j <= k)$ of elements of $P$, in which some entries may also be undefined.
We require that $A$ satisfies the following conditions:
1. rows are left-justified: if $a_(i,j+1)$ is defined, then $a_(i,j)$ is also defined
2. the rows are strictly increasing: if $a_(i,j)$ and $a_(i,j+1)$ are defined, then $a_(i,j) prec a_(i,j+1)$
We say that a $P$-array is a $P$-tableau if it satisfies the following additional condition:
3. If $a_(i+1,j)$ is defined, then $a_(i,j)$ is also defined and $a_(i+1,j) prec.not a_(i,j)$
Note the similarity between $P$-tableaux and semi-standard Young tableaux, from which Schur symmetric functions arise, in particular, we can associate a partition to the shape of a $P$-tableau.
We further say that a $P$-tableau is standard if all $a_(i,j)$ are distinct.
Let $op("SPT")(P)$ be the set of standard $P$-tableaux.

We can now state the following theorem.
#theorem(name: [@gasharov_incomparability_1996[Theorem 4]])[
  Let $P$ be a $(3+1)$-free poset and let $G = op("inc")(P)$, then
  $
    chi_G (X)
    &= sum_(A in op("SPT")(P)) s_(lambda(A)) \
    &= sum_(lambda tack.r n) \#lr(|{A in op("SPT")(P) mid(|) lambda(A) = lambda}|) s_lambda
  $
  where $lambda(A)$ is the shape of $A$.
]<thm-csf-exp-s-1>

The proof of this theorem uses multicoloring, that is, assigning multiple colors to the same vertex, and actually yields a more general result concerning an analog of the chromatic symmetric function with multicoloring.
The main idea is to assign a $P$-array to each multicoloring and to construct a sign-reversing involution that leads to the deletion of all $P$-arrays except the $P$-tableaux.

Given the relation between $(3+1)$-free posets and claw-free graphs (see @fig-poset-claw), one might wonder whether Gasharov's theorem and the Stanley-Stembridge conjecture are valid for generic claw-free graphs.

The answer to the first of these questions would seem to be positive, in fact, we have the following conjecture made by Gasharov in @stanley_graph_1998[Conjecture 1.4], which, however, is still without proof.
#conjecture()[
  Let $G$ be a claw-free graph, then $chi_G (X)$ is $s$-positive.
]<conj-g-claw-free-s-pos>

On the other hand, the second question is answered in the negative; in fact, see the following counterexample.
#counterexample()[
  #grid(columns: (2fr, 1fr), align: horizon,
  [
    For the graph $G$ in the picture, we have
    $
      chi_G (X)
        &= 48 s_(1^6) + 48 s_(2, 1^4) + 24 s_(2^2, 1^2) + 12 s_(3, 1^3) + 6 s_(3,2,1) \
        &= 6 e_(3,2,1) - 6 e_(3^2) + 6 e_(4,1^2) + 12 e_(4,2) + 18 e_(5,1) + 12 e_(6) med.
    $
    Notice how the graph is claw-free but the coefficient of $e_(3^2)$ is negative.
    Notice also that $chi_G (X)$ is $s$-positive.
  ],[
    #align(center)[#drawings.graph-3]
  ])
]<cntexm-claw-free-not-e-pos>

The biggest step toward a solution to the Stanley-Stembridge conjecture was taken by Guay-Paquet in @guay-paquet_modular_2013.
In this paper, he showed that it is sufficient to verify the conjecture only for both $(3+1)$-free and $(2+2)$-free posets, that is, for unit interval orders, to prove the general case.
From what we saw in the previous section, unit interval orders have a much more satisfactory characterization than $(3+1)$-free posets, e.g., we can enumerate the former via Dyck paths while we know relatively little about the latter.
This simplification has opened up many new possibilities, chief among them a generalization of the conjecture by Shareshian and Wachs which, given subsequent results, seems to be pointing to the right way of looking at this problem.
Unfortunately, this is not enough and nowadays the conjecture remains open.

The whole of the next chapter is directed toward studying in detail the proof given by Guay-Paquet, while in the following chapters, we see the generalization of Shareshian and Wachs and some techniques for dealing with the problem.
