#set par(justify: true)

////

#align(center)[
#text(18pt)[Master thesis abstract: Chromatic quasisymmetric functions]

#text(14pt)[Candidate: Eduardo Venturini \ Supervisor: prof. Michele D'Adderio]
]

#v(20pt)



The aim of this thesis is to deal with chromatic quasisymmetric functions and the Shareshian-Wachs conjecture.

In the first part, we will gradually introduce the problem, showing what are the main reasons why it is studied. \
We will begin with graph colorings, defining chromatic symmetric functions, dealing with the Stanley-Stembride conjecture, which concerns the positivity of coefficients in the expansion in the elementary basis of these functions, and how this conjecture can be reduced to a restricted class of graphs, the unit interval graphs.
Then, adding an additional statistic on the colorings, we will generalize our chromatic symmetric functions to quasisymmetric functions, and state the Shareshian-Wachs conjecture, a natural generalization of Stanley-Stembridge conjecture.
We will also briefly enunciate results and conjectures similar to those in question, such as the demonstrated positive expansion in the Schur basis, and links to other areas of mathematics, for example to representations on cohomology groups of Hessenberg varieties.

At this point we will proceed with a review of the currently existing literature, focusing mainly on articles that have been published since the introduction of quasisymmetric color functions and thus in the last decade.
We will treat the expansion of these functions in different bases, in particular:
- a formula given in 2015 by Athanasiadis, through the use of permutations with particular properties and the power basis, valid only for unit interval graphs.
- a formula given in 2019 by Alexandersson and Sulzgruber, through the use of acyclic orientations and a generalization of the powersum basis, valid for any graph.
- a formula given in 2020 by Abreu and Nigro through the use of increasing spanning forests and the Hall-Littlewood basis, valid only for unit interval graphs.
  The proof uses a "modular law," i.e., a local relation between graphs, to conclude with an equality that is true globally, and it is not constructive.
- a formula given in 2023 by D'Adderio et al., in which an earlier identity due to Carlsson and Mellit is generalized to a more general class of graphs, interval graphs.

The last part of the thesis will be devoted to original findings and reflections, and possible ideas to be developed in the future. \
Following D'Adderio's first example, we will try to generalize a part of the theory mentioned above to interval graphs, on which, however, there is not (yet) an analog of the Shareshian-Wachs conjecture.
We will also try to unify the various results present so far, showing that the different structures used have much in common with each other and it is not complicated to transfer some results from one structure to another in the case of interval graphs.
In this way, for example, we will illustrate a generalization of the formula given by Athanasiadis, conjectured in the article by D'Adderio et al. and demonstrated either by using Alexandersson and Sulzgruber's formula or by going through a different path. \
We will also introduce another structure, similar to the increasing spanning forests but with less stringent conditions and, in some respects, even different behaviors, and use it to give further expansion of chromatic quasisymmetric functions.
Finally, again through this last structure, we will show an approach to the Shareshian-Wachs conjecture that experimentally worked on small cases and in a sense refines this conjecture by restricting some degrees of freedom.