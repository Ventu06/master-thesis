#import "@preview/lemmify:0.1.5": *
#let (
  theorem, lemma, corollary,
  remark, proposition, example,
  proof, rules: default-theorems-rules
) = default-theorems("thm-group", lang: "en", thm-numbering: thm-numbering-linear)
#show: default-theorems-rules
#let (conjecture, rules: conjecture-rules) = new-theorems("thm-group", ("conjecture": text[Conjecture]), thm-numbering: thm-numbering-linear)
#show: conjecture-rules

#set math.equation(numbering: "(1)")
#show ref: it => {
  if it.element != none and it.element.func() == math.equation {
    numbering(it.element.numbering, ..counter(math.equation).at(it.element.location()))
  } else {
    it
  }
}

#set par(justify: true)

////

#align(center)[
#text(20pt)[Master thesis extended abstract: \ Chromatic quasisymmetric functions]

#text(14pt)[Candidate: Eduardo Venturini \ Supervisor: prof. Michele D'Adderio]
]

#v(32pt)


This thesis aims to deal with quasisymmetric chromatic functions and the Shareshian-Wachs conjecture.

In the first part we will introduce the objects and problems to be addressed, briefly showing their historical evolution, consequences and results currently found in the literature.
On the other hand, the second part will be devoted to exhibiting some original results obtained with the collaboration of Giovanni Interdonato, Ph.D. student, and under the supervision of Prof. Michele D'Adderio, and then indicating possible future developments.


= Introduction of the problem and current status

Given a simple graph $G$, we define the chromatic polynomial $p_G (n)$ as the function that counts the number of proper colorings of the graph with $n$ colors, that is, the ways of assigning a color to each vertex such that two vertices connected by an arc have distinct colors.
This polynomial constitutes one of the best-known statistics on graphs and has been widely studied.

In 1995 Richard P. Stanley in @stanley_symmetric_1995 introduced a generalization of it, the chromatic symmetric function $chi_G (X)$ defined as
$
	chi_G (X)
	= sum_(cal(kappa) in "PC"(G)) x_kappa
	= sum_(cal(kappa) in "PC"(G)) product_(v in G) x_kappa(v)
$
that is, as the sum over the proper colorings of monomials that count which colors were used.
Notice how by specializing $x_j arrow.bar 1$ for $j <= n$ and $x_j arrow.bar 0$ for $j > n$ we get the number of colorings with $n$ colors, i.e.
$
	chi_G (underbrace(1\, 1\, dots\, 1, n), 0, 0, dots) = p_G (n)
$

This function has also been the subject of numerous investigations, particularly concerning its expansion in some classical basis of symmetric functions: the Schur basis and the elementary basis.
In this connection, we proceed to state some important conjectures about the positivity of the coefficients of $chi_G (X)$ in the expansions in these bases for some large classes of graphs.

Given a poset $P$, i.e. a partially ordered set, we denote by $"inc"(P)$ the incomparability graph of $P$, i.e. the graph that has as vertices the elements of $P$ where two vertices are connected if and only if they are incomparable in $P$.
We say that a poset is $3+1$-free if it does not contain a subposet consisting of the disjoint union of a $3$-chain and a $1$-chain.
This condition is equivalent to requiring that $"inc"(P)$ be claw-free, that is, it does not contain a subgraph consisting of $4$ vertices where one vertex is connected to all others and there are no additional edges.

We say that a symmetric function is $s$-positive if, considering its expansion in the Schur basis, the coefficients are positive.
Similarly, we say that it is $e$-positive considering the expansion on the elementary basis.

In 1995 Vesselin Gasharov in @gasharov_incomparability_1996 proved that if $P$ is $3+1$-free, then $chi_"inc"(P) (X)$ is $s$-positive.
However, the problem, posed by Gasharov and Stanley in @stanley_graph_1998, of determining whether $chi_G (X)$ is $s$-positive for a claw-free $G$-graph remains unsolved.

A better-known and more important conjecture, formulated by Stanley and John R. Stembridge in @stanley_immanants_1993, is the following:
#conjecture(name: [Stanley-Stembridge])[\
Let $P$ be a $3+1$-free poset, then $chi_"inc"(P) (X)$ is $e$-positive.
]
In general, it turns out to be false that $chi_G (X)$ is $e$-positive if $G$ is claw-free but not the incomparability graph of some poset.


The interest behind these conjectures, and more generally behind problems of positivity in elementary and Schur basis, lies in the fact that these are often a clue to some hidden structure: the positive expansion in the Schur basis indicates that the function in question corresponds to a representation of the symmetric group or the general linear group, while the one in the elementary basis, a stronger condition than the previous one, narrows down the possible representations to some more particular and interesting ones.

An important contribution to the Stanley-Stembridge conjecture was made in 2013 by Mathieu Guay-Paquet in @guay-paquet_modular_2013, who showed that it is sufficient to prove the thesis for posets that are at the same time $3+1$-free and $2+2$ free. \
These posets correspond to Unit Interval Orders and their incomparability graphs to Unit Interval Graphs (henceforth "UIGs"), which in turn are in bijection with Dyck Paths, i.e. the paths that connect opposite vertices of a square by moving up or to the right by one unit each time, without ever going below the diagonal.
As can be guessed from the description just given, the graphs to be studied now are much simpler and more classifiable.

This is the context for a further generalization of the chromatic symmetric function, this time due to John Shareshian and Michelle L. Wachs in @shareshian_chromatic_2012.
Given a graph $G = (V,E)$, whose vertices we number from $1$ to $n$, we define the $q$-analog of the chromatic symmetric function:
$
	chi_G^q (X;q) = sum_(cal(kappa) in "PC"(G)) q^"coinv"(kappa) x_kappa
$ <chi_def>
where
$
	"coinv"(kappa) = \#|{(i,j) in E | i < j " and " kappa(i) < kappa(j)}|
$

Of course $chi_G^q (X;1) = chi_G (X)$.
This function in general is not symmetric in the variables ${x_j}$ for $j in NN$, but only quasisymmetric.
In the case $G$ is a UIG, however, it can be shown that $chi_G^q (X;q)$ is a symmetric function.
We can then state the following generalization of the Stanley-Stembridge conjecture, which first appeared in @shareshian_chromatic_2012:
#conjecture(name: [Shareshian-Wachs])[\
Let $G$ be a Unit Interval Graph, then $chi_G^q (X;q)$ is $e$-positive.
]

In addition to the conjecture just stated, it has been independently demonstrated by Patrick Brosnan and Timothy Y. Chow in @brosnan_unit_2018 and by Mathieu Guay-Paquet in @guay-paquet_second_2016 that UIGs and their $chi_G^q (X;q)$ have close connections with the cohomology of Hessenberg varieties.
For these reasons, UIGs have been the subject of numerous studies in recent years.
Among the most interesting results are the following:

- in 2015 Christos A. Athanasiadis in @athanasiadis_power_2015 obtained the following expansion in the power basis:
	$
		omega(chi_G^q (X;q)) = sum_(lambda tack n) p_(lambda) / z_(lambda) sum_(sigma in cal(N)_(G, lambda)) q^(tilde("inv")_G (sigma))
	$ <chi_eq_p_NGl>
	where
	$
		tilde("inv")_G (sigma) = \#|{ i < j | sigma(i) > sigma(j) " and " {sigma(i), sigma(j)} in E }|
	$ <tinv_def>
	while $cal(N)_(G, lambda)$ is a particular subset of $frak(S)_n$;

- let
	$
		"LLT"_G (X;q) = sum_(cal(kappa) in "C"(G)) q^"coinv"(kappa) x_kappa
	$
	where, differently from the definition of $chi_G^q (X;q)$ in @chi_def, the sum is done over all colorings and not just on the proper ones. \
	In 2018 Erik Carlsson and Anton Mellit in @carlsson_proof_2018 proved the following plethystic relation:
	$
		(1-q)^n omega(chi_G^q [X 1/(1-q); q]) = "LLT"_G [X;q]
	$ <chi_eq_LLT>
	where the square brackets indicate the plethysm operation; for a more detailed discussion, refer to @loehr_computational_2011;

- in 2020 Alex Abreu and Antonio Nigro in @abreu_chromatic_2021 showed that $chi_G^q (X;q)$ satisfies a particular modular law, and used the latter in @abreu_symmetric_2021 to obtain the following identities:
	$
		omega(chi_G^q (X;q)) &= sum_(F in "ISF"(G)) q^("wt"_G (F)) rho_lambda(F) \
		omega("LLT"_G^q (X;q)) &= sum_(F in "ISF"(G)) (q-1)^(n - l(lambda(F))) q^("wt"_G (F)) e_lambda(F)
	$ <chi_LLT_eq_rho_e_ISF>
	the sum in this case is performed on Increasing Spanning Forests (henceforth "ISF"), that is, on partitions of the graph into trees whose vertices, starting from the root, are strictly increasing.
	The term $"wt"_G (F)$ represents an appropriate weight, while the $rho_lambda$ is a modified version of the Hall-Littlewood polynomials, more specifically the following holds
	$
		rho_lambda = (-1)^(|lambda| - l(lambda)) (e_lambda [(1-q)X]) / (1-q)^l(lambda)
	$

- in 2023 Michele D'Adderio, Roberto Riccardi, and Viola Siconolfi in @dadderio_chromatic_2023 extended some of the previous results to a more general class of graphs, the Interval Graphs (henceforth "IGs"), i.e. Interval Order incomparability graphs.
	The latter are exactly the $2+2$-free posets.
	For these graphs, $chi_G^q (X;q)$ is no longer necessarily a symmetric function, so it becomes necessary to express it on a basis of quasisymmetric functions.
	Calling $L_alpha$ the fundamental basis, we have the following identities:
	$
		chi_G^q (X;q) &= sum_(sigma in frak(S)_n) q^("coinv"_G (sigma)) L_(n, "Des"(sigma^(-1))) \
		"LLT"_G (X;q) &= sum_(sigma in frak(S)_n) q^("inv"_G (sigma)) L_(n, "Des"(sigma^(-1)))
	$ <chi_LLT_eq_L_Sn>
	where $"Des"(sigma) = {1 <= i < n | sigma(i) > sigma(i+1)}$. \
	Using these two formulas, we can obtain a generalization of the Carlsson-Mellit formula for IGs:
	$
		(1-q)^n omega(chi_G^q [X 1/(1-q); q]) = "LLT"_G (X;q)
	$
	In this case, both the definition of $omega$ and the definition of plethysm require further refinements than in the case of symmetric functions.


= New results and possible future developments

Building on the results of the @dadderio_chromatic_2023 article, it is natural to ask what other identities demonstrated or conjectured for UIGs also apply to IGs.

The latter class of graphs is particularly interesting in that it is easily describable and parameterizable, at the same level as UIGs, and it allows the applications of some techniques always used with UIGs, such as ISFs.
On the other hand, IGs are a much broader and more general class, in which because of the non-symmetricity of $chi_G^q (X;q)$ one can observe new phenomena and find patterns in the quasi-symmetric functions that would have remained hidden by restricting oneself to dealing with UIGs.

A first proved generalization is that of Athanasiadis' @chi_eq_p_NGl formula, which we can now rewrite for $G$ Interval Graph as
$
	omega(chi_G^q (X;q)) = sum_(alpha tack.double n) Psi_alpha / z_alpha sum_(sigma in cal(N)_(G, alpha)) q^("inv"_G (sigma))
$ <chi_eq_Psi_NGa>
where the $Psi_alpha$ are a basis of quasisymmetric functions first introduced in @ballantine_quasisymmetric_2020 that refines the basis of powers.

We then introduced a new object, called (temporarily) Magic Spanning Forest (henceforth "MSF"), which generalizes ISFs: it is a forest on the graph, i.e., a partition of vertices into trees, in which these trees are ordered, rooted, and satisfy less strict rules than those of ISFs.

Let's show the expressiveness of MSFs right away. \
In 2019 Per Alexandersson and Robin Sulzgruber in @alexandersson_p_2021 obtained the following expansion of $chi_G^q (X;q)$ for a generic graph $G$:
$
	omega(chi_G^q (X;q)) = sum_(theta in "AO"(G)) q^("coinv"_G (theta)) sum_(f in cal(O)_G^* (theta)) Psi_"type"(f) / z_"type"(f)
$ <chi_eq_Psi_AO>
where $"AO"(G)$ is the set of acyclic orientations of $G$ and $cal(O)_G^* (theta)$ is a set of ordered partitions of the vertices of $G$ that respect the $theta$ orientation, in addition to satisfying an additional condition that we omit here. \
Starting from this result, we obtain the following identity on the IGs:
$
		omega(chi_G^q (X;q))
		= sum_(F in "MSF"(G)) q^("wt"_G (F)) Psi_alpha(F) / z_alpha(F)
		= sum_(alpha tack.double n) Psi_alpha / z_alpha sum_(F in "MSF"(G) \ alpha(F) = alpha) q^("wt"_G (F))
$ <chi_eq_Psi_MSF>
In this case $"wt"_G (F)$ is a weight on MSFs that generalizes that defined for ISFs. \
Specializing these identities to the case of UIGs one finds:
$
		omega(chi_G^q (X;q)) = sum_(lambda tack n) p_lambda / z_lambda binom(l(lambda), m_1 (lambda)\, dots\, m_n (lambda))^(-1) sum_(F in "MSF"(G) \ lambda(F) = lambda) q^("wt"_G (F))
$ <chi_eq_p_MSF>
where $m_i (lambda)$ counts the number of parts of $lambda$ of size $i$. \
Note the similarity of these expressions to the Abreu-Nigro expression stated in @chi_LLT_eq_rho_e_ISF.
However, the proof is very different and reduces to constructing a weight-preserving bijection between pairs of acyclic orientations with relative partitions and MSFs.

In addition to this first use of MSFs, we believe that they are closely related to the set $cal(N)_(G, alpha)$ and can be used to obtain @chi_eq_Psi_NGa again. \
In fact, given that $cal(N)_(G, (n))^i = {sigma in cal(N)_(G, (n)) | sigma(1) = i}$, we can show that $\#|cal(N)_(G, (n))^i| = 1/n \#|cal(N)_(G, (n))|$ and construct a bigection between $cal(N)_(G, (n))^1$ and the Increasing Spanning Trees of $G$.
This fact can be generalized by proving $\#|{ F in "ISF" | lambda(F) = lambda}| = 1/z_lambda \#|cal(N)_(G, lambda)|$ by means of a proper bigection. \
In this context, we believe it is possible to extend this map to a bijection between MSFs of type $alpha$ and $cal(N)_(G, alpha)$ that preserves the right weights so that the equations @chi_eq_Psi_NGa and @chi_eq_Psi_MSF return.

As can be guessed from what has been stated so far, a more general goal is to construct a "unified" theory for acyclic orientations, MSFs and $cal(N)_(G, alpha)$.

Another possible direction of research (with much less chance of success) is through a more direct application of MSFs to the Shareshian-Wachs conjecture. \
As shown in @shareshian_chromatic_2012, the conjecture can be reduced to finding a statistic $mu : "AO"(G) arrow "Partitions"(n)$ so that
$
	chi_G^q (X;q) = sum_(theta in "AO"(G)) q^("coinv"(theta)) e_mu(theta)
$
Choosing a $f_theta in cal(O)_G^*(theta)$ for each $theta$ and setting $mu(theta) = lambda("type"(f_theta))$, we rewrite the previous expression as
$
	chi_G^q (X;q)
		= sum_(theta in "AO"(G)) q^("coinv"(theta)) e_lambda("type"(f_theta))
		= sum_(F in "MSF"^*) q^("wt"_G (F)) e_lambda(F)
$ <chi_eq_e_MSF>
where $"MSF"^*$ is the set given by the previously established bijection between the pairs of acyclic orientations with relative partitions and the MSFs. \
Through some experiments, we noticed that by choosing some "maximal" $f_theta$, that is, in such a way as to minimize the length of the composition $"type"(f_theta)$, the identity @chi_eq_e_MSF is verified.
The choice is not unique and not all choices work, however, for each UIG we were able to generate one such that the identity is satisfied.

A different approach to the problem is obtained by writing @chi_eq_p_MSF on the elementary basis
$
	chi_G^q (X;q) = sum_(lambda tack n) e_lambda
    sum_(F in "MSF"(G)) (-1)^(l(lambda) - l(lambda(F))) / (l(lambda(F))! product_(j=0)^l(lambda(F)) lambda(F)_j) w(B_(lambda, lambda(F))) q^("wt"_G (F))
$
where $B_(lambda, mu)$ is an appropriate basic change coefficient. \
The coefficients of $e_lambda$ do not seem to lend themselves to convenient operations, such as constructing a classical involution leading to deleting terms with opposite signs.
However, this expression leaves some hope, since the structure of MSFs allows grouping several terms with the same weight.
Therefore, we believe that this approach should be investigated further if only to get a better understanding of what the maximal elements in @chi_eq_e_MSF might be.

////

#pagebreak()
#bibliography("extended-abstract.bib")