\documentclass[10pt]{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{babel}
\usepackage{biblatex}
\usepackage{csquotes}
\usepackage{etoolbox}
\usepackage{quiver}
\usepackage{setspace}
\usepackage{tikz-cd}
\usepackage{tikz}

\usetheme{Madrid}

\newcommand{\zerodisplayskips}{%
  \setlength{\abovedisplayskip}{3pt}%
  \setlength{\belowdisplayskip}{3pt}%
  \setlength{\abovedisplayshortskip}{3pt}%
  \setlength{\belowdisplayshortskip}{3pt}}
\appto{\normalsize}{\zerodisplayskips}
\appto{\small}{\zerodisplayskips}
\appto{\footnotesize}{\zerodisplayskips}

\newenvironment<>{definitionblock}[1]{%
	\setbeamercolor{block title}{fg=white,bg=blue!75!black}%
	\begin{block}#2{#1}}{\end{block}%
}
\newenvironment<>{theoremblock}[1]{%
	\setbeamercolor{block title}{fg=white,bg=green!40!black}%
	\begin{block}#2{#1}}{\end{block}%
}
\newenvironment<>{problemblock}[1]{%
	\setbeamercolor{block title}{fg=white,bg=red!75!black}%
	\begin{block}#2{#1}}{\end{block}%
}
\renewenvironment<>{exampleblock}[1]{%
	\setbeamercolor{block title}{fg=white,bg=orange!85!Black}%
	\begin{block}#2{#1}}{\end{block}%
}

\newcommand{\NN}{\mathbb{N}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\Hc}{\mathcal{H}}
\newcommand{\Nc}{\mathcal{N}}
\newcommand{\Oc}{\mathcal{O}}
\DeclareMathOperator{\PC}{PC}
\DeclareMathOperator{\ISF}{ISF}
\DeclareMathOperator{\MSF}{MSF}
\DeclareMathOperator{\AO}{AO}
\DeclareMathOperator{\Id}{Id}
\DeclareMathOperator{\Sym}{Sym}
\DeclareMathOperator{\QSym}{QSym}
\DeclareMathOperator{\inc}{inc}
\DeclareMathOperator{\inv}{inv}
\DeclareMathOperator{\coinv}{coinv}
\DeclareMathOperator{\wt}{wt}


\title{Chromatic quasisymmetric functions}
\author[Eduardo Venturini]{%
	Candidate: Eduardo Venturini%
	\texorpdfstring{\\}{}%
	Supervisor: prof. Michele D'Adderio%
}
\institute[UniPi]{University of Pisa}
\date{July 12, 2024}


\begin{document}


\frame{\titlepage}


\begin{frame}
	\frametitle{Chromatic polynomials}

	\pause
	$G =(V,E)$ simple graph
	\begin{minipage}{0.75\textwidth}%
		\begin{definitionblock}{Coloring}
			$\kappa : V \to \NN^+$ \\
			$\kappa$ is with (at most) $n$ colors if $\kappa(V) \subset \{1, \dots, n\}$ \\
			$\kappa$ is proper if $\{i,j\} \in E \Rightarrow \kappa(i) \neq \kappa(j)$
		\end{definitionblock}
	\end{minipage}%
	\hfill%
	\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph0}}\end{minipage}

	\pause
	\begin{definitionblock}{Chromatic polynomials}
		\vspace*{-\medskipamount}
		\[ p_G(r) = |\PC(G, r)| = |\{ \text{proper colorings of $G$ with $r$ colors} \}| \]
	\end{definitionblock}

	\pause
	\[ p_G(r) = p_{G \sqcup e}(r) + p_{G / e}(r) \]

	\begin{center}
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph1_0}}\end{minipage}
		$\quad = \quad$
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph1_1}}\end{minipage}
		$\quad + \quad$
		\begin{minipage}{0.2\textwidth}\resizebox{\textwidth}{!}{\input{res/graph1_2}}\end{minipage}
	\end{center}

\end{frame}


\begin{frame}
	\frametitle{Introduction to symmetric functions}

	\pause
	\begin{definitionblock}{Symmetric Functions}
		Formal series in $\QQ[X] = \QQ[\{x_j\}_{j \in \NN^+}]$ of finite degree, invariant by permutation of variables. \\
		They form the graded algebra $\Sym[X]$.
	\end{definitionblock}

	\pause
	\begin{definitionblock}{Composition}
		$\alpha = (\alpha_1, \dots, \alpha_k)$ is a composition of $n \in \NN$ (notation: $\alpha \vDash n$) if \\
		$\alpha_i \in \NN^+$ and $\alpha_1 + \dots + \alpha_k = n$.
	\end{definitionblock}
	\begin{definitionblock}{Partition}
		$\lambda = (\lambda_1, \dots, \lambda_k)$ is a partition of $n \in \NN$ (notation: $\lambda \vdash n$) if \\
		$\lambda_i \in \NN^+$, $\lambda_1 \ge \lambda_2 \ge \dots \ge \lambda_j > 0$ and $\lambda_1 + \dots + \lambda_k = n$.
	\end{definitionblock}

	\pause
	Notation:
	\begin{itemize}
		\item $x^{\alpha} = x_1^{\alpha_1} x_2^{\alpha_2} \dots$ for a sequence $\alpha \in \NN^k$
		\item $\alpha \sim \lambda$ if sorting $\alpha$ decreasingly we get $\lambda$
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Introduction to symmetric functions}

	\pause
	Some symmetric functions:
	\begin{itemize}
		\item monomial: $m_{\lambda} = \sum_{\alpha \sim \lambda} x^{\alpha}$
		\item elementary: $e_{\lambda} = e_{\lambda_1} \dots e_{\lambda_j}$ where $e_k = \sum_{i_1 < \dots < i_k} x_{i_1} \dots x_{i_k}$
		\item homogeneous: $h_{\lambda} = h_{\lambda_1} \dots h_{\lambda_j}$ where $h_k = \sum_{i_1 \le \dots \le i_k} x_{i_1} \dots x_{i_k}$
		\item powersum: $p_{\lambda} = p_{\lambda_1} \dots p_{\lambda_j}$ where $p_k = \sum_{i \in \NN} x_i^k$
		%\item schur: $s_{\lambda} = \dots$ (it's complicated)
	\end{itemize}

	\pause
	Examples:
	\begin{itemize}
		\item $m_{2,1,1} = x_1^2 x_2 x_3 + x_1 x_2^2 x_3 + x_1 x_2 x_3^2 + x_1^2 x_2 x_4 + \dots$
		\item $e_{2,1,1} = (x_1 x_2 + x_1 x_3 + x_2 x_3 + \dots) (x_1 + x_2 + x_3 + \dots)^2$
		\item $p_{2,1,1} = (x_1^2 + x_2^2 + x_3^2 + \dots) (x_1 + x_2 + x_3 + \dots)^2$
	\end{itemize}

	\pause
	\begin{theoremblock}{Symmetric functions basis}
		$\{m_{\lambda}\}_{\lambda \vdash n}$, $\{e_{\lambda}\}_{\lambda \vdash n}$, $\{h_{\lambda}\}_{\lambda \vdash n}$, $\{p_{\lambda}\}_{\lambda \vdash n}$ are a basis of the symmetric functions of degree $n$ as vector space. \\
		$\{e_j\}_{j \in \NN}$, $\{h_j\}_{j \in \NN}$ and $\{p_j\}_{j \in \NN}$ are algebraically independent.
	\end{theoremblock}

\end{frame}


\begin{frame}
	\frametitle{Chromatic symmetric functions}

	\pause
	\begin{minipage}{0.75\textwidth}%
		\begin{definitionblock}{Chromatic symmetric function - R. Stanley - 1995}
		\[
			\chi_G(X)
			= \sum_{\kappa \in \PC(G)} x_{\kappa}
			= \sum_{\kappa \in \PC(G)} \prod_{v \in V} x_{\kappa(v)}
		\]
		\end{definitionblock}
	\end{minipage}%
	\hfill%
	\begin{minipage}{0.2\textwidth}
		\resizebox{\textwidth}{!}{\input{res/graph0}}\\
		\[ x_{\kappa} = x_1 x_2^3 x_3 \]
	\end{minipage}

	\pause
	\medskip
	$\chi_G(X)$ is a symmetric function.

	\medskip
	$x_1, x_2, \dots, x_r \mapsto 1$ and $x_{r+1}, x_{r+2}, \dots \mapsto 0 \quad \Longrightarrow \quad \chi_G(X) \mapsto p_G(r)$ \\
	Let $\psi : \Sym[X] \to \QQ[x]$ with $\psi(p_j) = r \quad \Longrightarrow \quad \psi(\chi_G) = p_G$

\end{frame}


\begin{frame}
	\frametitle{Stanley-Stembridge conjecture}

	\pause
	\begin{minipage}{0.65\textwidth}%
		\begin{definitionblock}{Incomparability graph}
			$P$ poset, let $\inc(P) = (V, E)$ where
			\begin{itemize}
				\item $V$ are the elements of $P$
				\item $\{i,j\} \in E$ if $i$ and $j$ are incomparable in $P$
			\end{itemize}
		\end{definitionblock}
	\end{minipage}%
	\hfill%
	\begin{minipage}{0.3\textwidth}
		\resizebox{\textwidth}{!}{\input{res/hasse0} $\longrightarrow$ \input{res/graph2}}
	\end{minipage}

	\pause
	\medskip
	$P$ is $3+1$-free if it does not contain a subposet consisting of the disjoint union of a 3-chain and a 1-chain.

	\pause
	\medskip
	\begin{problemblock}{Stanley-Stembridge conjecture - 1995}
		Let $P$ be a $3+1$-free poset, then
		\[
			\chi_{\inc(P)}(X) \in \NN[\{e_{\lambda}\}_{\lambda \vdash |V|}]
		\]
	\end{problemblock}

\end{frame}


\begin{frame}
	\frametitle{Unit Interval Graphs}

	\pause
	\begin{theoremblock}{Mathieu Guay-Paquet - 2013}
		The Stanley-Stembridge conjecture holds for all posets $(3+1)$-free if and only if it holds for all posets both $(3+1)$-free and $(2+2)$-free.
	\end{theoremblock}

	\pause
	The incomparability graphs of $(3+1)$-free and $(2+2)$-free posets are the Unit Interval Graphs.

	\begin{minipage}{0.70\textwidth}%
		\begin{definitionblock}{Unit Interval Graph (UIG)}
			Graph $G = (V,E)$ where
			\begin{itemize}
				\item $V = \{1, \dots, n\}$
				\item if $\{i,j\} \in E$, then $\{i,k\} \in E$ and $\{k,j\} \in E$ for every $i < k < j$
			\end{itemize}
		\end{definitionblock}
	\end{minipage}%
	\hfill%
	\begin{minipage}{0.25\textwidth}
		\begin{center}
			\includegraphics[width=\textwidth]{res/graph3}
			\resizebox{0.8\textwidth}{!}{\input{res/graph3}}
		\end{center}
	\end{minipage}

	\medskip
	$\{i,j\} \in E \Longleftrightarrow$ cell $(i,j)$ lies between the diagonal and the path.

\end{frame}


\begin{frame}
	\frametitle{Introduction to quasisymmetric functions}

	\pause
	\begin{definitionblock}{Quasisymmetric Functions}
		Formal series in $\QQ[X] = \QQ[\{x_j\}_{j \in \NN^+}]$ of finite degree, invariant by shift of variables, i.e. the coefficient of $x^{\alpha} = x_{i_1}^{\alpha_1} \dots x_{i_j}^{\alpha_j}$ is the same for every $i_1 < \dots < i_j$. \\
		They form the graded algebra $\QSym[X]$.
	\end{definitionblock}

	\pause
	Some quasisymmetric functions:
	\begin{itemize}
		\item monomial: $M_{\alpha} = \sum_{i_1 < \dots < i_j} x_{i_1}^{\alpha_1} \dots x_{i_j}^{\alpha_j}$
		\item powersum: $\Psi_{\alpha} = \dots$ (it's complicated)
	\end{itemize}

	Example: $M_{1,2,1} = x_1 x_2^2 x_3 + x_1 x_2^2 x_4 + x_1 x_3^2 x_4 + x_2 x_3^2 x_4 + \dots$

	\pause
	\begin{theoremblock}{Quasisymmetric function basis}
		$\{M_{\alpha}\}_{\alpha \vDash n}$ and $\{\Psi_{\alpha}\}_{\alpha \vDash n}$, are a basis of the quasisymmetric functions of degree $n$ as vector space. \\
		These bases refine some bases of the symmetric functions:
		\begin{itemize}
			\item $m_{\lambda} = \sum_{\alpha \sim \lambda} M_{\alpha}$
			\item $p_{\lambda} = \sum_{\alpha \sim \lambda} \Psi_{\alpha}$
		\end{itemize}
	\end{theoremblock}

\end{frame}

\begin{frame}
	\frametitle{Chromatic quasisymmetric functions}

	\pause
	Let $G$ be graph with $V = \{1, \dots, n\}$ and let $\kappa \in \PC(G)$.

	Define
	\[
		\coinv(\kappa) = \#|\{\{i,j\} \in E \mid i < j \text{ and } \kappa(i) < \kappa(j) \}|
	\]

	\pause
	\medskip
	\begin{minipage}{0.75\textwidth}%
		\begin{definitionblock}{Chromatic quasisymmetric function \\ J. Shareshian, M. L. Wachs - 2012}
			\[
				\chi_G^q(X;q) = \sum_{\kappa \in \PC(G)} q^{\coinv(\kappa)} x_{\kappa}
			\]
		\end{definitionblock}
	\end{minipage}%
	\hfill%
	\begin{minipage}{0.2\textwidth}
		\resizebox{\textwidth}{!}{\input{res/graph0}}\\
		\[ q^5 x_1 x_2^3 x_3 \]
	\end{minipage}

	\pause
	\medskip
	Generally $\chi_G^q(X;q)$ is not symmetric, but only quasisymmetric. \\
	However, if $G$ is a UIG, $\chi_G^q(X;q)$ is always symmetric.

\end{frame}


\begin{frame}
	\frametitle{Shareshian-Wachs conjecture}

	\pause
	\begin{problemblock}{Shareshian-Wachs conjecture - 2012}
		Let $G$ be a Unit Interval Graph, then $\chi_G^q(X;q)$ is $e$-positive, that is
		\[
			\chi_G^q(X;q) \in \NN[\{e_{\lambda}\}_{\lambda \vdash |V|}, q]
		\]
	\end{problemblock}

	\pause
	\medskip
	Why do we care about this problem?
	\begin{itemize}
		\pause
		\item Positivity in a basis indicates the presence of hidden structures, and the function is counting these structures. \\
			For example, the structures could be the representations of $\mathfrak{S}_n$ and $e$-positivity corresponds to an action by permutation stabilized by a Young subgroup.
		\pause
		\item Connection with \\
			\smallskip
			\begin{tabular}{c c}
				Hessenberg varieties: & LLT polynomials: \\
				$\omega (\chi_G^q(X;q)) = \sum_{i=0}^{|E|} q^i \text{Frob } H^{2i}(\Hc(G))$
				&
				$\text{LLT}_G[X(q-1)] = \chi_G^q[X] (q-1)^n$ \\
			\end{tabular}
		\pause
		\item It is a fairly simple and natural problem, but it has remained open for more than 30 years (and Stanley proposed it!).
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Shareshian-Wachs conjecture}

	\begin{problemblock}{Shareshian-Wachs conjecture - 2012}
		Let $G$ be a Unit Interval Graph, then $\chi_G^q(X;q)$ is $e$-positive, that is
		\[
			\chi_G^q(X;q) \in \NN[\{e_{\lambda}\}_{\lambda \vdash |V|}, q]
		\]
	\end{problemblock}

	\medskip
	How to deal with this problem? Some ideas:
	\begin{itemize}
		\pause
		\item Studying Hessenberg varieties, including by algebraic and geometric means. \\
			Using theory on LLT polynomials.
		\pause
		\item Generalizing the problem to interval graphs (IGs).
		\pause
		\item Obtaining new formulas to expand $\chi_G^q(X;q)$:
			\begin{itemize}
				\item new bases for symmetric and quasisymmetric functions ($\rho_{\lambda}$, $\Psi_{\alpha}$);
				\item new structures and statistics that can capture information in graphs;
				\item constructive approaches (bjections, involutions) and nonconstructive approaches (local relationships, modular law).
			\end{itemize}
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Shareshian-Wachs conjecture}

	\begin{problemblock}{Shareshian-Wachs conjecture - 2012}
		Let $G$ be a Unit Interval Graph, then $\chi_G^q(X;q)$ is $e$-positive, that is
		\[
			\chi_G^q(X;q) \in \NN[\{e_{\lambda}\}_{\lambda \vdash |V|}, q]
		\]
	\end{problemblock}

	\medskip
	For example.
	\begin{itemize}
		\pause
		\item Expansion in the basis $\rho_{\lambda}$ for UIGs using the modular law and:
			\begin{itemize}
				\item Sets of permutations ($\mathfrak{S}_{\le h}$)
				\item Increasing Spanning Forests (ISFs)
				\item Acyclic Orientations (AOs)
			\end{itemize}
		\pause
		\item Expansion in the basis $\Psi_{\alpha}$ for IGs using:
			\begin{itemize}
				\item Sets of permutations ($\Nc_{G, \alpha}$)
				\item Acyclic Orientations (AOs)
				\item Magic Spanning Forests (MSFs)
			\end{itemize}
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Interval Graphs}

	\pause
	\begin{minipage}{0.70\textwidth}%
		\begin{definitionblock}{Interval Graph (IG)}
			Graph $G = (V,E)$ where
			\begin{itemize}
				\item $V = \{1, \dots, n\}$
				\item if $\{i,j\} \in E$, then $\{i,k\} \in E$ for every $i < k < j$
			\end{itemize}
		\end{definitionblock}
	\end{minipage}%
	\hfill%
	\begin{minipage}{0.25\textwidth}
		\begin{center}
			\includegraphics[width=\textwidth]{res/graph4}
			\resizebox{0.8\textwidth}{!}{\input{res/graph4}}
		\end{center}
	\end{minipage}

	\medskip
	They are the incomparability graphs of the $2+2$-free posets.

	\medskip
	$\{i,j\} \in E \Longleftrightarrow$ cell $(i,j)$ lies between the diagonal and the path.

\end{frame}


\begin{frame}
	\frametitle{Increasing Spanning Forests and Modular Law}

	\pause
	\begin{minipage}{0.70\textwidth}%
		\begin{definitionblock}{Increasing Spanning Forest (ISF)}
			An ISF of a labeled graph is a partition of the vertices into rooted trees such that for each tree the labels are strictly increasing going from the root to the leaves.
		\end{definitionblock}
		\onslide<3->{
		\medskip
		In 2020 Abreu and Nigro proved the following identity for UIGs:
		\[
			\omega(\chi_G^q(X;q)) = \sum_{F \in \ISF(G)} q^{\wt_G(F)} \rho_{\lambda(F)}
		\]
		}
	\end{minipage}
	\hfill%
	\begin{minipage}{0.25\textwidth}
		\begin{center}
			\includegraphics[width=\textwidth]{res/graph3} \\
			\resizebox{0.8\textwidth}{!}{\input{res/graph3}} \\
			\bigskip
			\hrule
			\bigskip
			\resizebox{0.5\textwidth}{!}{\input{res/isf0}} \\
		\end{center}
	\end{minipage}

\end{frame}


\begin{frame}
	\frametitle{Increasing Spanning Forests and Modular Law}

	How do we prove it?

	\pause
	\begin{definitionblock}{Modular Law}
		A function $f : \text{UIGs} \to \Sym[X]$ satisfies the modular law if
		\[
			f(G_2) + q f(G_0) = (1+q) f(G_1)
		\]
		for $G_0$, $G_1$ and $G_2$ satisfying particular conditions, including $G_1 = G_2 \setminus e_2$ and $G_0 = G_1 \setminus e_1$.
	\end{definitionblock}

	\pause
	A general version with $q \mapsto 1$ was used in Guay-Paquet's proof.

	They generalize the deletion-contraction relationship of the chromatic polynomial.

	\pause
	\medskip
	Proof:
	\begin{itemize}
		\pause
		\item Both $\chi_G^q(X;q)$ and $\sum_{F \in \ISF(G)} q^{\wt_G(F)} \rho_{\lambda(F)}$ satisfies the modular law.
		\pause
		\item A function that satisfies the modular law is uniquely determined by the values it takes on $K_n$.
		\pause
		\item Both functions take the same values on $K_n$, so they are globally equal.
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Increasing Spanning Forests and Modular Law}

	Can we generalize the previous result to IGs?

	\pause
	Regarding the modular law.
	\begin{itemize}
		\item The modular law for $\chi_G^q(X;q)$ holds also for IGs.
		\item All local linear relations seems to be obtained by applying the modular law.
		\item No set of IGs indexed by the compositions $\alpha \vDash n$ on which a function satisfying the modular law is determined.
			Probably larger classes of graphs are needed.
	\end{itemize}

	\pause
	Regarding Increasing Spanning Forests.
	\begin{itemize}
		\item No refinement of the elementary basis or the related Hall-Littlewood basis in the literature.
			Some ideas for constructing refinements do not apply.
		\item Increasing Spanning Forests were reused by D'Adderio to find some identities for IGs through a different, more constructive approach.
		\item We introduced a generalization of ISFs, i.e. Magic Spanning Forests, but with different weights and we used different, more constructive approaches.
	\end{itemize}

\end{frame}


\begin{frame}
	\frametitle{Acyclic Orientations}

	\pause
	\begin{minipage}{0.70\textwidth}%
		\begin{definitionblock}{Acyclic Orientation (AO)}
			An Acyclic Orientation is a choice of a direction for each edge such that, as one travels the arcs along the chosen direction, one can never return to the starting point.
		\end{definitionblock}

		For $\theta \in \AO(G)$ let $\Oc_G^*(\theta)$ be a specific set of ordered partition of $V$.
		Let
		\[
			\AO_{\alpha}^*(G) = \{ (\theta, f) \mid \theta \in \AO(G), f \in \Oc_G^*(\theta), \alpha(f) = \alpha \}
		\]
	\end{minipage}
	\hfill%
	\begin{minipage}{0.25\textwidth}
		\begin{center}
			\includegraphics[width=\textwidth]{res/graph4}
			\resizebox{0.8\textwidth}{!}{\input{res/graph4}}
			\bigskip
			\hrule
			\bigskip
			\resizebox{0.8\textwidth}{!}{\input{res/ao0}}
		\end{center}
	\end{minipage}

	\pause
	\begin{theoremblock}{Alexandersson, Sulzgruber - 2019}
		Let $G$ be a generic graph, then
		\[
			\omega(\chi_G^q(X;q)) = \sum_{\alpha \vDash n} \frac{\Psi_{\alpha}}{z_{\alpha}} \sum_{(\theta, f) \in \AO_{\alpha}^*(G)} q^{\inv_G(\theta)}
		\]
	\end{theoremblock}

\end{frame}


\begin{frame}
	\frametitle{Permutations of $\Nc_{G, \alpha}$}

	\pause
	Given a composition $\alpha$, we consider the set $\mathcal{N}_{G, \alpha} \subset \mathfrak{S}_{|V|}$ of permutations satisfying some particular property.

	\pause
	\begin{minipage}{0.48\textwidth}
		\begin{theoremblock}{Athanasiadis - 2015}
			Let $G$ be an UIG, then
			\[
				\omega(\chi_G^q(X;q)) = \sum_{\lambda \vdash n} \frac{p_{\lambda}}{z_{\lambda}} \sum_{\sigma \in \Nc_{G, \lambda}} q^{\widetilde{\inv}_G (\sigma)}
			\]
		\end{theoremblock}
	\end{minipage}
	\hfill
	\pause
	\begin{minipage}{0.48\textwidth}
		\begin{problemblock}{D'Adderio - 2024}
			Let $G$ be an IG, prove that
			\[
				\omega(\chi_G^q(X;q)) = \sum_{\alpha \vDash n} \frac{\Psi_{\alpha}}{z_{\alpha}} \sum_{\sigma \in \Nc_{G, \alpha}} q^{\widetilde{\inv}_G (\sigma)}
			\]
		\end{problemblock}
	\end{minipage}

	\pause
	\begin{theoremblock}{Equivalence between $\Nc_{G, \alpha}$ and $\AO_{\alpha}^* (G)$}
		There is a bijection $\Phi : \Nc_{G, \alpha} \to \AO_{\alpha}^* (G)$ such that
		$\widetilde{\inv}_G (\sigma) = \inv_G(\Phi(\sigma))$. \\
	\end{theoremblock}

	\pause
	Consequently,
	\[
		\omega(\chi_G^q(X;q))
			= \sum_{\alpha \vDash n} \frac{\Psi_{\alpha}}{z_{\alpha}} \sum_{(\theta, f) \in \AO_{\alpha}^*(G)} q^{\inv_G(\theta)}
			= \sum_{\alpha \vDash n} \frac{\Psi_{\alpha}}{z_{\alpha}} \sum_{\sigma \in \Nc_{G, \alpha}} q^{\widetilde{\inv}_G (\sigma)}
	\]
	so D'Adderio's conjecture is proved (again).

\end{frame}


\begin{frame}
	\frametitle{Magic Spanning Forests}

	\pause
	\begin{minipage}{0.71\textwidth}%
		\begin{definitionblock}{Magic Spanning Forest (MSF)}
			A MSF of a labeled graph is an ordered partition of the vertices into rooted trees such that, if traversing a tree starting from the root to the leaves we encounter the labels $a$, $b$, $c$, and both $\{a,b\} \in E$ and $\{a,c\} \in E$, then $b<c$.
		\end{definitionblock}

		\medskip
		Let $\MSF_{\alpha}(G)$ be set of MSFs $F$ whose succession of tree sizes $\alpha(F)$ is $\alpha$.
	\end{minipage}
	\hfill%
	\begin{minipage}{0.25\textwidth}
		\begin{center}
			\includegraphics[width=\textwidth]{res/graph4}
			\resizebox{0.8\textwidth}{!}{\input{res/graph4}}
			\bigskip
			\hrule
			\bigskip
			\resizebox{0.3\textwidth}{!}{\input{res/msf0}}
		\end{center}
	\end{minipage}

\end{frame}


\begin{frame}
	\frametitle{Magic Spanning Forests}

	\pause
	\begin{theoremblock}{$\chi_G^q$ expansion using MSFs}
		Let $G$ be an Interval Graph, then
		\[
			\omega(\chi_G^q(X;q))
				= \sum_{\alpha \vDash n} \frac{\Psi_{\alpha}}{z_{\alpha}} \sum_{F \in \MSF_{\alpha}(G)} q^{\inv_G(F)}
		\]
	\end{theoremblock}

	\pause
	\medskip
	\begin{theoremblock}{Equivalence between $\MSF_{\alpha}(G)$ and $\AO_{\alpha}^* (G)$}
		There is a bijection $\Phi : \MSF_{\alpha}(G) \to \AO_{\alpha}^* (G)$ such that
		$\inv_G (F) = \inv_G(\Phi(F))$. \\
	\end{theoremblock}

	\pause
	Consequently,
	\[
		\omega(\chi_G^q(X;q))
			= \sum_{\alpha \vDash n} \frac{\Psi_{\alpha}}{z_{\alpha}} \sum_{(\theta, f) \in \AO_{\alpha}^*(G)} q^{\inv_G(\theta)}
			= \sum_{\alpha \vDash n} \frac{\Psi_{\alpha}}{z_{\alpha}} \sum_{F \in \MSF_{\alpha}} q^{\inv_G(F)}
	\]

\end{frame}


\begin{frame}
%	\frametitle{}

	\begin{center}
		{\LARGE
			Thank you \\
			for your attention! \\
		}
		\vspace{32pt}
		Questions?
	\end{center}

\end{frame}

\end{document}
